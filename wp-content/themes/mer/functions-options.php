<?php




add_action('admin_head', 'editor_admin_head');



add_action( 'admin_init', 'theme_options_init' );



add_action( 'admin_menu', 'theme_options_add_page' );







/**



 * Init plugin options to white list our options



 */



function theme_options_init(){







	register_setting( 'mer_options', 'mer_theme_options' );



}







/**







* Load Head 



*/







function editor_admin_head() {

	 

	

wp_enqueue_style('thickbox');

wp_enqueue_script('media-upload');

wp_enqueue_script('thickbox');

	 

wp_register_script('mer-utility', '/wp-content/themes/mer/js/utility.js' );

wp_enqueue_script('mer-utility');

  



}





 





/**



 * Load up the menu page



 */



function theme_options_add_page() {



	add_theme_page( __( 'Theme Options', 'mertheme' ), __( 'Theme Options', 'mertheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );



}







/**



 * Create arrays for our select and radio options



 */



 



/**



 * Create the options page



 */



function theme_options_do_page() {



  



	require_once ( get_template_directory() . '/theme-options.php' );



}







/**



 * Sanitize and validate input. Accepts an array, return a sanitized array.



 */



function theme_options_validate( $input ) {



	global $select_options, $radio_options;







	// Our checkbox value is either 0 or 1



	if ( ! isset( $input['option1'] ) )



		$input['option1'] = null;



	$input['option1'] = ( $input['option1'] == 1 ? 1 : 0 );







	// Say our text option must be safe text with no HTML tags



	$input['sometext'] = wp_filter_nohtml_kses( $input['sometext'] );







	// Our select option must actually be in our array of select options



	if ( ! array_key_exists( $input['selectinput'], $select_options ) )



		$input['selectinput'] = null;







	// Our radio option must actually be in our array of radio options



	if ( ! isset( $input['radioinput'] ) )



		$input['radioinput'] = null;



	if ( ! array_key_exists( $input['radioinput'], $radio_options ) )



		$input['radioinput'] = null;







	// Say our textarea option must be safe text with the allowed tags for posts



	$input['sometextarea'] = wp_filter_post_kses( $input['sometextarea'] );







	return $input;



}























// adapted from http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/