<?php 

$options=$_SESSION['mer_site_option'];
$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];

?>

<div  class='menu' >
	<div  class='menu-inner' >
    	<ul id='dropdown-menu'>
        <li><a <?php if($Conference_Schedule_Page==get_the_ID()){ echo " class='active' "; } ?>   href='<?php echo get_permalink( $Conference_Schedule_Page ); ?>'>Conference Schedule</a></li>
        <li><a <?php if($Topics==get_the_ID()){ echo " class='active' "; } ?> href='<?php echo get_permalink( $Topics ); ?>'>Topics</a></li>
        <li><a <?php if($Accredetation==get_the_ID()){ echo " class='active' "; } ?> href='<?php echo get_permalink( $Accredetation ); ?>'>Accreditation</a></li>
        <li id="menu4"><a  href='#'>Half-Day Format</a>
            <ul class="display-none" id="smenu4" ><li>
            <div class="menu4"  ><?php $menu4=html_entity_decode($options['halfdaytext']);echo $menu4 ; ?></div>
            </li>
            </ul>
        </li>
        </ul>
       
    </div>
    <div  class='login' >
         <div  class='login-box' >
         
          <?php  
           
		   if ( empty( $redirect_to ) ) {
			if ( isset( $_REQUEST['redirect_to'] ) ) 
				$redirect_to = esc_url( $_REQUEST['redirect_to'] );
			else
				$redirect_to =  current_page_url(); 
		}
		
			if ( force_ssl_admin() ) 
			$redirect_to = str_replace( 'http:', 'https:', $redirect_to );
		           // $redirect_to = trim(stripslashes(get_option('sidebarlogin_login_redirect')));
		if ($user_ID != '') {
		$current_user=get_userdata($user_ID);
  		$lg= get_permalink( $options['profile_page']);
	$logouturl=	wp_logout_url( site_url() ); 
		/*if(strpos($lg,'?')===false){
			$logouturl=$lg.'?logout=1';
			}else{
			$logouturl=$lg.'&logout=1';	
				}*/
		echo "<div class='welcome-container' >
			<div class='welcome'>welcome " . $current_user->display_name."</div>
				<ul>
				<li><a href='". get_permalink( $options['profile_page'])."'>My Account</a></li>
					<li> <a href='".$logouturl."'>Log Out</a></li>
				</ul>
			</div>
			
			";
		}else{
		$pg=$options['Login_Page'];
			$lg= get_permalink( $options['Login_Page']);
		
		if(strpos($lg,'?')===false){
			$resetpurl=$lg.'?resetp=1';
			}else{
			$resetpurl=$lg.'&resetp=1';	
				}
		
		if(strpos($lg,'?')===false){
			$forgeturl=$lg.'?forget=1';
			}else{
			$forgeturl=$lg.'&forget=1';	
				}
		?><?php if($_GET['page_id']!=$pg) {?>
         		<div class='login-container' >
                
                 <form name="loginform" id="loginform" action="<?php echo $lg; ?>" method="post"> 
                	<input type="text" class="username"  placeholder="username" id="log" name="log"  />
                	<input type="password" placeholder="password" name="user_password" id="pwd" class="password"  />
                    <input type="submit" name="wp_submit" class="login-submit" value="login" />
                     
                     
                  </form>
               
                </div>
             <?php }else{?><div class='login-container2' >&nbsp;</div><?php } ?>
                <div class='login-container-forget' >
                <a href='<?php echo $resetpurl ?>'>Forgot Password?</a>
                <a href='<?php echo $forgeturl ?>'>Forgot Username?</a>
                <a href='<?php echo get_permalink( $options['register_page']) ;?>'>New to MER? Click here</a>
                </div><?php
		  
          }?>      
          </div>
    </div>
     <div class="clear" ></div> 
</div>