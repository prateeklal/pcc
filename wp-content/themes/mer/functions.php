<?php
//  
session_start();

if (isset($_GET['review'])){

 $_SESSION['review']=1;
header("Location:index.php");
}

if (!$_SESSION['review']){

 require_once(get_template_directory() .'/offline.php');
 exit;
}

 $post_type = ( isset($_GET["post_type"]) ) ? sanitize_text_field($_GET["post_type"]) : "" ;
 

/**

 * @package WordPress

 * @subpackage mer

 */



/**

 * Make theme available for translation

 * Translations can be filed in the /languages/ directory

 * If you're building a theme based on Bluewave, use a find and replace

 * to change 'Bluewave' to the name of your theme in all the template files

 */
 
 



 
 
if ( get_magic_quotes_gpc() ) {

    $_POST      = array_map( 'stripslashes_deep', $_POST );
    $_GET       = array_map( 'stripslashes_deep', $_GET );
    $_COOKIE    = array_map( 'stripslashes_deep', $_COOKIE );
    $_REQUEST   = array_map( 'stripslashes_deep', $_REQUEST );
}
load_theme_textdomain( 'mer', TEMPLATEPATH . '/languages' );
require_once ( get_template_directory() . '/Mobile_Detect.php' );

require_once ( get_template_directory() . '/functions-options.php' );
require_once(get_template_directory() . "/Tax-meta-class.php");
require_once(ABSPATH . 'wp-includes/class-wp-image-editor.php');
require_once(get_template_directory() .'/admin/authorizenet.class.php');
$locale = get_locale();

$locale_file = TEMPLATEPATH . "/languages/$locale.php";

if ( is_readable( $locale_file ) )

	require_once( $locale_file );



/**

 * Set the content width based on the theme's design and stylesheet.

 */

if ( ! isset( $content_width ) )

	$content_width = 640; /* pixels */



/**

 * This theme uses wp_nav_menu() in one location.

 */

 register_nav_menus( array(

	'mainmenu' => __( 'Main Menu', 'mer' ),

	'topmenu' => __( 'Top Menu', 'mer' ),

	'footermenu' => __( 'Footer Menu', 'mer' ),

) );

 
add_filter( 'show_admin_bar', '__return_false' );
 
/**

 * Add default posts and comments RSS feed links to head

 */

add_theme_support( 'automatic-feed-links' );

/**

 * Add support for the Aside and Gallery Post Formats

 */

add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );


/**

 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.

 */


add_action( 'show_user_profile', 'mer_user_profile_fields' );
add_action( 'edit_user_profile', 'mer_user_profile_fields' );	
add_action( 'personal_options_update', 'mer_save_profile_fields' );
add_action( 'edit_user_profile_update', 'mer_save_profile_fields' );
	
function mer_init() {
	add_rewrite_tag('%loc%','([^&]+)');
	add_rewrite_tag('%month%','([^&]+)');
	add_rewrite_tag('%topics%','([^&]+)');
	add_rewrite_tag('%conferenceid%','([^&]+)');
	add_rewrite_tag('%cat%','([^&]+)');
	add_rewrite_tag('%msg%','([^&]+)');
	add_rewrite_tag('%switch%','([^&]+)');
	add_rewrite_tag('%pending%','([^&]+)');
	add_rewrite_tag('%review%','([^&]+)');

	if ( is_admin()) {
	$options = get_option( 'mer_theme_options' );
	//var_dump($options);
	add_theme_support( 'post-thumbnails' );
	//set_post_thumbnail_size( 150, 150, true ); 
	
	add_image_size( 'featured-location', $options['thumb_loc_w'],  $options['thumb_loc_h'], true );
	
	 add_image_size( 'slider-image', $options['thumb_semi_w'],  $options['thumb_semi_h'], true );
	 set_post_thumbnail_size( $options['thumb_semi_w'],  $options['thumb_semi_h'] );
	  /*add_image_size( 'schedule-page-image', $options['thumb_sch_h'],  $options['thumb_sch_h'], true );
	   add_image_size( 'course-image', $options['thumb_course_w'],  $options['thumb_course_h'], true );*/
	    add_image_size( 'venue-image', $options['thumb_ven_w'],  $options['thumb_ven_h'], true );
	}
	
	if (! is_admin()) {
		
		wp_enqueue_style('lightbox.css','/wp-content/themes/mer/css/jquery.lightbox-0.5.css', null, '1.0');
		wp_enqueue_script('jquery');
		wp_register_script( 'mer-lightbox', '/wp-content/themes/mer/js/jquery.lightbox-0.5.min.js');
		wp_enqueue_script('mer-lightbox');
	
	}
	
	/////////////////////////// Topic  /////////////////////////////////////////////////////////
	
			setup_topics();
	
	 /////////////////////////// Conference  /////////////////////////////////////////////////////////
	
			setup_conferences();

	//////////////////////////// Registration  ///////////////////////////////////////////////

			setup_registration();

  
  $prefix = '';
  $config = array(
    'id' => 'mer_meta_box',          // meta box id, unique per meta box
    'title' => 'Image Detail',          // meta box title
    'pages' => array('course_photo','venue_photo'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => true,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  $my_meta =  new Tax_Meta_Class($config);
   $my_meta->addImage($prefix.'image',array('name'=> __('Image','mer')));
$my_meta->Finish();
$config = array(
    'id' => 'mer_meta_box',          // meta box id, unique per meta box
    'title' => 'Document Detail',          // meta box title
    'pages' => array('document'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => true,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
   $my_meta =  new Tax_Meta_Class($config);
  
  $my_meta->addFile($prefix.'document',array('name'=> __('Course Document ','mer')));
 $my_meta->Finish();
  
///////////////////////////////// /////////////////////////////////////////////////
	
  
  setup_agenda();

	setup_transaction();
	
	
 /////////////////////////  SIDE BAR          ////////////////////////////////////////////////////////
 register_sidebar( array (

		'name' => __( 'Left Side Bar', 'mer' ),

		'id' => 'leftsidebar',

		'description' => __( 'A widget area for left side', 'mer' ),

		'before_widget' => '',

		'after_widget' => '',

		'before_title' => '<h2 class="widget-title">',

		'after_title' => '</h2>',

	) );

/////////////////////  SIDE BAR          ////////////////////////////////////////////////////////
 register_sidebar( array (

		'name' => __( 'Home Page Slider', 'mer' ),

		'id' => 'homeslider',

		'description' => __( 'A widget area for Home Slider', 'mer' ),

		'before_widget' => '',

		'after_widget' => '',

		'before_title' => '<h2 class="widget-title">',

		'after_title' => '</h2>',

	) );
register_sidebar( array (

		'name' => __( 'Footer Menu', 'mer' ),

		'id' => 'footermenu',

		'description' => __( 'A widget area for footer menu', 'mer' ),

		'before_widget' => '',

		'after_widget' => '',

		'before_title' => '<h2 class="widget-title">',

		'after_title' => '</h2>',

	) );

}//mer init ends here 
add_action( 'init', 'mer_init' );

 function credits_function() {

	require_once ( get_template_directory() . '/admin/credits_list.php' );
}


require_once(get_template_directory() .'/function_extra/utility.php');
require_once(get_template_directory() .'/function_extra/topics_post.php');
require_once(get_template_directory() .'/function_extra/conference_post.php');
require_once(get_template_directory() .'/function_extra/registration_post.php');
require_once(get_template_directory() .'/function_extra/transaction_post.php');
require_once(get_template_directory() .'/function_extra/agenda_post.php');
require_once(get_template_directory() .'/function_extra/cutom_search.php');
require_once(get_template_directory() .'/admin/meta-boxes.php');
require_once(get_template_directory() .'/admin/meta-boxes-function.php');
 require_once(get_template_directory() .'/admin/save_post_custom.php');
add_action( 'add_meta_boxes', 'mer_add_custom_box' );



