<?php
 /////////////////////////// Agenda  /////////////////////////////////////////////////////////
 function setup_agenda(){
	register_post_type( 'agenda',
		array(
			'labels' => array(
				'name' => __( 'Agendas' ),
				'singular_name' => __( 'Agenda' ),
				'singular_name' => _x( 'Agendas', 'Agenda singular name' ),
				'search_items' =>  __( 'Search Agenda' ),
				'all_items' => __( 'All Agendas' ),
				'view_item' => __( 'View Agenda' ),
				'parent_item_colon' => __( 'Parent Agenda:' ),
				'edit_item' => __( 'Edit Agenda' ), 
				'update_item' => __( 'Update Agenda' ),
				'add_new_item' => __( 'Add New Agenda' ),
				'new_item_name' => __( 'New Agenda' ),
				'not_found' => __( 'No course found' ),
				'not_found_in_trash' => __( 'No course found in Trash' ),
				'menu_name' => __( 'Agenda' ),
				'rewrite' => array('slug' => 'agenda') 
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'supports' => array( '', )
		)
	);
	
	//////////////////////////// Transaction  ///////////////////////////////////////////////
add_filter( 'manage_edit-agenda_columns', 'edit_agenda_columns' ) ;
add_action( 'manage_agenda_posts_custom_column', 'manage_agenda_columns', 10, 2 );
add_filter( 'manage_edit-agenda_sortable_columns', 'agenda_sortable_columns' );
/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'edit_agenda_load' );

}

function edit_agenda_columns( $columns ) {
	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
	
		'title' => __( 'Title' ),	
		'__kp_agenda_items_ID' => __( 'Agenda ID' ),
		'__kf_Course_ID' => __( 'Course ID' ),
		'Agenda_Item_Date' => __( 'Agenda Date' ),
		'Agenda_Item_Start_Time' => __( 'Start Time' ),
		'Agenda_Item_End_Time' => __( 'End Time' ),
		'day' => __( 'Day' )
	);

	return $columns;
}


function manage_agenda_columns( $column, $post_id ) {
	global $post;
	
	"";
	
	 

	  $val= get_post_meta( $post_id, $column, true );
	 
	$search = ( isset($_GET["s"]) ) ? sanitize_text_field($_GET["s"]) : false ;
	$search = (string)$search;
	$val=str_replace($search,"<font color='red'><b>$search</b></font>" ,$val) ;
	
	echo  $val;
}


function agenda_sortable_columns( $columns ) {

	 
$columns['__kf_Course_ID'] = '__kf_Course_ID';

 $columns['day'] = 'day';

	return $columns;
}
	
function edit_agenda_load() {
	add_filter( 'request', 'sort_agenda' );
}

/* Sorts the movies. */
function sort_agenda( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'agenda' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			/* Merge the query vars with our custom variables. */
			$arr['meta_key']=$vars['orderby'];
			if($vars['orderby']=='__kf_Course_ID' || $vars['orderby']=='Agenda_Item_Date' )
			$arr['orderby']='meta_value_num';
			 
			$vars = array_merge(
				$vars,$arr
				
			);
		}
	}

	return $vars;
}
?>