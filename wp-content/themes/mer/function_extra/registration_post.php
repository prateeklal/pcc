<?php
function setup_registration(){

	register_post_type( 'registration',
		array(
			'labels' => array(
				'name' => __( 'Registration' ),
				'singular_name' => __( 'Registration' ),
				'singular_name' => _x( 'Registration', 'Registration singular name' ),
				'search_items' =>  __( 'Search Registration' ),
				'all_items' => __( 'All Registration' ),
				'view_item' => __( 'View Registration' ),
				'parent_item_colon' => __( 'Parent Registration:' ),
				'edit_item' => __( 'Edit Registration' ), 
				'update_item' => __( 'Update Registration' ),
				'add_new_item' => __( 'Add New Registration' ),
				'new_item_name' => __( 'New Registration' ),
				'not_found' => __( 'No Registration found' ),
				'not_found_in_trash' => __( 'No Registration found in Trash' ),
				'menu_name' => __( 'Registration' )
			),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('hear_about_seminar'),
		'rewrite' => true,
		'capability_type' => 'post',
		'rewrite' => array('slug' => 'registration') ,
		'supports' => array( ''),
		 
		)
	);
	add_filter( 'manage_edit-registration_columns', 'edit_registration_columns' ) ;
	add_action( 'manage_registration_posts_custom_column', 'manage_registration_columns', 10, 2 );
	add_filter( 'manage_edit-registration_sortable_columns', 'registration_sortable_columns' );
	/* Only run our customization on the 'edit.php' page in the admin. */
	add_action( 'load-edit.php', 'edit_registration_load' );


}


function edit_registration_columns( $columns ) {

	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
		'__kp_Registration_ID' => __( 'Reg ID' ),
		'Course' => __( 'Course' ),
		'Attendee' => __( 'Attendee' ),
		'Registration_Fee' => __( 'Reg Fee' ),
		'Registration_Date' => __( 'Reg Date' ),
		'Registration_Status' => __( 'Status' ) 
		
	);

	return $columns;
}	

function manage_registration_columns( $column, $post_id ) {
	global $post;
	$search = ( isset($_GET["s"]) ) ? sanitize_text_field($_GET["s"]) : false ;
	$search = (string)$search;
	if($column=='Attendee'){
	
	 $user_id=get_post_meta( $post_id, '__kf_Attendee_ID', true );
	 
	$title= get_user_meta( $user_id, 'first_name', true ) . ' ' . get_user_meta( $user_id, 'last_name', true );
		if($title==" "){
				$title="<font color='red'>__kf_Attendee_ID :$user_id has not been uploaded in System .</font>";
			
			} 
  echo  str_replace($search,"<font color='red'><b>$search</b></font>" ,$title) ;


	}else if($column=='__kp_Registration_ID'){
	$v=get_post_meta( $post_id, $column, true );
	echo "<a href='post.php?post=$post_id&action=edit'  class='row-title' title='$title' >".
	   str_replace($search,"<font color='red'><b>$search</b></font>" ,$v )  ."</a>";
	
	
	}else if($column=='Registration_Date'){
	$dt=date('Y-m-d', strtotime(get_post_meta( $post_id, $column, true )));
	echo  str_replace($search,"<font color='red'><b>$search</b></font>" ,$dt) ;
	 
	}
	else if($column=='Date'){
	echo date('Y-m-d', strtotime($post->post_date));
	}
	else if($column=='Course'){
		$cid= get_post_meta( $post_id,'__kf_CourseID', true ) ;
		  $title= $post->post_title  ;
		   $title=str_replace(")","",$title);
		   $title=str_replace("(","<br/><br/>",$title);
			$title=str_replace("Location","<font color='green'><b>Location</b></font>",$title);
			 $title=str_replace("Start Date","<font color='green'><b>Start Date </b></font>",$title);
			 
		 	 if($title=="Course Not Selected"){
				$title="";
			} 
			if($title==""){
				$title="<font color='red'>Course ID :$cid has not been uploaded in System .</font>";
			
			} 
			
			 echo $title;
		 
	}
	else{
			echo  str_replace($search,"<font color='red'><b>$search</b></font>" ,get_post_meta( $post_id, $column, true )) ;

	 
	}
}

function registration_sortable_columns( $columns ) {

	 
$columns['__kf_Attendee_ID'] = '__kf_Attendee_ID';
$columns['__kp_Registration_ID'] = '__kp_Registration_ID'; 
$columns['Registration_Date'] = 'Registration_Date';
$columns['Registration_Status'] = 'Registration_Status';
 


	return $columns;
}

function edit_registration_load() {
	add_filter( 'request', 'sort_registration' );
}

/* Sorts the movies. */
function sort_registration( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'registration' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			/* Merge the query vars with our custom variables. */
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => $vars['orderby'],
					'orderby' => 'meta_value_num'
				)
			);
		}
	}

	return $vars;
} 
?>