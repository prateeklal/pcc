<?php
function setup_topics(){

register_post_type( 'topic',
		array(
			'labels' => array(
				'name' => __( 'Topic' ),
				'singular_name' => _x( 'Topic', 'Topic singular name' ),
				'search_items' =>  __( 'Search Topic' ),
				'all_items' => __( 'All Topics' ),
				'view_item' => __( 'View Topic' ),
				'parent_item_colon' => __( 'Parent Topic:' ),
				'edit_item' => __( 'Edit Topic' ), 
				'update_item' => __( 'Update Topic' ),
				'add_new_item' => __( 'Add New Topic' ),
				'new_item_name' => __( 'New Topic' ),
				'not_found' => __( 'No Topic found' ),
				'not_found_in_trash' => __( 'No Topic found in Trash' ),
				'menu_name' => __( 'Topic' ),
				'rewrite' => array('slug' => 'topic') 
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'supports' => array( '' )
		)
	);	

}
?>