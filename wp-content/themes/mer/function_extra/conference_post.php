<?php
function setup_conferences(){

register_post_type( 'conference',
		array(
			'labels' => array(
				'name' => __( 'Conference' ),
				'singular_name' => _x( 'Conference', 'Conference singular name' ),
				'search_items' =>  __( 'Search Conference' ),
				'all_items' => __( 'All Conferences' ),
				'view_item' => __( 'View Conference' ),
				'parent_item_colon' => __( 'Parent Conference:' ),
				'edit_item' => __( 'Edit Seminar' ), 
				'update_item' => __( 'Update Conference' ),
				'add_new_item' => __( 'Add New Conference' ),
				'new_item_name' => __( 'New Conference' ),
				'not_found' => __( 'No Conference found' ),
				'not_found_in_trash' => __( 'No Conference found in Trash' ),
				'menu_name' => __( 'Conference' ),
				'rewrite' => array('slug' => 'conference') 
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		capabilities=>array('create_posts' => false,
				'edit_post' => 'manage_options',
                'read_post' => 'manage_options',
                'delete_post' => 'manage_options',
                'edit_posts' => 'manage_options',
                'edit_others_posts' => 'manage_options',
                'publish_posts' => 'manage_options',
                'read_private_posts' => 'manage_options'),
		'supports' => array( '',  'thumbnail' )
		)
	);
	

add_filter( 'manage_edit-conference_columns', 'edit_conference_columns' ) ;
add_action( 'manage_conference_posts_custom_column', 'manage_conference_columns', 10, 2 );
add_filter( 'manage_edit-conference_sortable_columns', 'conference_sortable_columns' );
/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'edit_conference_load' );

}

function edit_conference_columns( $columns ) {

	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
		'Course_Topic' => __( 'Course Topic' ),
		'__kp_CourseID' => __( 'Course ID' ),
		'Course_Start_Date' => __( 'Start Date' ),
		'Course_End_Date' => __( 'End Date' ),
		'Course_Location' => __( 'Location' ),
		'Date' => __( 'Date' )
	);

	return $columns;
}


function manage_conference_columns( $column, $post_id ) {
	global $post;
	"";
	if($column=='Course_Topic'){$title= get_post_meta( $post_id, $column, true );

 $val= "<a href='post.php?post=$post_id&action=edit'  class='row-title' title='$title' >$title</a>";


	}
	else if($column=='Date'){
	  $val= date('Y-m-d', strtotime($post->post_date));
	}
	else{

	  $val= get_post_meta( $post_id, $column, true );
	}
	$search = ( isset($_GET["s"]) ) ? sanitize_text_field($_GET["s"]) : false ;
	$search = (string)$search;
	$val=str_replace($search,"<font color='red'><b>$search</b></font>" ,$val) ;
	
	echo  $val;
}


function conference_sortable_columns( $columns ) {

	 
	$columns['__kp_CourseID'] = '__kp_CourseID';
$columns['Course_Start_Date'] = 'Course_Start_Date'; 
$columns['Course_Location'] = 'Course_Location';
 

	return $columns;
}
	
function edit_conference_load() {
	add_filter( 'request', 'sort_conference' );
}

/* Sorts the movies. */
function sort_conference( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'conference' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			/* Merge the query vars with our custom variables. */
			$arr['meta_key']=$vars['orderby'];
			if($vars['orderby']=='__kp_CourseID' || $vars['orderby']=='Course_Start_Date' )
			$arr['orderby']='meta_value_num';
			 
			$vars = array_merge(
				$vars,$arr
				
			);
		}
	}

	return $vars;
}
?>