<?php

function setup_transaction(){

register_post_type( 'transaction',
		array(
			'labels' => array(
				'name' => __( 'Transactions' ),
				'singular_name' => __( 'Transaction' ),
				'singular_name' => _x( 'Transaction', 'Transaction singular name' ),
				'search_items' =>  __( 'Search Transaction' ),
				'all_items' => __( 'All Transaction' ),
				'view_item' => __( 'View Transaction' ),
				'parent_item_colon' => __( 'Parent Transaction:' ),
				'edit_item' => __( 'Edit Transaction' ), 
				'update_item' => __( 'Update Transaction' ),
				'add_new_item' => __( 'Add New Transaction' ),
				'new_item_name' => __( 'New Transaction' ),
				'not_found' => __( 'No Transaction found' ),
				'not_found_in_trash' => __( 'No Transaction found in Trash' ),
				'menu_name' => __( 'Transactions' )
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'rewrite' => array('slug' => 'transaction') ,
		'supports' => array( ''),
		 
		)
	);

	add_filter( 'manage_edit-transaction_columns', 'edit_transaction_columns' ) ;
	add_action( 'manage_transaction_posts_custom_column', 'manage_transaction_columns', 10, 2 );
 	add_filter( 'manage_edit-transaction_sortable_columns', 'transaction_sortable_columns' );
	add_action( 'load-edit.php', 'edit_transaction_load' );
}

function edit_transaction_columns( $columns ) {

	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
		'__kp_Transaction_ID' => __( 'Transaction ID' ),
		'Name' => __( 'Name' ),		
		
		'__kf_Registration_ID' => __( 'Registration ID' ),
		'Registration_Fee' => __( 'Fee' ),
		'Registration_Fee_Type' => __( 'Fee Type' ),
		'Transaction_Amount' => __( 'Amount' ),
		'Transaction_Category' => __( 'Category' ),
		
			
		'Date' => __( 'Date' )
	);

	return $columns;
}	

function manage_transaction_columns( $column, $post_id ) {
global $post;
$search = ( isset($_GET["s"]) ) ? sanitize_text_field($_GET["s"]) : false ;
	$search = (string)$search;
	$val="";

 $registration_id=get_post_meta($post_id,'__kf_Registration_ID',true);
   
   
$Transaction_Category=get_post_meta( $post_id, 'Transaction_Category', true );
$CheckTransaction_Category=$Transaction_Category;	
$sup="";
$postslist=array();
	if ($CheckTransaction_Category=='Registration PAID CC' || $CheckTransaction_Category=='Registration CREDIT USED'){
			 $args2 = array(
				'orderby'        => 'ID',
				'order'            => 'DESC',
				'post_type' => 'transaction', // This is where you should put your Post Type 
				'post_status'        => 'publish',
				'posts_per_page'    => -1,
				'meta_query' => array(
									array(
											'key' => '__kf_Registration_ID',
											'value' => $registration_id,
											'compare' => '='
											),
											array(
											'key' => 'Transaction_Category',
											'value' => array('Cancellation CREDIT','Registration REFUND','Cancellation REFUND'),
											'compare' => 'in'
											) 
								)
				
			);
		   
		 $postslist=get_posts($args2);
		 
		 
 }
	 
	



	if($column=='Name'){$title= get_post_meta( $post_id, 'First_Name', true ) .' ' . get_post_meta( $post_id, 'Last_Name', true ) ;

  $val=  str_replace($search,"<font color='red'><b>$search</b></font>" ,$title) ;


	}else if($column=='__kp_Transaction_ID'){
	$id=get_post_meta( $post_id, $column, true );
	
	 
	$sup="";	
	$action.="<span class='edit'>
													<a title='Edit this transaction' href='post.php?post=$post_id&action=edit'>Edit</a>
												</span>
											";
						
						if(count($postslist)>0){
							
						foreach($postslist as $lpost){
							 $lid =$lpost->ID;
							 $tcat=get_post_meta( $lid, 'Transaction_Category', true );
								if ($tcat=="Registration REFUND"){
									 
									$sup.="<sup  style='background:#000;padding:2px;border-radius:5px;margin-right:2px'>
											<a title='Check Registration REFUND issued against this transaction' href='post.php?post=$lid&action=edit'>
											<font color='#fff'>RRf</font></a>
										</sup>";
								}
								if ($tcat=="Cancellation REFUND"){
									 
										 
										$sup.="<sup style='background:#000;padding:2px;border-radius:5px;margin-right:2px'>
												<a title='Check Cancellation Refund issued against this transaction' href='post.php?post=$lid&action=edit'>
													<font color='#fff'>CRf</font></a>
											</sup>";
									}
								
								if ($tcat=="Cancellation CREDIT"){
									 
									
									$sup.="<sup style='background:#000;padding:2px;border-radius:5px;margin-right:2px'>
											<a title='Check Cancellation CREDIT issued against this transaction' href='post.php?post=$lid&action=edit'>
												<font color='#fff'>CCr</font></a>
										</sup>";
								}
								 
							 }
							 
						 		$action="<div class='row-actions'>$action</div>";
						 }
						   
						 elseif($Transaction_Category=='Cancellation FEE' ){
						 
							 $action="<div class='row-actions'>
										<span class='edit'>
											<a title='Edit this transaction' href='post.php?post=$post_id&action=edit'>Edit</a>
										</span> | 
										<span class='edit'>
											<a title='Refund this Cancellation FEE' href='post.php?post=$post_id&action=edit&type=refundfee'>Refund</a>
										</span>
									</div>";
							}elseif($Transaction_Category=='Registration PAID CC' || 
							$Transaction_Category=='Registration CREDIT USED' || 
							$Transaction_Category=='Registration PAID CHECK' )
							{
						 
							 $action="<div class='row-actions'>
										<span class='edit'>
											<a title='Edit this transaction' href='post.php?post=$post_id&action=edit'>Edit</a></span> | 
										<span class='edit'>
										<a title='Cancel this transaction' href='post.php?post=$post_id&action=edit&type=cancel'>Cancel</a></span>  
									</div>";
							}else{
							
								$action="<div class='row-actions'>$action</div>";
							} 
	
	  $val=   str_replace($search,"<font color='red'><b>$search</b></font>" ,$id).  $sup .$action ;

	 
	}else if($column=='__kf_Registration_ID'){
	$rid=get_post_meta( $post_id, $column, true );
	
	  $val= "<a href='edit.php?s=$rid&post_status=all&post_type=registration&action=-1&m=0&paged=1&mode=list&action2=-1'  class='row-title' title='$title' >". str_replace($search,"<font color='red'><b>$search</b></font>" ,$rid)."</a>";

	 
	}else if($column=='Registration_Fee'){
		 $args3= array(
			'orderby'        => 'ID',
			'order'            => 'DESC',
			'post_type' => 'registration', // This is where you should put your Post Type 
			'post_status'        => 'publish',
			'posts_per_page'    => -1,
			'meta_query' => array(
								array(
										'key' => '__kp_Registration_ID',
										'value' => $registration_id,
										'compare' => '='
										)
							)
		);
		 $tpostslist=get_posts($args3);
		 $registrationpostid=$tpostslist[0]->ID;
		$val= get_post_meta($registrationpostid,'Registration_Fee',true);
		if($val==""){
		 $val="<font color='red'>Registration could not created due to unavailbility of course id in system</font>";
		}else{
		  $val= "<a href='post.php?post=$registrationpostid&action=edit'  class='row-title' title='$title' > $val</a>";

		}
			
		 		 
	 }
	else if($column=='Transaction_Category'){
	
	$Transaction_Category2=str_replace(" ","",$Transaction_Category);
	 if($Transaction_Category2=="RegistrationCREDITUSED"){
		   $Transaction_Category= "<font color='blue'>$Transaction_Category</font>";
		 $val= str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 }
		else if($Transaction_Category2=="RegistrationREFUND" || $Transaction_Category2=="CancellationREFUND"){
		   $Transaction_Category= "<font color='orange'>$Transaction_Category</font>";
		 $val= str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 }
	 else if($Transaction_Category2=="CourtesyCREDIT" || $Transaction_Category2=="LoyaltyCredit" || $Transaction_Category2=="CancellationCREDIT" ){
		   $Transaction_Category= "<font color='green'>$Transaction_Category</font>";
		 $val= str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 }
		 else if($Transaction_Category2=="RegistrationREFUND"){
		   $Transaction_Category= "<font color='brown'>$Transaction_Category</font>";
		 $val= str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 	}
	 else{
		 $val= str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
		}
	}
	
	else if($column=='Date'){
	$dt=date('Y-m-d', strtotime($post->post_date));
	$val= str_replace($search,"<font color='red'><b>$search</b></font>" ,$dt) ;
	}
	else{
		$val=  str_replace($search,"<font color='red'><b>$search</b></font>" ,get_post_meta( $post_id, $column, true )) ;
	}
	
	
	if(count($postslist)>0){
	$val="<div style='color:#ccc;' >$val</div>";
	}
	echo  $val;
}
	

function transaction_sortable_columns( $columns ) {

	 
	$columns['__kp_Transaction_ID'] = '__kp_Transaction_ID';
$columns['__kf_Registration_ID'] = '__kf_Registration_ID'; 
$columns['Transaction_Category'] = 'Transaction_Category';
 

	return $columns;
}


/* Only run our customization on the 'edit.php' page in the admin. */
//add_action( 'load-edit.php', 'edit_transaction_load' );

function edit_transaction_load() {
	add_filter( 'request', 'sort_transaction' );
}

/* Sorts the movies. */
function sort_transaction( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'transaction' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			 
			/* Merge the query vars with our custom variables. */
			$arr['meta_key']=$vars['orderby'];
			if($vars['orderby']=='__kp_Transaction_ID' || $vars['orderby']=='__kf_Registration_ID' )
			$arr['orderby']='meta_value_num';
			
			$vars = array_merge(
				$vars,$arr
				
			);
		}
	}

	return $vars;

}




?>