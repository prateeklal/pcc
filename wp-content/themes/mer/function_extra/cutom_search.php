<?php function mer_post_types_in_search($query) {
	if($query->is_search && is_admin() ) {
	    $posttype=  $query->query_vars['post_type'];
	 $custom_fields = array();
	 
	   
	 if($posttype=='registration' ) {
 
	
	 $meta_query = array('relation' => 'OR'); 
	
	array_push($meta_query,array(
		'key' => '__kp_Registration_ID',
		'value' =>$query->query_vars['s'], 
		'compare' => '=='
		),
		array(
		'key' => '__kf_Attendee_ID',
		'value' =>$query->query_vars['s'], 
		'compare' => '=='
		)		
		) ;
/* $query->set('meta_query', array(
		'key' => '__kp_Registration_ID',
		'value' =>$query->query_vars['s'], 
		'compare' => '=='
		) );
 */
	 
	
	
	 }
/*	 echo "<pre>";
	  var_dump($query);
	echo "</pre>";*/
	
	}
	return $query;
}

add_filter('posts_groupby', 'custom_posts_groupby' );
function custom_posts_groupby( $groupby ) {
    global $wpdb;
    if( is_search()  ) {

        $groupby = "$wpdb->posts.ID";
		
		/*$arr=array('CC_Num','CC_Type','First_Name','Last_Name','Note','Registration_Fee_Type','__kf_Registration_ID','Conf_Start_Date','Transaction_Amount','Transaction_Category','__kp_Transaction_ID','Transaction_Source','AN_Transaction_ID','AN_CC_Number','AN_Transaction_Amount','AN_CC_Type','create_date','Record_correction','Cancel_Date','Credit_Expire_Date','Is_Credit_Used');
		//$arr=array( '__kp_Transaction_ID' );
		$str=" $wpdb->postmeta.meta_key = '__kf_Attendee_ID' ";
		foreach ($arr as $a){
		$groupby .= " , $wpdb->postmeta.meta_key = '$a'"; 
		}*/
    }
    return $groupby;
}
add_filter('posts_join', 'custom_posts_join' );

function custom_posts_join($join){
    global $wpdb;
	global $post_type ;
	 
    if( is_search() && $post_type=='registration' ) {

        $join .= " LEFT JOIN $wpdb->postmeta  ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";
		  $join .= " LEFT JOIN $wpdb->users  ON $wpdb->users.ID = $wpdb->postmeta.meta_value  and $wpdb->postmeta.meta_key='__kf_Attendee_ID'";
		 
    }
	 if( is_search() && $post_type=='transaction' ) {

        $join .= " LEFT JOIN $wpdb->postmeta  ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";
		   $join .= " LEFT JOIN $wpdb->users  ON $wpdb->users.ID = $wpdb->postmeta.meta_value  and $wpdb->postmeta.meta_key='__kf_Attendee_ID'";
		    
		
    }
	
	   if( is_search() && ($post_type=='conference' ||   $post_type=='agenda') ) {

        $join .= " LEFT JOIN $wpdb->postmeta  ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";
		
		 
    }
	  remove_filter( 'posts_join', 'custom_posts_join' );
    return $join;
}
add_filter( 'posts_where', 'custom_where' );
function custom_where( $where = '' ){
    global $wpdb;
	global $post_type ;
	 
 	$search = ( isset($_GET["s"]) ) ? sanitize_text_field($_GET["s"]) : false ;
	$search = (string)$search;
	 
	if ($post_type=='registration'){
			$replace="AND ((($wpdb->posts.post_title";
		$replace_with="AND ((
		 	 ( ($wpdb->postmeta.meta_key = 'Registration_Status' OR $wpdb->postmeta.meta_key = 'Registration_Date' OR $wpdb->postmeta.meta_key = '__kp_Registration_ID'  ) AND $wpdb->postmeta.meta_value lIKE '%$search%') OR
			($wpdb->postmeta.meta_key='__kf_Attendee_ID' and $wpdb->users.display_name lIKE '%$search%' )  OR ($wpdb->posts.post_title";
	
	 $where =str_replace($replace,$replace_with ,$where);
    	    	//$where .= " " ;
		 
		 		
	}
	
	
		if ($post_type=='transaction'){
		$arr=array('CC_Num','CC_Type','First_Name','Last_Name','Note','Registration_Fee_Type','__kf_Registration_ID','Conf_Start_Date','Transaction_Amount','Transaction_Category','__kp_Transaction_ID','Transaction_Source','AN_Transaction_ID','AN_CC_Number','AN_Transaction_Amount','AN_CC_Type','create_date','Record_correction','Cancel_Date','Credit_Expire_Date','Is_Credit_Used');
		//$arr=array( '__kp_Transaction_ID' );
		$str=" $wpdb->postmeta.meta_key = '__kf_Attendee_ID' ";
		foreach ($arr as $a){
		$str .= " OR $wpdb->postmeta.meta_key = '$a'"; 
		}
		 
		
		$replace="AND ((($wpdb->posts.post_title";
		$replace_with="AND ((
		 	 ( ($str ) AND $wpdb->postmeta.meta_value lIKE '%$search%') OR
			($wpdb->postmeta.meta_key='__kf_Attendee_ID' and $wpdb->users.display_name lIKE '%$search%' )  OR ($wpdb->posts.post_title";
	
	 $where =str_replace($replace,$replace_with ,$where);
    	    	//$where .= " " ;
		 
		 	 
	}
	
	if ($post_type=='conference'){
		$arr=array('Course_Topic','Course_Location','Course_Start_Date','Course_End_Date');
		//$arr=array( '__kp_Transaction_ID' );
		$str=" $wpdb->postmeta.meta_key = '__kp_CourseID' ";
		foreach ($arr as $a){
		$str .= " OR $wpdb->postmeta.meta_key = '$a'"; 
		}
		 
		
		$replace="AND ((($wpdb->posts.post_title";
		$replace_with="AND ((
		 	 ( ($str ) AND $wpdb->postmeta.meta_value lIKE '%$search%')  OR ( $wpdb->posts.ID  lIKE '%$search%' or $wpdb->posts.post_date lIKE '%$search%' or $wpdb->posts.post_title";
	
	 $where =str_replace($replace,$replace_with ,$where);
    	    	//$where .= " " ;
		 
		 	 
	} 
	 
	if ($post_type=='agenda'){
		$arr=array('__kf_Course_ID','Agenda_Item_Start_Time','Agenda_Item_End_Time','Course_End_Date');
		//$arr=array( '__kp_Transaction_ID' );
		$str=" $wpdb->postmeta.meta_key = '__kp_agenda_items_ID' ";
		foreach ($arr as $a){
		$str .= " OR $wpdb->postmeta.meta_key = '$a'"; 
		}
		 
		
		$replace="AND ((($wpdb->posts.post_title";
		$replace_with="AND ((
		 	 ( ($str ) AND $wpdb->postmeta.meta_value lIKE '%$search%')  OR ( $wpdb->posts.ID  lIKE '%$search%' or $wpdb->posts.post_date lIKE '%$search%' or $wpdb->posts.post_title";
	
	 $where =str_replace($replace,$replace_with ,$where);
    	    	//$where .= " " ;
		 
		 	 
	} 
	
	
	
	
	 
    remove_filter( 'posts_where', 'custom_where' );

    return $where;
 
} // custom_where



 add_filter('pre_get_posts', 'mer_post_types_in_search');
add_filter('posts_orderby', 'edit_posts_orderby');

function edit_posts_orderby($orderby_statement) {
  global $wpdb;
	 $orderby_statement = "  ID DESC";
	return $orderby_statement;
}

 ?>