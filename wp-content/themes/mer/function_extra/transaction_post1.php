<?php

function setup_transaction(){

register_post_type( 'transaction',
		array(
			'labels' => array(
				'name' => __( 'Transactions' ),
				'singular_name' => __( 'Transaction' ),
				'singular_name' => _x( 'Transaction', 'Transaction singular name' ),
				'search_items' =>  __( 'Search Transaction' ),
				'all_items' => __( 'All Transaction' ),
				'view_item' => __( 'View Transaction' ),
				'parent_item_colon' => __( 'Parent Transaction:' ),
				'edit_item' => __( 'Edit Transaction' ), 
				'update_item' => __( 'Update Transaction' ),
				'add_new_item' => __( 'Add New Transaction' ),
				'new_item_name' => __( 'New Transaction' ),
				'not_found' => __( 'No Transaction found' ),
				'not_found_in_trash' => __( 'No Transaction found in Trash' ),
				'menu_name' => __( 'Transactions' )
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'rewrite' => array('slug' => 'transaction') ,
		'supports' => array( ''),
		 
		)
	);

	add_filter( 'manage_edit-transaction_columns', 'edit_transaction_columns' ) ;
	add_action( 'manage_transaction_posts_custom_column', 'manage_transaction_columns', 10, 2 );
 	add_filter( 'manage_edit-transaction_sortable_columns', 'transaction_sortable_columns' );
	add_action( 'load-edit.php', 'edit_transaction_load' );
}

function edit_transaction_columns( $columns ) {

	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
		'__kp_Transaction_ID' => __( 'Transaction ID' ),
		'Name' => __( 'Name' ),		
		'__kf_Attendee_ID' => __( 'Attendee ID' ),
		'__kf_Registration_ID' => __( 'Registration ID' ),
		'Registration_Fee_Type' => __( 'Fee Type' ),
		'Transaction_Amount' => __( 'Amount' ),
		'Transaction_Category' => __( 'Category' ),
		
			
		'Date' => __( 'Date' )
	);

	return $columns;
}	

function manage_transaction_columns( $column, $post_id ) {

$search = ( isset($_GET["s"]) ) ? sanitize_text_field($_GET["s"]) : false ;
	$search = (string)$search;


	global $post;
	if($column=='Name'){$title= get_post_meta( $post_id, 'First_Name', true ) .' ' . get_post_meta( $post_id, 'Last_Name', true ) ;

  echo  str_replace($search,"<font color='red'><b>$search</b></font>" ,$title) ;


	}else if($column=='__kp_Transaction_ID'){
	$id=get_post_meta( $post_id, $column, true );
	
	echo "<a href='post.php?post=$post_id&action=edit'  class='row-title' title='$title' >". str_replace($search,"<font color='red'><b>$search</b></font>" ,$id)."</a>";

	 
	}
	else if($column=='Transaction_Category'){
	$Transaction_Category=get_post_meta( $post_id, $column, true );
	$Transaction_Category2=str_replace(" ","",$Transaction_Category);
	 if($Transaction_Category2=="RegistrationCREDITUSED"){
		   $Transaction_Category= "<font color='blue'>$Transaction_Category</font>";
		 echo str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 }
		else if($Transaction_Category2=="RegistrationREFUND" || $Transaction_Category2=="CancellationREFUND"){
		   $Transaction_Category= "<font color='orange'>$Transaction_Category</font>";
		 echo str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 }
	 else if($Transaction_Category2=="CourtesyCREDIT" || $Transaction_Category2=="LoyaltyCredit" || $Transaction_Category2=="CancellationCREDIT" ){
		   $Transaction_Category= "<font color='green'>$Transaction_Category</font>";
		 echo str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 }
		 else if($Transaction_Category2=="RegistrationREFUND"){
		   $Transaction_Category= "<font color='brown'>$Transaction_Category</font>";
		 echo str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
	 	}
	 else{
		 echo str_replace($search,"<font color='red'><b>$search</b></font>" ,$Transaction_Category) ;
		}
	}
	
	else if($column=='Date'){
	$dt=date('Y-m-d', strtotime($post->post_date));
	echo str_replace($search,"<font color='red'><b>$search</b></font>" ,$dt) ;
	}
	else{

	echo  str_replace($search,"<font color='red'><b>$search</b></font>" ,get_post_meta( $post_id, $column, true )) ;
	}
}
	

function transaction_sortable_columns( $columns ) {

	 
	$columns['__kp_Transaction_ID'] = '__kp_Transaction_ID';
$columns['__kf_Attendee_ID'] = '__kf_Attendee_ID'; 
$columns['Transaction_Category'] = 'Transaction_Category';
 

	return $columns;
}


/* Only run our customization on the 'edit.php' page in the admin. */
//add_action( 'load-edit.php', 'edit_transaction_load' );

function edit_transaction_load() {
	add_filter( 'request', 'sort_transaction' );
}

/* Sorts the movies. */
function sort_transaction( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'transaction' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			 
			/* Merge the query vars with our custom variables. */
			$arr['meta_key']=$vars['orderby'];
			if($vars['orderby']=='__kp_Transaction_ID' || $vars['orderby']=='__kf_Attendee_ID' )
			$arr['orderby']='meta_value_num';
			
			$vars = array_merge(
				$vars,$arr
				
			);
		}
	}

	return $vars;

}




?>