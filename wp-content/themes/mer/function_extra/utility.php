<?php 

function is_edit_page($new_edit = null){
    global $pagenow;
    //make sure we are on the backend
    if (!is_admin()) return false;


    if($new_edit == "edit")
        return in_array( $pagenow, array( 'post.php',  ) );
    elseif($new_edit == "new") //check for new post page
        return in_array( $pagenow, array( 'post-new.php' ) );
    else //check for either new or edit
        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
}


function mer_AjaxWrapper_callback(){
$prefix='mer_';
$function=$prefix.$_POST['FUNCTION'];
 $objs = $_POST;
  unset( $objs['FUNCTION']);
  unset( $objs['action']);
  $pramas=array();
  
                   foreach ( $objs as $k=>$v)
                {
				
                     $pramas[] = $v ;
                }

	$str=call_user_func_array($function,$pramas );
	
	header('Content-Type: application/json');
	echo $str;
	die();
}
add_action( 'wp_ajax_mer_data_callback', 'mer_data_callback' );

add_action( 'wp_ajax_nopriv_mer_data_callback', 'mer_data_callback' );

function mer_data_callback() {

	mer_AjaxWrapper_callback();
}
function mer_date_compare($a, $b)
						{
							$t1 = strtotime($a['sort_date']); //$a['datetime'] sorting column name
							$t2 = strtotime($b['sort_date']);
							return $t1 - $t2;
						}    
 function mer_check_email($email,$id) {

	  global  $wpdb;   
	 
 $tbl= $wpdb->prefix."users";
   $sql="SELECT ID FROM  $tbl where user_email='$email' " ;
 $result=0;
 $rowid = $wpdb->get_var($sql);
 
 if($rowid!=""){
 		if($rowid!=$id){
		 $result=$rowid;
		 }
 
 }
 
 $str = "{\"result\" : \"" . 0 . "\",\"content\" : \"" . $result . "\"}";
return $str;
}
 
 
 function mer_check_username($uname,$id) {

	  global  $wpdb;   
	 
 $tbl= $wpdb->prefix."users";
 
  $sql="SELECT ID FROM  $tbl where user_login='$uname' " ;
 $result=0;
 $rowid = $wpdb->get_var($sql);
 
 if($rowid!=""){
 		if($rowid!=$id){
		 $result=$rowid;
		 }
 
 }
 
 $str = "{\"result\" : \"" . 0 . "\",\"content\" : \"" . $result . "\"}";
return $str;
}

function mer_page_menu_args($args) {

	$args['show_home'] = true;

	return $args;

}
add_filter( 'wp_page_menu_args', 'mer_page_menu_args' );

function conference_date($sdate,$edate) {
$sdateD= date("d",  strtotime($sdate));
$sdateM= date("M",  strtotime($sdate));	
$sdateY= date("Y",  strtotime($sdate));
	
$edateD= date("d",  strtotime($edate));
$edateM= date("M",  strtotime($edate));	
$edateY= date("Y",  strtotime($edate));

$date='';
	if($sdateY==$edateY){
		if($sdateM==$edateM)
			{$date="$sdateM $sdateD-$edateD, $sdateY";}
		else
			{$date="$sdateM $sdateD-$edateM $edateD,$sdateY";}
	}else 
	{
		$date="$sdateM $sdateD-$edateM $edateD, $edateY";
		
	}

return $date;
}

 

function custom_permalink($permalink) {
		 $permalink_structure = get_option('permalink_structure');
		 if( $permalink_structure){
		 $permalink= str_replace('/?','/',$permalink);	
		 $permalink= str_replace('?','/',$permalink);
		 $permalink= str_replace('=','/',$permalink);
    	 $permalink= str_replace('&','/',$permalink);
		 }
     return $permalink;

};

function add_rewrite_rules($rules) {
$aNewRules = array('register/conference/([^/]+)/?$' => 'index.php?pagename=register&conferenceid=$matches[1]');
 $rules = $aNewRules + $rules;
$aNewRules = array('payment/conference/([^/]+)/pending/([^/]+)/?$' => 'index.php?pagename=payment&conferenceid=$matches[1]&pending=$matches[2]');
 $rules = $aNewRules + $rules;


$aNewRules = array('location/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?pagename=location&loc=$matches[1]&month=$matches[3]-$matches[2]');
$rules = $aNewRules + $rules; 

$aNewRules = array('location/([^/]+)/([^/]+)/?$' => 'index.php?&pagename=location&month=$matches[2]-$matches[1]');
$rules = $aNewRules + $rules;

/* $aNewRules = array('location/month/([^/]+)/?$' => 'pagename=location&index.php?month=$matches[1]');
$rules = $aNewRules + $rules; 

 $aNewRules = array('location/([^/]+)/month/([^/]+)/?$' => 'index.php?pagename=location&loc=$matches[1]&month=$matches[2]');
$rules = $aNewRules + $rules; */



 $aNewRules = array('topics/([^/]+)/?$' => 'index.php?pagename=topics&topics=$matches[1]');
$rules = $aNewRules + $rules; 


$aNewRules = array('schedule/([^/]+)/([^/]+)/?$' => 'index.php?pagename=schedule&month=$matches[2]-$matches[1]');

$aRules = $aNewRules + $rules;



/* echo "<pre>";
 print_r($aRules);
echo "</pre>";*/ 
 return $aRules; 

}
 
// hook add_rewrite_rules function into rewrite_rules_array
add_filter('rewrite_rules_array', 'add_rewrite_rules');

 //wp_page_menu('show_home=1&include=9999');  

function array_sort_by_column(&$array, $column, $direction = SORT_ASC) {
    $reference_array = array();

    foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
    }

    array_multisort($reference_array, $direction, $array);
}
/**

 * Register  area and update sidebar with default widgets

 */
    add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );  
    function custom_image_sizes_choose( $sizes ) {  
        $custom_sizes = array(  
            'slider-image' => 'Home Slider Image' ,
			 'featured-location' => 'Featured Location Image' ,
			  'schedule-page-image' => 'Schedule Page Image' ,
			   'venue-image' => 'Venue Image' , 
			    'course-image' => 'Course Image' , 
        );
		
        return array_merge( $sizes, $custom_sizes );  
    }  
function my_remove_meta_boxes() {

 add_menu_page( 'Credits', 'User Credits', 'edit_others_posts', 'credits', 'credits_function', '' , 25 ); 

  //add_menu_page( 'Credits', 'User Credits', 'manage_options', 'themes.php?type=credit&amp;page=theme_options', '', '', .6 );


//if(!current_user_can('administrator')) {
	
	
	/* remove_meta_box('course_photodiv', 'course', 'side');
 	remove_meta_box('venue_photodiv', 'venue', 'side');
 remove_meta_box('venue_photodiv', 'featured', 'side');*/
 
	
remove_meta_box('conferencediv', 'seminar', 'side');	
  remove_meta_box('user_titlediv', 'user', 'side');
  remove_meta_box('user_titlediv', 'registration', 'side');
  
  
 
  remove_meta_box('hear_about_seminardiv', 'registration', 'side');
  
  remove_meta_box('degreediv', 'user', 'side');
  remove_meta_box('degreediv', 'registration', 'side');
 
   remove_meta_box('agenda_itemdiv', 'agenda', 'side');
  
  
  
  remove_meta_box('specialitydiv', 'user', 'side');
  remove_meta_box('specialitydiv', 'registration', 'side');
  
  
  
  remove_meta_box('activitydiv', 'user', 'side');
  
  remove_meta_box('like_informationdiv', 'user', 'side');
 
 
//}
 
}
	function post_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)){
			if(!isset($_GET['paged']))
			{
				$paged =$_GET['paged']  ; 
			}else{
			 $paged = 1;
			}
	   }

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}
add_action( 'admin_menu', 'my_remove_meta_boxes' );

add_action("admin_notices", "show_names");
 function show_names(){
	// $_SESSION['my_admin_notices']='oops';
	 $msg= get_transient( 'my_admin_notices' );
	 if($msg !=''){
	 echo '<div class="error">
       <p>'.$msg.'</p>
    </div>';
	 delete_transient( 'my_admin_notices' );
	 }
	// echo "User Name already";
}

 function monthsBetween($startDate, $endDate) {
    $retval = "";

    // Assume YYYY-mm-dd - as is common MYSQL format
    $splitStart = explode('-', $startDate);
    $splitEnd = explode('-', $endDate);

    if (is_array($splitStart) && is_array($splitEnd)) {
        $difYears = $splitEnd[0] - $splitStart[0];
        $difMonths = $splitEnd[1] - $splitStart[1];
        $difDays = $splitEnd[2] - $splitStart[2];

        $retval = ($difDays > 0) ? $difMonths : $difMonths - 1;
        $retval += $difYears * 12;
    }
    return $retval;
}
  function the_breadcrumb() {



	if (!is_home()) {

	

		echo '<a href="';

		echo get_option('home');

		echo '">';

		bloginfo('name');

		echo "</a> / ";

		if (is_category() || is_single()) {

			the_category('title_li=');

			if (is_single()) {

				echo " / ";

				the_title();

			}

		} elseif (is_page()) {

			echo the_title();

		}

	}

} 

   function current_page_url() {
	$pageURL = 'http';
	if( isset($_SERVER["HTTPS"]) ) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}
	
	function updateData($tablename, $input_array,$where) {
        $query = "update $tablename set ";
		$fields="";
		foreach ($input_array as $k => $v) {
		$v=GetSQLValueString($v,"text"); 
		
			$fields .= "`$k` =$v,";
		
		}
		
		
		$fields = substr($fields,0,strlen($fields)-1);
		
		$query .= $fields ;
		$condition ='';
		
		//////////////////////  where /////////////////////////
		if(is_array($where)){
			foreach($where as  $key => $value ){
			
			
					if  ($condition==''){
					$condition="$key=". GetSQLValueString($value,"text"); 
					}else{
					 $condition.=" and $key=". GetSQLValueString($value,"text"); 
					 }
			}
			$where=$condition;
			 $query .=" where $where";
		}

	 return  $query;
            
    }

	function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
	{
	  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	
	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
	
	  switch ($theType) {
		case "text":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "''";
		  break;    
		case "long":
		case "int":
		  $theValue = ($theValue != "") ? intval($theValue) : "1";
		  break;
		case "double":
		  $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "1";
		  break;
		case "date":
		  $theValue = ($theValue != "") ? "'" . $theValue . "'" : "1";
		  break;
		case "defined":
		  $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
		  break;
	  }
	  return $theValue;
	}

	
?>