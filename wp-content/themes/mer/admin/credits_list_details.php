<?php
global $wpdb;

$id=$_GET[uid];



  //  $posts_per_page=$options['post_per_page'];
	$loop=0;
	 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args =array(
				'orderby'        => 'ID',
				'order'            => 'DESC',
				'post_type' => 'transaction', // This is where you should put your Post Type 
				'post_status'        => 'publish',
				'posts_per_page'    => -1,
				'meta_query' => array(
									array(
											'key' => '__kf_Attendee_ID',
											'value' => $id,
											'compare' => '='
											),
											array(
											'key' => 'Transaction_Category',
											'value' => array('Cancellation CREDIT','Registration REFUND','Cancellation REFUND','Registration CREDIT USED'),
											'compare' => 'in'
											) 
								)
				
			);
    
$the_query = new WP_Query( $args );
//var_dump($the_query);
     $total=$the_query->max_num_pages;;

	 
		  
		
		?><table class="wp-list-table widefat fixed posts" width="100%" >
        <thead>
        <tr>
         <th align="left" valign='top'>Date</th>
        <th align="left" valign='top'>User</th>
      
        <th align="left" valign='top'>Conference</th>
        <th align="left" valign='top'>Conference Location</th>
        <th align="left" valign='top'>Conference Date</th>
        <th align="right" valign='top'>Fee</th>
        <th align="right" valign='top'>Amount</th>
         <th align="left" valign='top'>Transaction Type</th>
        
 
  </tr>
  </thead> <tbody>
        <?php
		$i=0;
	  while ( $the_query->have_posts()   ):
	  $alt='';
	  if($i==0){
	  $i=1;
	   $alt='alternate';
	  }else{
	  	$i=0;
	   $alt='';
	  }
	$the_query->next_post();
	$id= $the_query->post->ID;
	$post_date= $the_query->post->post_date;
	$cancel_date= $the_query->post->post_modified;
	$registration_id=get_post_meta($id,'__kf_Registration_ID',true);
	$transaction_amount=get_post_meta($id,'Transaction_Amount',true);
	 $feetype=get_post_meta($id,'Registration_Fee_Type',true)   ;
	  $category=get_post_meta($id,'Transaction_Category',true)   ;
	 $__kf_Attendee_ID=get_post_meta($id,'__kf_Attendee_ID',true)   ;
	 $CC_Num=get_post_meta($id,'CC_Num',true);
	 $__kp_Transaction_ID=get_post_meta($id,'__kp_Transaction_ID',true);
	//GET REGISTRATION POST ID
	  $sql = "SELECT p.ID FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						$lc_prt1
						WHERE p.post_type = 'registration'
						AND p.post_status = 'publish'
						AND pm1.meta_key = '__kp_Registration_ID'
						AND pm1.meta_value = '$registration_id'
						$lc_prt2 						
						";
					$fregistrationpostidposts= $wpdb->get_row($sql );
	 $registrationpostid=$fregistrationpostidposts->ID;
	 
	  $course=get_post_meta($registrationpostid,'__kf_CourseID',true);
	$Registration_Date=get_post_meta($registrationpostid,'Registration_Date',true);
	 $registration_fee=get_post_meta($registrationpostid,'Registration_Fee',true);
	//GET REGISTRATION CONFERENCE POST ID
	  $sql = "SELECT p.ID FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						$lc_prt1
						WHERE p.post_type = 'conference'
						AND p.post_status = 'publish'
						AND pm1.meta_key = '__kp_CourseID'
						AND pm1.meta_value = '$course'
						$lc_prt2 						
						";
					$fposts= $wpdb->get_row($sql );
					
	 $postid=$fposts->ID;
	 $venue=get_post_meta($postid,'Course_Venue',true);
	 	$sdate=get_post_meta($postid, 'Course_Start_Date',true);
			$edate=get_post_meta($postid, 'Course_End_Date',true);
	
		   $display_date= conference_date($sdate,$edate);
		$location=get_post_meta($postid, 'Course_Location',true);
	
	 $link1= get_permalink($postid);
	 $title=get_post_meta($postid,'Course_Topic',true);
	 
	 $user=get_userdata($__kf_Attendee_ID);
	 
	  echo "<tr>
	  <td class='$alt' align='left' >".date("Y-m-d", strtotime($post_date))."</td>
	  <td  class='$alt' align='left' >$user->display_name</td>
	  <td  class='$alt' align='left'><a class='conference-title-green' href='$link1'>$title </a></td>
	  <td class='$alt'  align='left'>".$venue."</td>
	   <td class='$alt'  align='left'>".$display_date."</td>
	   <td class='$alt'  align='right' >$".number_format($registration_fee, 2)."</td>
		<td  class='$alt' align='right'>$". number_format($transaction_amount, 2)."</td>
		 <td class='$alt'  align='left'>$category</td>
	 
	  </tr>" ;
endwhile;
	?>
   </tbody>
   <tfoot>
    <tr><td colspan="8" >
    <div class="tablenav bottom">

		<div class="alignleft actions bulkactions">
			
		</div>
		<div class="alignleft actions">
		</div>
	<?php 
    	 post_pagination($total);
		 ?></div>
		<br class="clear">
	</div>
    
	
		</td></tr> 
        </tfoot> 
       
       
        <?php
	
   
	
	  if($the_query->found_posts==0 ){
		 echo  "<tr><td colspan='8' >No Transaction Found</td></tr>" ;

		}
		
		wp_reset_query();
wp_reset_postdata(); 
		
		
		
		
		
		
	   ?>
          </table>   
         


