<?php 


/*********************************************************************************************************/
/*********************************************************************************************************/
/*********************************************************************************************************/
function mer_user_profile_fields( $user ) {
	
	
	$user_id=$user->ID;
	
	?>
      <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
  
    <h3>Extra Profile Information</h3>
 <table  class="form-table" > 
 
   <tr><th width="150" align="left"><?php
      echo '<label for="credits_available">' . _e("Credits Available", 'mer' ).'</label> ';?>
       </th><td>
       <?php if(!is_super_admin()){?>
       <input type="text"   disabled   value="<?php echo get_user_meta($user_id, 'credits_available',true); ?>" size="25" /><input type="hidden" id="credits_available"  name="credits_available" value="<?php echo get_user_meta($user_id, 'credits_available',true); ?>" size="25" />
         
         <?php }else{?>
          <input type="text" id="credits_available"  name="credits_available" value="<?php echo get_user_meta($user_id, 'credits_available',true); ?>" size="25" />
         
          <?php }?>
        
        </td></tr>
        
        
      <tr><th width="150" align="left">
	  
	   
       
	  <?php 
	 // var_dump(get_user_meta($user_id));
      echo '<label for="middle_name">' . _e("Title", 'mer' ).'</label> ';?>
        </th><td>
        
        <?php
		global $wpdb;
        $tbl=$wpdb->prefix.'titles';
	 		$q = "SELECT  title FROM $tbl ";
  			 $usertitles = $wpdb->get_results($q );
					
					
					echo " <select name='Title' id='title' >";	 
						 echo "<option value='' >None</option> ";  
						  foreach ($usertitles  as $row) {
						  if($row->title!='')
							if(get_user_meta($user_id, 'Title',true)==$row->title){
								echo  "<option selected='selected'  value=\"".$row->title."\" >".$row->title."</option>";
							}else{
								echo "<option   value=\"".$row->title."\" >".$row->title."</option>";
								
							}  
						  }
							
					   
							?></select>
        
 </td></tr>
    <tr><th width="150" align="left"><?php
      echo '<label for="middle_name">' . _e("Middle Name", 'mer' ).'</label> ';?>
        </th><td><input type="text" id="middle_name" name="MI" value="<?php echo get_user_meta($user_id, 'MI',true); ?>" size="25" /></td></tr>
    <tr><th width="150" align="left"><?php
      echo '<label for="degree">' . _e("Degree", 'mer' ).'</label> ';?>
        </th><td>
        <select name="Degree" id='degree' ><option  value="" >Please select One</option>
                          <?php
                        	$tbl=$wpdb->prefix.'degree';
	 		$q = "SELECT  degree FROM $tbl ";
  			 $user_degrees = $wpdb->get_results($q );
						 
						  foreach ($user_degrees  as $row) {
						  
						  if($row->degree!='')
							if(get_user_meta($user_id, 'Degree',true)==$row->degree){
								echo  "<option selected='selected'  value=\"".$row->degree."\" >".$row->degree."</option>";
							}else{
								echo "<option   value=\"".$row->degree."\" >".$row->degree."</option>";
								
							}  
						  }
							
							?>
                        
                        </select>
        
        <!--<input type="text" id="degree" name="Degree" value="<?php echo get_user_meta($user_id, 'Degree',true); ?>" size="25" />--></td></tr>
    
     <tr><th width="150" align="left"><?php
      echo '<label for="middle_name">' . _e("Speciality", 'mer' ).'</label> ';?>
        </th><td>  <select name="Specialty" id='Speciality' ><option value="">Please select One</option>
                        
                           <?php
						   
						    $us=get_user_meta($user_id, 'Specialty',true);
						   
                       $tbl=$wpdb->prefix.'speciality';
	 		$q = "SELECT  speciality FROM $tbl ";
  			 $user_specialitys = $wpdb->get_results($q );
			 
						  foreach ($user_specialitys  as $row) {
						  	 if($row->speciality!='')
							if(get_user_meta($user_id, 'Specialty',true)==$row->speciality){
								echo  "<option selected='selected'  value=\"".$row->speciality."\" >".$row->speciality."</option>";
							}else{
								echo "<option   value=\"".$row->speciality."\" >".$row->speciality."</option>";
								
							}  
						  }
							
							?>
                        </select><!--<input type="text" id="speciality" name="Speciality" value="<?php echo get_user_meta($user_id, 'Speciality',true); ?>" size="25" />--></td></tr>
    
             
       <tr><th width="150" align="left"><?php
      echo '<label for="instution">' . _e("Institution/Company ", 'mer' ).'</label> ';?>
        </th><td><input type="text" id="instution" name="instution" value="<?php echo get_user_meta($user_id, 'instution',true); ?>" size="25" /></td></tr>
       
       
       <tr><th width="150" align="left"><?php
      echo '<label for="Mailing Address">' . _e("Mailing Address", 'mer' ).'</label> ';?>
       </th><td><textarea rows='4' cols="40" name="Address" id='mailing_address1' ><?php echo get_user_meta($user_id, 'Address',true); ?></textarea></td></tr>
     
        <tr><th width="150" align="left"><?php
      echo '<label for="mailing_address2">' . _e("Mailing Address2", 'mer' ).'</label> ';?>
      
       </th><td><textarea rows='4' cols="40" name="Address_2" id='mailing_address2' ><?php echo get_user_meta($user_id, 'Address_2',true); ?></textarea></td></tr>
      
       <tr><th width="150" align="left"><?php
      echo '<label for="city">' . _e("City", 'mer' ).'</label> ';?>
        </th><td><input type="text" id="city" name="City" value="<?php echo get_user_meta($user_id, 'City',true); ?>" size="25" /></td></tr>
       
       <tr><th width="150" align="left"><?php
      echo '<label for="state">' . _e("State/Proviance", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="state" name="State_Province" value="<?php echo get_user_meta($user_id, 'State_Province',true); ?>" size="25" /></td></tr>
     
     <tr><th width="150" align="left"><?php
      echo '<label for="country">' . _e("Country", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="country" name="Country" value="<?php echo get_user_meta($user_id, 'Country',true); ?>" size="25" /></td></tr>
     
     <tr><th width="150" align="left"><?php
      echo '<label for="postal_code">' . _e("Zip", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="postal_code" name="Zip" value="<?php echo get_user_meta($user_id, 'Zip',true); ?>" size="25" /></td></tr>
    
    <tr><th width="150" align="left"><?php
      echo '<label for="day_work">' . _e("Day/Work Phone ", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="day_work" name="Phone_Office" value="<?php echo get_user_meta($user_id, 'Phone_Office',true); ?>" size="25" /></td></tr>
    
    <tr><th width="150" align="left"><?php
      echo '<label for="home_phone">' . _e("Home Phone", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="home_phone" name="Phone_Home" value="<?php echo get_user_meta($user_id, 'Phone_Home',true); ?>" size="25" /></td></tr>

 
<tr><th width="150" align="left"><?php
      echo '<label for="Pharm_BirthDate">' . _e("Pharm BirthDate", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="Pharm_BirthDate" name="Pharm_BirthDate" value="<?php echo get_user_meta($user_id, 'Pharm_BirthDate',true); ?>" size="25" /></td></tr>

<tr><th width="150" align="left"><?php
      echo '<label for="Pharm_NAPB">' . _e("Pharm NAPB", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="Pharm_NAPB" name="Pharm_NAPB" value="<?php echo get_user_meta($user_id, 'Pharm_NAPB',true); ?>" size="25" /></td></tr>

<tr><th width="150" align="left"><?php
      echo '<label for="Phone_Contact">' . _e("Phone Contact", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="Phone_Contact" name="Phone_Contact" value="<?php echo get_user_meta($user_id, 'Phone_Contact',true); ?>" size="25" /></td></tr>

<tr><th width="150" align="left"><?php
      echo '<label for="Phone_Mobile">' . _e("Phone Mobile", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="Phone_Mobile" name="Phone_Mobile" value="<?php echo get_user_meta($user_id, 'Phone_Mobile',true); ?>" size="25" /></td></tr>

<tr><th width="150" align="left"><?php
      echo '<label for="Phone_Primary">' . _e("Phone Primary", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="Phone_Primary" name="Phone_Primary" value="<?php echo get_user_meta($user_id, 'Phone_Primary',true); ?>" size="25" /></td></tr>

<tr><th width="150" align="left"><?php
      echo '<label for="home_phone">' . _e("Email 2", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="email2" name="Email_2" value="<?php echo get_user_meta($user_id, 'email2',true); ?>" size="25" /></td></tr>
       
      

       </tr>
        <tr><th width="150" align="left"><?php
      echo '<label for="billing__name">' . _e("Billing Name", 'mer' ).'</label> ';?>
        </th><td><input type="text" id="billing_name"      name="Billing_Name" value="<?php echo get_user_meta($user_id, 'Billing_Name',true); ?>" size="25" /></td></tr>
  <tr>
       <th width="150" align="left">      
       </th><td><input  type="checkbox" name="Billing_Same" value="1" <?php if(get_user_meta($user_id, 'Billing_Same',true)==1){ echo " checked='checked' "; }   ?> id='Billing_Same' > &nbsp; &nbsp;<span  class='heading'>Billing Address Same as Mailing</span>
                    </td></tr>


</table>

  <h3>Billing Information</h3>
     <table   id="billing" class="form-table"  >   
        
        
       <tr><th width="150" align="left"><?php
      echo '<label for="billing_address1">' . _e("Billing Address1", 'mer' ).'</label> ';?>
       </th><td><textarea rows='4' cols="40"    name="Billing_Address" id='billing_address1' ><?php echo get_user_meta($post->ID, 'Billing_Address',true); ?></textarea></td></tr>
     
        <tr><th width="150" align="left"><?php
      echo '<label for="billing_address2">' . _e("Billing Address2", 'mer' ).'</label> ';?>
      
       </th><td><textarea rows='4' cols="40"    name="Billing_Address_2" id='billing_address2' ><?php echo get_user_meta($post->ID, 'Billing_Address_2',true); ?></textarea></td></tr>
      
       <tr><th width="150" align="left"><?php
      echo '<label for="billing_city">' . _e("Billing City", 'mer' ).'</label> ';?>
        </th><td><input type="text" id="billing_city"    name="Billing_City" value="<?php echo get_user_meta($post->ID, 'Billing_City',true); ?>" size="25" /></td></tr>
       
       <tr><th width="150" align="left"><?php
      echo '<label for="billing_state">' . _e("Billing State/Proviance", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="billing_state"    name="Billing_State_Province" value="<?php echo get_user_meta($post->ID, 'Billing_State_Province',true); ?>" size="25" /></td></tr>
     
     <tr><th width="150" align="left"><?php
      echo '<label for="billing_postal_code">' . _e("Billing Zip", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="billing_postal_code"    name="Billing_Zip" value="<?php echo get_user_meta($post->ID, 'Billing_Zip',true); ?>" size="25" /></td></tr>
    
    <tr><th width="150" align="left"><?php
      echo '<label for="billing_country">' . _e("Billing Country", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="billing_country"    name="Billing_Country" value="<?php echo get_user_meta($post->ID, 'Billing_Country',true); ?>" size="25" /></td></tr>
    
    <tr><th width="150" align="left"><?php
      echo '<label for="day_work">' . _e("Day/Work Phone ", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="day_work"    name="Phone_Office" value="<?php echo get_user_meta($post->ID, 'Phone_Office',true); ?>" size="25" /></td></tr>
    
    <tr><th width="150" align="left"><?php
      echo '<label for="billing_zip">' . _e("Home Phone", 'mer' ).'</label> ';?>
       </th><td><input type="text" id="home_phone"    name="Phone_Home" value="" size="25" /></td></tr>
       
        
     
</table>
    
     <script language="javascript" type="text/javascript" >
 
  if( $('#Billing_Same')){
		     if  ($('#Billing_Same').is(':checked')) {
			 // $("#billing :input").attr("disabled", true);
			 // $("#billing :input").attr("value", "");
			   $("#billing_name").attr("value", $("#first_name").val()+ ' ' +$("#last_name").val());
			   $("#billing_address1").attr("value", $("#mailing_address1").val());
			    $("#billing_address2").attr("value", $("#mailing_address2").val());
			   $("#billing_city").attr("value", $("#city").val());
			   $("#billing_state").attr("value", $("#state").val());
			    $("#billing_city").attr("value", $("#city").val());
				 $("#billing_postal_code").attr("value", $("#postal_code").val());
				 $("#billing_country").attr("value", $("#country").val());	
			  
			 }
		   
	   $('#Billing_Same').click(function() {
			if  ($('#Billing_Same').is(':checked')) {
			  //$("#billing :input").attr("disabled", true);
			 
			  $("#billing_name").attr("value", $("#first_name").val()+ ' ' +$("#last_name").val());
			   $("#billing_address1").attr("value", $("#mailing_address1").val());
			    $("#billing_address2").attr("value", $("#mailing_address2").val());
			   $("#billing_city").attr("value", $("#city").val());
			   $("#billing_state").attr("value", $("#state").val());
			    $("#billing_city").attr("value", $("#city").val());
				 $("#billing_postal_code").attr("value", $("#postal_code").val());
				 $("#billing_country").attr("value", $("#country").val());
			 }else{
			  //$("#billing :input").attr("disabled", false); 
			   $("#billing :input").attr("value", "");
			  				 
			}
	   });
	   
	   }
 
 </script>
    
 <?php  
  
 
 
 	
}
function mer_conference_box( $post ) {
	$conf_array=array('__kp_CourseID','__kf_Topic_ID','Course_Topic_ID','Course_Topic','__kf_Venue_ID','Airfare_Statement','Course_Start_Date','Course_End_Date','Course_Display_Date','Course_Location','Course_Lowest_Room_Rate','Course_Travel_Text','Course_Venue','Course_Venue_Address','Course_Venue_Address_2','Course_Venue_Airport_Code_1','Course_Venue_Airport_Name_1','Course_Venue_Amenities_Text_List','Course_Venue_Area_Site','Course_Venue_Check_Out_Time','Course_Venue_City','Course_Venue_Country','Course_Venue_Map_Site','Course_Venue_Passport_Flag','Course_Venue_Reservation_Group_Code','Course_Venue_Reservation_Phone','Course_Venue_State','Course_Venue_Web_Site','Course_Venue_Zip','Course_Venue_Booking_Link','Course_Venue_Check_In_Time','Early_Fee_Date','MeetingCode','Notes_Accommodations','Pricing_Physician','Pricing_Early_Physician','Pricing_Other','Pricing_Early_Other','Pricing_Early_Promo_Amount','Web_Category_1','Web_Category_2','Credits','Course_Educational_Objective','create_date');
  $textarea=array('Course_Educational_Objective','Course_Travel_Text','Course_Venue_Address','Course_Venue_Address_2','Notes_Accommodations');
  // Use nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'mer_noncename' );?>
  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  
    
   
  <table width="100%" >
   
   <?php
foreach ($conf_array as $field) {   ?> 
  
   <tr><th valign="top"  width="150" align="left" ><?php
  echo '<label for="$field">' . _e($field, 'mer' ).'</label> ';?>
  
   </th>
   
   <td>
  <?php if(in_array($field,$textarea)){ ?>
  
   <textarea rows='4' cols="40" id="<?php echo $field; ?>" name="<?php echo $field; ?>" ><?php echo get_post_meta($post->ID, $field,true); ?></textarea> 

   <?php }elseif($field=='Course_Location'){ ?>
    <input type="text" id="Course_Location" name="Course_Location" value="<?php echo get_post_meta($post->ID, $field,true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?olocation=Course_Location&page=theme_options&TB_iframe=true&width=600&height=550" title="Select An Attendee"  >Modify</a> 

  <?php }elseif($field=='__kf_Topic_ID'){ ?>
    <input type="text" id="__kf_Topic_ID" name="__kf_Topic_ID" value="<?php echo get_post_meta($post->ID, $field,true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?otopic=__kf_Topic_ID&page=theme_options&TB_iframe=true&width=600&height=550" title="Select An Attendee"  >Modify</a> 

  <?php } elseif($field=='Web_Category_1' || $field=='Web_Category_2'){ ?>
    <input type="text" id="<?php echo $field; ?>" name="<?php echo $field; ?>" value="<?php echo get_post_meta($post->ID, $field,true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?ocategory=<?php echo $field; ?>&page=theme_options&TB_iframe=true&width=600&height=550" title="Select An Attendee"  >Modify</a> 

  <?php }else{ ?>
    <input type="text" id="<?php echo $field; ?>" name="<?php echo $field; ?>" value="<?php echo get_post_meta($post->ID, $field,true); ?>" size="25" /> 

  <?php }
  
  ?> 
   </td>
   </tr>
    <?php
}?> 
  <tr style="display:none"  ><th width="150" align="left"> 
       </th><td>
    <input type="text"     name="mer_key" value="__kp_CourseID" size="25" /> 
 	<input type="text"     name="mer_tbl" value="conference" size="25" /> </td></tr>
 
  </table>
 <script>
    $(function() {
        
		$( "#Course_End_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#Course_Start_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#Early_Fee_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#create_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });
    </script>
 <?php 
}

function mer_registration_box( $post ) {
	 
	 
     wp_nonce_field( plugin_basename( __FILE__ ), 'mer_noncename' );
   $__kf_CourseID=get_post_meta($post->ID, '__kf_CourseID',true);
    $user_id=get_post_meta($post->ID, '__kf_AttendeeID',true);
  
  //var_dump(get_user_meta($user_id));
  ?>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  
 
  
   <table width="100%" >   
    <tr><th width="150" align="left"><?php
      echo '<label for="user_id">' . _e("Attendee", 'mer' ).'</label> ';?>
       </th><td>
       <?php 
	   
	 //  echo $user_id;
	 //  wp_dropdown_users(array('name' => '__kf_AttendeeID','selected'=> $user_id  )); ?>
       
       <input type="text" id='__kf_Attendee_ID'  name="__kf_Attendee_ID" value="<?php echo get_post_meta($post->ID, '__kf_Attendee_ID',true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?ouser=__kf_Attendee_ID&page=theme_options&TB_iframe=true&width=600&height=550" title="Select An Attendee"  >Modify</a></td></tr>
      <tr>
     <th width="150" align="left"><?php
  echo '<label for="registration_id">' . _e("Conference", 'mer' ).'</label> ';?>
  
   </th><td>
   
    <?php 
   
      $args=array(
      'post_type' => 'conference',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'caller_get_posts'=> 1
    );
	
	
//$postslist = get_posts( $args );  
   
//  get_post_meta($post->ID, 'payment_type',true);
?> 
<input type="text" id='__kf_CourseID'  name="__kf_CourseID" value="<?php echo get_post_meta($post->ID, '__kf_CourseID',true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?oconference=__kf_CourseID&page=theme_options&TB_iframe=true&width=600&height=550"  title="Select A  Conference" >Modify</a>
   </td></tr>
     
       <tr><th width="150" align="left"><?php
      echo '<label for="first_name">' . _e("__kp_Registration_ID", 'mer' ).'</label> ';?>
       </th><td>
       <input type="text"   name="__kp_Registration_ID" value="<?php echo get_post_meta($post->ID, '__kp_Registration_ID',true); ?>" size="25" /></td></tr>
     
    
<tr><th width="150" align="left"><?php
      echo '<label for="first_name">' . _e("Registration Fee", 'mer' ).'</label> ';?>
       </th><td>
       <input type="text"   name="Registration Fee" value="<?php echo get_post_meta($post->ID, 'Registration_Fee',true); ?>" size="25" /></td></tr>
     
 <tr><th width="150" align="left"><?php
  echo '<label for="first_name">' . _e("Registration Date", 'mer' ).'</label> ';?>
 
       </th><td>
       <input type="text" id="Registration_Date"   name="Registration_Date" value="<?php echo get_post_meta($post->ID, 'Registration_Date',true); ?>" size="25" /></td></tr>
   
<tr>
       <th width="150" align="left">   <?php
      echo '<label for="Registration_Status">' . _e("Registration Status Enabled", 'mer' ).'</label> ';?>   
       </th><td>
       
       <select name="Registration_Status">
       <option <?php if( get_post_meta($post->ID, 'Registration_Status',true)=='Pending'){echo " selected='selected' ";} ?> value="Pending">Pending</option>
       <option  <?php if( get_post_meta($post->ID, 'Registration_Status',true)=='Completed'){echo " selected='selected' ";} ?>   value="Completed" >Completed</option>
       </select>
        
                    </td></tr>
<tr style="display:none" ><th  width="150" align="left"> 
       </th><td>
        <input type="text"     name="mer_key" value="__kp_Registration_ID" size="25" /> 
 		<input type="text"     name="mer_tbl" value="registrations" size="25" />  
 </td></tr>
   
<tr></table>
 
     <script>
    $(function() {
       
		$( "#Registration_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });
    </script>

   
 <?php	
 
 
 
 
 
 
}

function mer_transaction_box( $post ) {
  // Use nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'mer_noncename' );

 ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  
   
  <table width="100%"    >
    <tr><th width="150" align="left"><?php
  echo '<label for="__kp_Transaction_ID">' . _e("__kp_Transaction_ID", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="__kp_Transaction_ID" name="__kp_Transaction_ID" value="<?php echo get_post_meta($post->ID, '__kp_Transaction_ID',true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?otransaction=__kp_Transaction_ID&page=theme_options&TB_iframe=true&width=600&height=550" title="Select A Transaction"  >Modify</a></td></tr>
   
   <tr><th width="150" align="left"><?php
  echo '<label for="__kf_Registration_ID">' . _e("__kf_Registration_ID", 'mer' ).'</label> ';?>
  
   </th><td>
   
    <?php 
   
    
?> 
  <input type="text" id="__kf_Registration_ID" name="__kf_Registration_ID" value="<?php echo get_post_meta($post->ID, '__kf_Registration_ID',true); ?>" size="25" />
  &nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?oregistration=__kf_Registration_ID&page=theme_options&TB_iframe=true&width=700&height=550" title="Select Registration"  >Modify</a>
   </td></tr>
   
<tr><th width="150" align="left"><?php
  echo '<label for="user">' . _e("Attendee", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="__kf_Attendee_ID" name="__kf_Attendee_ID" value="<?php echo get_post_meta($post->ID, '__kf_Attendee_ID',true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?ouser=__kf_Attendee_ID&page=theme_options&TB_iframe=true&width=600&height=550" title="Select An Attendee"  >Modify</a>
   
   
   <?php  //wp_dropdown_users(array('name' => '__kf_Attendee_ID','selected'=> get_post_meta($post->ID, '__kf_Attendee_ID',true))); ?></td></tr>
   <tr><th width="150" align="left"><?php
  echo '<label for="first_name">' . _e("First Name", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="first_name" name="First_Name" value="<?php echo get_post_meta($post->ID, 'First_Name',true); ?>" size="25" /></td></tr>

   <tr><th width="150" align="left"><?php
  echo '<label for="last_name">' . _e("Last Name", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="last_name" name="Last_Name" value="<?php echo get_post_meta($post->ID, 'Last_Name',true); ?>" size="25" /></td></tr>

   
   
  
   <tr><th width="150" align="left"><?php
  echo '<label for="transaction_amount">' . _e("Transaction Amount", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="Transaction_Amount" name="Transaction_Amount" value="<?php echo get_post_meta($post->ID, 'Transaction_Amount',true); ?>" size="25" /><input type="hidden"   name="Prev_Transaction_Amount" value="<?php echo get_post_meta($post->ID, 'Transaction_Amount',true); ?>" size="25" /></td></tr>
   
  
   
    <tr><th valign="top"  width="150" align="left" ><?php
  echo '<label for="transaction_category">' . _e("Transaction Category", 'mer' ).'</label> ';?>
  
   </th><td>
   <?php
  
  $options = get_option( 'mer_theme_options' );
                              $transaction_categorys=$options['transaction_category'];
						   
						   $arrs = preg_split('/\n|\r\n?/', $transaction_categorys);
						 //var_dump( $arrs); 
						  ?>
   <input type="hidden" name="pre_Transaction_Category" value="<?php echo get_post_meta($post->ID, 'Transaction_Category',true);?>" />
   <select name="Transaction_Category"><option value=''>None</option> 
   <?php
  
 
						  
						 
						  foreach ($arrs  as $term) {
						  
							if($term==get_post_meta($post->ID, 'Transaction_Category',true)){
								echo  "<option selected='selected'  value=\"".$term."\" >".$term."</option>";
							}else{
								echo "<option   value=\"".$term."\" >".$term."</option>";
								
							}  
						  }
                                ?>
   
    <!--<option value='Registration PAID CC' <?php if( get_post_meta($post->ID, 'Transaction_Category',true)=='Registration PAID CC'){ echo " selected='selected' "; } ?> >Registration PAID CC</option>
   <option value='Registration PAID CHECK' <?php if( get_post_meta($post->ID, 'Transaction_Category',true)=='Registration PAID CHECK'){ echo " selected='selected' "; } ?> >Registration PAID CHECK</option>
  <option value='Cancellation REFUND' <?php if( get_post_meta($post->ID, 'Transaction_Category',true)=='Cancellation REFUND'){ echo " selected='selected' "; } ?> >Cancellation REFUND</option>
   <option value='Cancellation CREDIT' <?php if( get_post_meta($post->ID, 'Transaction_Category',true)=='Cancellation CREDIT'){ echo " selected='selected' "; } ?> >Cancellation CREDIT</option>
   <option value='Courtesy CREDIT' <?php if( get_post_meta($post->ID, 'Transaction_Category',true)=='Courtesy CREDIT'){ echo " selected='selected' "; } ?> >Courtesy CREDIT</option>
 
   <option value='Cancellation FEE' <?php if( get_post_meta($post->ID, 'Transaction_Category',true)=='Cancellation FEE'){ echo " selected='selected' "; } ?> >Cancellation FEE</option>
   <option value='Registration CREDIT USED' <?php if( get_post_meta($post->ID, 'Transaction_Category',true)=='Registration CREDIT USED'){ echo " selected='selected' "; } ?> >Registration CREDIT USED</option>-->
   </select>
   
   </td>
   </tr>
    <tr><th width="150" align="left"><?php
  echo '<label for="Conf_Start_Date">' . _e("Conf Start Date", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="Conf_Start_Date" name="Conf_Start_Date" value="<?php echo get_post_meta($post->ID, 'Conf_Start_Date',true); ?>" size="25" /></td></tr>
<tr><th width="150" align="left"><?php
  echo '<label for="CC_Num">' . _e("CC Num", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="CC_Num" name="CC_Num" value="<?php echo get_post_meta($post->ID, 'CC_Num',true); ?>" size="25" /></td></tr>
  <tr><th width="150" align="left"><?php
  echo '<label for="CC_Type">' . _e("CC Type", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="CC_Type" name="CC_Type" value="<?php echo get_post_meta($post->ID, 'CC_Type',true); ?>" size="25" /></td></tr>
  
   
  
<tr><th width="150" align="left"><?php
  echo '<label for="Registration_Fee_Type">' . _e("Registration Fee Type", 'mer' ).'</label> ';?>
    
   </th><td><select name="Registration_Fee_Type"><option value=''>None</option> 
   <option value='Pricing Physician' <?php if( get_post_meta($post->ID, 'Registration_Fee_Type',true)=='Pricing Physician'){ echo " selected='selected' "; } ?> >Physician</option>
   
   <option value='Pricing Early Physician' <?php if( get_post_meta($post->ID, 'Registration_Fee_Type',true)=='Pricing Early Physician'){ echo " selected='selected' "; } ?> >Physician Early</option>
   <option value='Pricing Other' <?php if( get_post_meta($post->ID, 'Registration_Fee_Type',true)=='Pricing Other'){ echo " selected='selected' "; } ?> >Other</option>
   <option value='Pricing Early Other' <?php if( get_post_meta($post->ID, 'Registration_Fee_Type',true)=='Pricing Early Other'){ echo " selected='selected' "; } ?> >Other Early</option>
 </select></td>
   </tr>
   <tr><th width="150" align="left"><?php
  echo '<label for="Transaction_Source">' . _e("Transaction Source", 'mer' ).'</label> ';?>
  
   </th><td><select name="Transaction_Source">  
   <option value='CC' <?php if( get_post_meta($post->ID, 'Transaction_Source',true)=='CC'){ echo " selected='selected' "; } ?> >Credit Card</option>
   <option value='Check' <?php if( get_post_meta($post->ID, 'Transaction_Source',true)=='Check'){ echo " selected='selected' "; } ?> >Check</option>
   </select></td>
   </tr>
    
     <tr><th width="150" align="left"><?php
  echo '<label for="AN_Transaction_ID">' . _e("Auth Transaction Id", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="AN_Transaction_ID" name="AN_Transaction_ID" value="<?php echo get_post_meta($post->ID, 'AN_Transaction_ID',true); ?>" size="25" /></td></tr>

    
    <tr><th width="150" align="left"><?php
  echo '<label for="AN_CC_Number">' . _e("Auth CC Number", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="AN_CC_Number" name="AN_CC_Number" value="<?php echo get_post_meta($post->ID, 'AN_CC_Number',true); ?>" size="25" /></td></tr>
  
 
   
    <tr><th valign="top"  width="150" align="left" ><?php
  echo '<label for="note">' . _e("Note", 'mer' ).'</label> ';?>
  
   </th><td><textarea rows='6' cols="50" name="Note" id='Note' ><?php echo get_post_meta($post->ID, 'Note',true); ?></textarea></td><tr><th width="150" align="left"><?php
  echo '<label for="create date">' . _e("Create Date", 'mer' ).'</label> ';?>
  
   </th><td><input type="text" id="create_date" name="create_date" value="<?php echo get_post_meta($post->ID, 'create_date',true); ?>" size="25" /></td></tr>
   </tr>
   <tr style="display:none" ><th width="150" align="left"> 
       </th><td>
      <input type="text"     name="mer_key" value="__kp_Transaction_ID" size="25" /> 
 		<input type="text"     name="mer_tbl" value="transactions" size="25" />  </td></tr>

  </table>
   <script>
    $(function() {
       
		$( "#Conf_Start_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#create_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });
    </script>
  <?php
          
   
   
}
	
	
	
function mer_topic_box( $post ) {
	$conf_array=array('__kp_Topic_ID','Topic_Full_Name','Educational_Objectives','Nursing_Purpose','Topic_Order','Target_Audience','Topic_Chair_1','Topic_Chair_2');
  $textarea=array('Educational_Objectives','Nursing_Purpose');
  // Use nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'mer_noncename' );?>
  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  
    <script>
    $(function() {
        
		$( "#Course_End_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#Course_Start_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#Early_Fee_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });

    });
    </script>
  
  <table width="100%" >
   
   <?php
foreach ($conf_array as $field) {   ?> 
  
   <tr><th valign="top"  width="150" align="left" ><?php
  echo '<label for="$field">' . _e($field, 'mer' ).'</label> ';?>
  
   </th><td>
   
   <?php if(in_array($field,$textarea)){ ?>
  
   <textarea rows='4' cols="40" id="<?php echo $field; ?>" name="<?php echo $field; ?>" ><?php echo get_post_meta($post->ID, $field,true); ?></textarea></td></tr>

   <?php }else{ ?>
    <input type="text" id="<?php echo $field; ?>" name="<?php echo $field; ?>" value="<?php echo get_post_meta($post->ID, $field,true); ?>" size="25" /></td></tr>

  <?php } ?> 
   
   </td>
   </tr>
    <?php
}?> 
    <tr style="display:none"  ><th width="150" align="left"> 
       </th><td>
        <input type="text"     name="mer_key" value="__kp_Topic_ID" size="25" /> 
 	<input type="text"     name="mer_tbl" value="topics" size="25" /> 
 </td></tr>
 
  </table>
 
 <?php 
}	

	
function mer_agenda_box( $post ) {
	$conf_array=array('__kp_agenda_items_ID','__kf_Course_ID','Agenda_Item_Faculty','Agenda_Item_Title','Agenda_Item_Date','Agenda_Item_Start_Time','Agenda_Item_End_Time','day','description');
  $textarea=array('description');
  
  // Use nonce for verification
  wp_nonce_field( plugin_basename( __FILE__ ), 'mer_noncename' );?>
  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
  
    <script>
    $(function() {
        
		$( "#Agenda_Item_Date" ).datepicker({ dateFormat: 'yy-mm-dd' });
	

    });
    </script>
  
  <table width="100%" > 
   
   <?php
foreach ($conf_array as $field) {   ?> 
  
   <tr><th valign="top"  width="150" align="left" ><?php
  echo '<label for="$field">' . _e($field, 'mer' ).'</label> ';?>
  
   </th><td>
   
   <?php if(in_array($field,$textarea)){ ?>
  
   <textarea rows='4' cols="40" id="<?php echo $field; ?>" name="<?php echo $field; ?>" ><?php echo get_post_meta($post->ID, $field,true); ?></textarea> 

   <?php }else if($field=='__kf_Course_ID'){ ?>
    <input type="text" id="__kf_Course_ID" name="__kf_Course_ID" value="<?php echo get_post_meta($post->ID, $field,true); ?>" size="25" />&nbsp;&nbsp;<a class="thickbox"   href="<?php echo site_url(); ?>/wp-admin/themes.php?oconference=__kf_Course_ID&page=theme_options&TB_iframe=true&width=600&height=550"  title="Select A  Conference" >Modify</a>

  <?php }    else { ?>
    <input type="text" id="<?php echo $field; ?>" name="<?php echo $field; ?>" value="<?php echo get_post_meta($post->ID, $field,true); ?>" size="25" />

  <?php } ?> 
   
   </td>
   </tr>
    <?php
}?> 
  <tr style="display:none"><th   width="150" align="left"> 
       </th><td>
       <input type="text" id="mer_key"   name="mer_key" value="__kp_agenda_items_ID" size="25" /> 
 	<input type="text" id="mer_key"   name="mer_tbl" value="agenda_items" size="25" /></td></tr>
 
  </table>
 
 <?php 
}	

	
/*********************************************************************************************************/
/*********************************************************************************************************/
/*********************************************************************************************************/

 
  

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*  When the post is saved, saves our custom data */
function mer_save_postdata( $post_id ) {   
  // verify if this is an auto save routine. 
  // If it is our form has not been submitted, so we dont want to do anything
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;
  // verify this came from the our screen and with proper authorization,
  // because save_post can be triggered at other times
  if ( !wp_verify_nonce( $_POST['mer_noncename'], plugin_basename( __FILE__ ) ) )
      return;
  // Check permissions
  if ( 'page' == $_POST['post_type'] ) 
  {
    if ( !current_user_can( 'edit_page', $post_id ) )
        return;
  }
  else
  {
    if ( !current_user_can( 'edit_post', $post_id ) )
        return;
  } 

  // OK, we're authenticated: we need to find and save the data

  //if saving in a custom table, get post_ID
  $post_ID = $_POST['post_ID'];
  
  
  
  $checkdata=array(
  //--------Conference Columns
'__kf_Topic_ID','__kf_Venue_ID','__kp_CourseID','Airfare_Statement','Course_Display_Date','Course_Educational_Objective','Course_End_Date','Course_Location','Course_Lowest_Room_Rate','Course_Start_Date','Course_Topic','Course_Topic_ID','Course_Travel_Text','Course_Venue','Course_Venue_Address','Course_Venue_Address_2','Course_Venue_Airport_Code_1','Course_Venue_Airport_Name_1','Course_Venue_Amenities_Text_List','Course_Venue_Area_Site','Course_Venue_Booking_Link','Course_Venue_Check_In_Time','Course_Venue_Check_Out_Time','Course_Venue_City','Course_Venue_Country','Course_Venue_Map_Site','Course_Venue_Passport_Flag','Course_Venue_Reservation_Group_Code','Course_Venue_Reservation_Phone','Course_Venue_State','Course_Venue_Web_Site','Course_Venue_Zip','Early_Fee_Date','MeetingCode','Notes_Accommodations','Pricing_Early_Other','Pricing_Early_Physician','Pricing_Early_Promo_Amount','Pricing_Other','Pricing_Physician','Web_Category_1','Web_Category_2','Credits','create_date','course_live_flag',

//----------Agenda Columns
 
'__kf_Course_ID','__kp_agenda_items_ID','Agenda_Item_Faculty','Agenda_Item_Title','Agenda_Item_Date','Agenda_Item_Start_Time','Agenda_Item_End_Time','day','description',
//----------Registration Columns
'__kf_Attendee_ID','__kf_CourseID','__kp_Registration_ID','Registration_Fee','Registration_Date','Registration_Status',
//----------Transaction Columns

'CC_Num','CC_Type','first_name','last_name','Note','Registration_Fee_Type','__kf_Registration_ID','Conf_Start_Date','Transaction_Amount','Transaction_Category','__kp_Transaction_ID','Transaction_Source','__kf_Attendee_ID','AN_Transaction_ID','AN_CC_Number','AN_Transaction_Amount','AN_CC_Type','create_date','Record_correction',
//----------Topic Columns

'__kp_Topic_ID','Educational_Objectives','Nursing_Purpose','Topic_Order','Target_Audience','Topic_Chair_1','Topic_Chair_2','Topic_Full_Name',
  //----------Unique Columns
  'mer_key'
  
);

global $wpdb;
$data=array(); 

  $mer_key=$_POST['mer_key'];
  
  $updateid=get_post_meta($post_ID,$mer_key,true); 
  $tbl  =$wpdb->prefix.$_POST['mer_tbl'];
  
  foreach($_POST as $k=>$v){
	  if ($k==$mer_key && $v==''){
 
	   $sql = "SELECT max($mer_key)+1  from $tbl";  
			echo $v= $wpdb->get_var($sql );
	 }
	  if(in_array($k,$checkdata)){
		  	$data[$k]=$v;
			update_post_meta(   $post_ID,$k,$v); 
	  }
  
 }
 // $update[]=$postarr;
 
 
 
 unset($data['mer_key']);
 if(isset($_POST['create_date'])){
  if($data['create_date']==''){
	$data['create_date']=date('Y-m-d');
	}
 }	
 //{ Transaction 
 if ($mer_key=='__kp_Transaction_ID'){
 
 
 	 
 	  $prev_amount=$_POST['Prev_Transaction_Amount'];
	 $prev_Transaction_Category=$_POST['pre_Transaction_Category'];
	 $credits_available=get_user_meta($data['__kf_Attendee_ID'],'credits_available',true);
	 
	if($data['Transaction_Category']=='Registration CREDIT USED'){
		
		 
		$amount=$data['Transaction_Amount'];
		
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
			$credits_available=$credits_available+$prev_amount;
		} 
		
		$credits_available=$credits_available-$amount;	
		 $wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) );
	}
	
	if($data['Transaction_Category']=='Cancellation REFUND'){
	
					$islive=true;
					$requesturl="https://test.authorize.net/gateway/transact.dll";
					$login="44hKSq9Up3XX";
					$tran_key="6W8Xm328FmC7nTaQ";
					$login=$options['api_key'];
					$tran_key=$options['api_password'];
					$api_mode=$options['api_mode'];    
					
					 $pricing=$_POST['payment_type'];
					$credit=$_POST['credit'];						
					//Make It live
							if($api_mode=='live'){
								
								$requesturl="https://secure.authorize.net/gateway/transact.dll";
								/*$login="7Jqq22UW";
								$tran_key="8YuxC882942dvnFk";*/
							}
							  // require_once('authorizenet.class.php');
							$a = new authorizenet_class;
							 $a->add_field('x_login', $login);
							$a->add_field('x_tran_key', $tran_key);
							$a->add_field('x_trans_id', $tran_key);
							$a->add_field('x_type', 'CREDIT');
							 $a->gateway_url=$requesturl;
							$a->add_field('x_version', '3.1');
							$a->add_field('x_type', 'AUTH_CAPTURE');
			   				// Just a test transaction
							$a->add_field('x_relay_response', 'FALSE');
							//$a->add_field('x_trans_id', 'abc12345');
							// You *MUST* specify '|' as the delim char due to the way I wrote the class.
							// I will change this in future versions should I have time.  But for now, just
							// make sure you include the following 3 lines of code when using this class.
							$a->add_field('x_delim_data', 'TRUE');
							$a->add_field('x_delim_char', '|'); 
							$a->add_field('x_encap_char', '');
							if($api_mode=='live'){
							    $a->add_field('x_test_request', 'FALSE');
							}else{
								$a->add_field('x_test_request', 'TRUE');
							} 
							
							switch ($a->process($islive))
							{  
							 case 1:  // Successs
							  	break;
							 case 2:  // Fail
								echo $a->get_response_reason_text();
								  break;
								  exit;
							}
							
	}
 	if($data['Transaction_Category']=='Cancellation CREDIT'){
		 
		
		$amount=$data['Transaction_Amount'];
		$credits=$amount-50;
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
			 $credits=$amount;
			 $credits_available=$credits_available-$prev_amount;
			
		} else{
			$data2=$data;
			$data2['Transaction_Amount']=50;
			 $data2['Transaction_Category']='Cancellation FEE';
			unset($data2[$mer_key]);
			$wpdb->insert($tbl , $data2 ); 
			
		}
		
		$credits_available=$credits_available+$credits;
		$wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) );
	}
	
	if($data['Transaction_Category']=='Registration REFUND'){
	
		$credits=$data['Transaction_Amount'];
		  $prev_Transaction_Category;
		  $data['Transaction_Category'];
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
			  $credits_available=$credits_available-$prev_amount;
			
		} 
		 
		  $credits_available=$credits_available+$credits;
		 $wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) );
		 
		  
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	if($data['Transaction_Category']=='Courtesy CREDIT' || $data['Transaction_Category']=='Loyalty Credit' ){ 
			 
		  $amount=$data['Transaction_Amount'];
		 
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
		 	$credits_available=$credits_available-$prev_amount;
			
		}  
	
		
		  $credits_available=$credits_available+$amount;
		//update_usermeta( $data['__kf_Attendee_ID'], 'credits_available',$credits_available);
		 $wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) ); 
		
	}
 
 
 
 }
  
  	
 
	 if($updateid=='')
		 { 
			  $wpdb->insert($tbl , $data ); 
			
		 }
		 else{
	 	$where=array($mer_key=>$updateid);
		  $wpdb->update($tbl , $data,$where );  
		}
		
	  /*echo updateData($tbl , $data,$where);	
	exit;	*/
		
	/*	echo " <div style='width:500px' >";
 var_dump($data);
 echo "</div> ";
 */
	  
  // Do something with $mydata 
  // probably using add_post_meta(), update_post_meta(), or 
  // a custom table (see Further Reading section below)
}


function mer_save_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;
	
 $user_array=array('first_name','last_name','Address','Address_2','Billing_Address','Billing_Address_2','Billing_City','Billing_Country','Billing_Name','Billing_Same','Billing_State_Province','Billing_Zip','City','Country','Degree','Email_1','Email_2','Full_Name','MI','password','Pharm_BirthDate','Pharm_NAPB','Phone_Contact','Phone_Home','Phone_Mobile','Phone_Office','Phone_Primary','Specialty','State_Province','Title','username','Zip','credits_available','__kp_Attendee_ID');

/* remove_action('personal_options_update', 'mer_save_profile_fields' ); 
remove_action('edit_user_profile_update', 'mer_save_profile_fields' ); 
				// update the post, which calls save_post again
				wp_remove_user();
				//wp_update_post(array('ID' => $post_ID, 'post_title' => '','post_status' => 'private'));
				// re-hook this function
add_action('personal_options_update', 'mer_save_profile_fields' ); 
add_action('edit_user_profile_update', 'mer_save_profile_fields' ); */
if(!isset($_POST['Billing_Same'])){
			$data['Billing_Same']=0;
		}else{
			
			$data['Billing_Same']=1;
		}
		$userarr=array();	
		
		global $wpdb;
		 $tbl  =$wpdb->prefix.'users';
		$sql = "SELECT  *   FROM $tbl where `ID`='$user_id'";
  		$urow = $wpdb->get_row($sql ); 
		$userarr['__kp_Attendee_ID']=$user_id;
		
		
		$userarr['username']=$urow->user_login;
		
		$userarr['Email_1']=$urow->user_email;
		 $_POST['Email_1']=$urow->user_email;
		foreach($user_array as $k) { 
		 update_usermeta( $user_id, $k, $_POST[$k]);
		$userarr[$k]=$_POST[$k];
		
		}
		
		if(!is_super_admin( $user_id ))
		{
		
		 
		 
		 
		 
 		$tbl  =$wpdb->prefix.'attendees';
		$sql = "SELECT  __kp_Attendee_ID   FROM $tbl where `__kp_Attendee_ID`='$user_id'";
  		$updateid = $wpdb->get_var($sql ); 
		
				 
				if($updateid=='')
				 { 
						
						$wpdb->insert($tbl , $userarr ); 
				 }
				 else{
				  $userarr['password']=$urow->user_pass; 
				$where=array('__kp_Attendee_ID'=>$updateid);
				   $wpdb->update($tbl , $userarr,$where );  
				}
		
		}
 	 
		
}



