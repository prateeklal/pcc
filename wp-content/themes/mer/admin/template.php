<?php


require_once ( get_template_directory() . '/admin/popup.php' );

$otitle='Options';
if (isset($_POST['osubmit'])){
	 global $wpdb;
    $col=$_POST['tbl']; 
	$tbl=$_POST['tbl']; 
	
	
	
	if($tbl=='title'){
	  $tbl=$wpdb->prefix.$tbl.'s';
	  }else{
	  
	  $tbl=$wpdb->prefix."$tbl";
	  }
	$v=$_POST['value'];
	$vs = preg_split('/\n|\r\n?/', $v);
	$stats=array();



foreach ( $vs as $stat ){
$stats[]=array($col=>$stat);
}
 
$wpdb->query("truncate $tbl");

 foreach ( $stats as $stat )
    $wpdb->insert( $tbl, $stat ); 
	$_REQUEST['settings-updated'] =true;
	$otitle="$col ";
	$otitle=ucfirst($otitle); 
	 
}

if (isset($_GET['oupdate'])){
$v=$_GET['oupdate'];
 
        echo <<<HTML
        <style type="text/css">
        #wpcontent, #footer { margin-left: 0px; }
        </style>
        <script type="text/javascript">
        jQuery(document).ready( function($) {
            $('#adminmenuback, #adminmenuwrap,#wpadminbar,.update-nag,#wpfooter').remove();
			 
        });     
        </script>
HTML;
 
 
?>
<form target="_top" action="<?php echo site_url(); ?>/wp-admin/themes.php?page=theme_options" method="post">
 <table id="template" cellspacing="0" class="wp-list-table widefat fixed media" >

     <thead>
     <tr> 
     <?php
     global $wpdb;
	
      $tbl=$wpdb->prefix."$v";
	  if($v=='title'){
	  $tbl.='s';
	  }
	 		$q = "SELECT  $v   FROM $tbl ";
  			 $datas = $wpdb->get_results($q ); 
			 $larr =array();
			foreach ($datas  as $data) 
 				{
				$larr []=$data->$v;
				} 
			$val =implode("\n", $larr );
			 ?>
      	<td valign='top' align="center"><textarea  name="value"   rows="15" cols="40"  ><?php echo $val; ?></textarea>
        <input type="hidden" name="tbl" value="<?php echo $v; ?>" />
<br/>
<input type="submit" name="osubmit"   value="Submit" /></td>
     </tr>
     </thead>
     </table>
     </form>
<?php


}else{


	if ( ! isset( $_REQUEST['settings-updated'] ) )

		$_REQUEST['settings-updated'] = false;



	?>

	
 
		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>

		<div class="updated fade"><p><strong><?php _e( "$otitle saved", 'mertheme' ); ?></strong></p></div>

		<?php endif; ?>
 
		<form method="post" action="options.php" >

			<?php settings_fields( 'mer_options' ); ?>

			<?php $options = get_option( 'mer_theme_options' );
			$img=$options['logo'];
			
			if($options['logo']==''){
			
			$img='../wp-content/themes/mer/images/logo.jpg';
			
			}
			$sitepages = get_pages();		 ?> 


     
     <table id="template" cellspacing="0" class="wp-list-table widefat fixed media" >

     <thead>
       <tr><th  width="25%" valign='top'>Admin Email</th>

      	  <td valign='top' >
				<input  size="50" type="text" name="mer_theme_options[admin_email]" id='admin_email' value="<?php echo $options['admin_email']; ?>" />
      
		   </td>

        </tr>
        
         <tr><th  width="25%" valign='top'>Share Conference Email From</th>

      	  <td valign='top' >
				<input  size="50" type="text" name="mer_theme_options[conf_email_from]" id='conf_email_from' value="<?php echo $options['conf_email_from']; ?>" />
      
		   </td>

        </tr>
        <tr><th  width="25%" valign='top'>Conference Mail Sent Message</th>

      	  <td valign='top' >
				<input  size="50" type="text" name="mer_theme_options[conf_email_sent_msg]" id='conf_email_sent_msg' value="<?php echo $options['conf_email_sent_msg']; ?>" />
      
		   </td>

        </tr>
 	<tr>

     <th width="25%"></th>

     <th >Image Size</th>

     </tr>

       <tr><th >Featured Thumb Size (W x H)</th>

      	  <td>

			<input type="text" name="mer_theme_options[thumb_loc_w]" id='mer_thumb_loc_w' value="<?php echo $options['thumb_loc_w']; ?>" />
          <strong>x</strong>

			<input type="text" name="mer_theme_options[thumb_loc_h]" id='mer_thumb_loc_h' value="<?php echo $options['thumb_loc_h']; ?>" />
         </td>

        </tr>
    	
        <tr><th >Venue Thumb Size (W x H)</th>

      	  <td>

			<input type="text" name="mer_theme_options[thumb_ven_w]" id='mer_thumb_ven_w' value="<?php echo $options['thumb_ven_w']; ?>" />
          <strong>x</strong>

			<input type="text" name="mer_theme_options[thumb_ven_h]" id='mer_thumb_ven_h' value="<?php echo $options['thumb_ven_h']; ?>" />
         </td>

        </tr>
       
        <!--<tr><th >Course Thumb Size (W x H)</th>

      	  <td>

			<input type="text" name="mer_theme_options[thumb_course_w]" id='mer_thumb_course_w' value="<?php //echo $options['thumb_course_w']; ?>" />
          <strong>x</strong>

			<input type="text" name="mer_theme_options[thumb_course_h]" id='mer_thumb_course_h' value="<?php //echo $options['thumb_course_h']; ?>" />
         </td>

        </tr>
         <tr><th >Schedule Thumb Size (W x H)</th>

      	  <td>

			<input type="text" name="mer_theme_options[thumb_sch_w]" id='thumb_sch_w' value="<?php //echo $options['thumb_sch_w']; ?>" />
          <strong>x</strong>

			<input type="text" name="mer_theme_options[thumb_sch_h]" id='thumb_sch_h' value="<?php //echo $options['thumb_sch_h']; ?>" />
         </td>

        </tr>-->
        <tr>
		<th  valign='top' width="25%">Hide Home Page Slider</th>
      	<td valign='top'>
        
         <select   id="mer_theme_options[mer_slider_hide]" name="mer_theme_options[mer_slider_hide]" >
	                                <option <?php if(  $options['mer_slider_hide']==0) echo "selected='selected'" ; ?>    value="0">No</option>
                                     <option <?php if(  $options['mer_slider_hide']==1) echo "selected='selected'" ; ?>  value="1">Yes</option>
          </select>
      

      	</td>
        </tr> 
        
         <tr><th >Slider Thumb Size (W x H)</th>

      	  <td>

			<input type="text" name="mer_theme_options[thumb_semi_w]" id='mer_thumb_course_w' value="<?php echo $options['thumb_semi_w']; ?>" />
          <strong>x</strong>

			<input type="text" name="mer_theme_options[thumb_semi_h]" id='mer_thumb_semi_h' value="<?php echo $options['thumb_semi_h']; ?>" />
         </td>

        </tr>
        
         <tr><th >No Of Slides </th>

      	  <td>

			<input type="text" name="mer_theme_options[slide_no]" id='slide_no' value="<?php echo $options['slide_no']; ?>" />
           
         </td>

        </tr>
     <tr>

     <th width="25%"></th>

     <th >Header</th>

     </tr>

       <tr><th >Logo</th>

      	  <td>

			<input type="text" name="mer_theme_options[logo]" id='mer_theme_options_logo' value="<?php echo $options['logo']; ?>" />&nbsp;<input type="button" value="browse" id='upload_logo_btn' /><div id='logo'><img src='<?php echo $img; ?>'/></div>
         </td>

        </tr>
 <tr>
		<th width="25%">Posts Per Page</th>
      	<td><input type="text" name="mer_theme_options[post_per_page]" id='post_per_page' value="<?php echo $options['post_per_page']; ?>" />
      	</td>
        </tr>
        <tr>
		<th width="25%">Brochure</th>
      	<td><input type="text" name="mer_theme_options[brochure]" id='mer_theme_options_brochure' value="<?php echo $options['brochure']; ?>" />&nbsp;<input type="button"  value="browse" id='upload_brocher_btn' />
      	</td>
        </tr>
        
       
        
        <tr>
		<th  valign='top' width="25%">Home Page Mer Video</th>
      	<td valign='top'><textarea name="mer_theme_options[mer_video]" rows="5" cols="40"  ><?php echo $options['mer_video']; ?></textarea> 

      	</td>
        </tr>
        <tr>
		<th  valign='top' width="25%">Hide Home Page Mer Video</th>
      	<td valign='top'>
        
         <select   id="mer_theme_options[mer_video_hide]" name="mer_theme_options[mer_video_hide]" >
	                                <option <?php if(  $options['mer_video_hide']==0) echo "selected='selected'" ; ?>    value="0">No</option>
                                     <option <?php if(  $options['mer_video_hide']==1) echo "selected='selected'" ; ?>  value="1">Yes</option>
          </select>
      

      	</td>
        </tr>
        <tr>

        <th width="25%"></th>

     <th >Top Menu</th>

     </tr>
 <tr><th >Location Page</th>

      	  <td>
          <select   id="mer_theme_options[Location_Page]" name="mer_theme_options[Location_Page]" >
	                                <option value="">Later</option>
                                   
          <?php
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['Location_Page']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>
			
         </td>

        </tr>
       <tr><th >Login Page</th>

      	  <td>
          <select   id="mer_theme_options[Login_Page]" name="mer_theme_options[Login_Page]" >
	                                <option value="">Later</option>
                                   
          <?php
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['Login_Page']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>
			
         </td>

        </tr>

       <tr><th >User Purchase Page</th>

      	  <td>
          <select   id="mer_theme_options[user_purchase]" name="mer_theme_options[user_purchase]" >
	                                <option value="">Later</option>
                                   
          <?php
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['user_purchase']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>
			
         </td>
<tr><th >Conference Register Page</th>

      	  <td>

			<select   id="mer_theme_options[register_page]" name="mer_theme_options[register_page]" >
	                                <option value="">Later</option>
                                   
          <?php 	
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['register_page']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>  </td>

        </tr>
        </tr>
<tr><th >User's Registration Page</th>

      	  <td>
          <select   id="mer_theme_options[user_reg_page]" name="mer_theme_options[user_reg_page]" >
	                                <option value="">Later</option>
                                   
          <?php
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['user_reg_page']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>
			
         </td>

        </tr>
      <tr><th >Conference Schedule Page</th>

      	  <td>
          <select   id="mer_theme_options[Conference_Schedule_Page]" name="mer_theme_options[Conference_Schedule_Page]" >
	                                <option value="">Later</option>
                                   
          <?php
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['Conference_Schedule_Page']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>
			
         </td>

        </tr>

       

         <tr><th >Topics</th>

      	  <td>

			<select   id="mer_theme_options[Topics]" name="mer_theme_options[Topics]" >
	                                <option value="">Later</option>
                                   
          <?php
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['Topics']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>  </td>

        </tr>

       

        <tr><th >Accredetation</th>

      	  <td>

			<select   id="mer_theme_options[Accredetation]" name="mer_theme_options[Accredetation]" >
	                                <option value="">Later</option>
                                   
          <?php
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['Accredetation']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>  </td>

        </tr>
    
 <tr><th >Profile Page</th>

      	  <td>

			<select   id="mer_theme_options[profile_page]" name="mer_theme_options[profile_page]" >
	                                <option value="">Later</option>
                                   
          <?php 	
 						foreach ( $sitepages as $pagg ) {
                                        $option = '<option value="' .  $pagg->ID .'" ' ;
                                        if( $pagg->ID==$options['profile_page']){
											$option .=  " selected='selected'";
										}
                                        $option .=  '>';
                                        $option .= $pagg->post_title;
                                        $option .= '</option>';
                                        echo $option;
                                      }
									  
									  ?>
                                      </select>  </td>

        </tr>
         <tr>

     <th width="25%"></th>

     <th >Footer</th>

     </tr>
 		<tr>
		<th width="25%">Telephone</th>
      	<td><input type="text" name="mer_theme_options[telephone]" id='mer_theme_options[telephone]' value="<?php echo $options['telephone']; ?>" />
      	</td>
        </tr>

        <tr>
		<th width="25%">Email</th>
      	<td><input type="text" name="mer_theme_options[email]" id='mer_theme_options[email]' value="<?php echo $options['email']; ?>" />
      	</td>
        </tr>
		 <tr>
		<th width="25%">Footer Information</th>
      	<td><textarea name="mer_theme_options[footer_info]" id='mer_theme_options[footer_info]' rows="5" cols="10" ><?php echo $options['footer_info']; ?></textarea>
      	</td>
        </tr>
      <tr>
		<th width="25%">Copyright Text</th>
      	<td><input type="text" name="mer_theme_options[copyright]" id='mer_theme_options[copyright]' value="<?php echo $options['copyright']; ?>" />
      	</td>
        </tr>

        

          <tr>
 
		<th width="25%">Pinterest Url</th>
      	<td><input type="text" name="mer_theme_options[pinterest]" id='mer_theme_options[pinterest]' value="<?php echo $options['pinterest']; ?>" />
      	</td>
        </tr>
 <tr>
		<th width="25%">Google Url</th>
      	<td><input type="text" name="mer_theme_options[google]" id='mer_theme_options[google]' value="<?php echo $options['google']; ?>" />
      	</td>
        </tr>
       <tr>
		<th width="25%">Facebook Url</th>
      	<td><input type="text" name="mer_theme_options[facebook]" id='mer_theme_options[facebook]' value="<?php echo $options['facebook']; ?>" />
      	</td>
        </tr>

         <tr>
		<th width="25%">Twitter Text</th>
      	<td><input type="text" name="mer_theme_options[twitter]" id='mer_theme_options[twitter]' value="<?php echo $options['twitter']; ?>" />
      	</td>
        </tr>
  <tr>
		<th width="25%">Linkedin Text</th>
      	<td><input type="text" name="mer_theme_options[linkedin]" id='mer_theme_options[linkedin]' value="<?php echo $options['linkedin']; ?>" />
      	</td>
        </tr>  
<tr>
		<th width="25%">Youtube Text</th>
      	<td><input type="text" name="mer_theme_options[youtube]" id='mer_theme_options[youtube]' value="<?php echo $options['youtube']; ?>" />
      	</td>
        </tr>
       </thead>

     </table>


     <br/>

     

<?php 
 require_once(get_template_directory() .'/admin/profile.php');

?>		
  <br/>
	
<?php 
 require_once(get_template_directory() .'/admin/billing.php');
}
?>
	