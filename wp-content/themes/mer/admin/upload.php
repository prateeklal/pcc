<?php  

$options = get_option( 'mer_theme_options' );
   $checkdata=array(
   "venue",
   "booking_information",
   "video",
   "educational_objective",
  "room_rates",
   "room_rate_for",
  "promotion_credits",
  "meeting_code",
  "pricing_physician",
  "pricing_other",
  "promotion_exp_date",
  "start_date",
  "end_date",
  "earlyfee_date",  
  "display_date",
  "course_url",  
  "address",
  "address2",
  "area_site",
  "booking_link",
  "map_link",
  "city",
  "state",
  "postal_code",
  "country",
  "group_code",
  "phone",
  "booking_phone",
  "web_site",
  "passport_required",
  "airport_name",
  "extra_note",
  "course",
  "day",
  "start_time",
  "end_time",
	"an_transaction_id",
	"user_id",
	"registration_id",
	"first_name",
	"last_name",
	"note",
	"transaction_category",
	"transaction_amount",
	"activity_info",
	"registration_fee_type",
	"registration_fee",
	"cc_num",
	"refunded",
	"transaction_source",
	"balance_due",
	"created" ,
	"seminar_course",
	"featured_cat_post",
	"featured_location_post",
	"middle_name",
	"last_name",
	"instution",
	"mailing_address1",
	"Mailing Address2",
	"city",
	"state",
	"country",
	"postal_code",
	"day_work",
	"home_phone",
	"billing_first_name",
	"billing_last_name",
	"billing_address1",
	"billing_address2",
	"billing_city",
	"billing_state",
	"billing_postal_code",
	"billing_country",
	"billing_day_work",
	"billing_home_phone"
  
);
$downloadfile='';
$msg='';
$emsg='';
$vdir="jp" . DIRECTORY_SEPARATOR;
$tmp=$_SERVER['DOCUMENT_ROOT']. $vdir ."tmp";
$check_cat_array=array('category',
'activity',
'ammentie',
'conference',
'region',
'location',
'agenda_item',
'like_information',
'speciality',
'degree',
'hear_about_seminar',
'user_title',
);


$check_user_array=array(
'activity'=>'activities',
'like_information'=>'like_information',
'speciality'=>'speciality',
'degree'=>'degree',
'hear_about_seminar'=>'hear_about_seminar',
'user_title'=>'title',
);


if (!function_exists('get_tax_meta'))
    require_once("Tax-meta-class.php");
//$saved_data = get_tax_meta(65,'image');

if (!function_exists('image_resize'))
require_once(ABSPATH . '/wp-admin/includes/image.php');	

 $check_post_array=array( 
'agenda',
'featured',
'seminar',
'transaction',
'user',
'registration'
);

/*$check_post_array=array('transaction',
 

);*/


$post_array=array(
'registration_id',
'course',
'conference',
'venue' 

);

if(isset($_POST['download'])){

	 $taxonomies=get_taxonomies('','names');
	$mxml='';
	foreach ($taxonomies as $taxonomy )
	{
		
	 if(in_array($taxonomy,$check_cat_array))
	 {
		$folderxml='';
		if(!file_exists($tmp)){
				mkdir($tmp);
		}
	  $terms = get_terms( $taxonomy, 'orderby=count&hide_empty=0' );
			
	  if (strpos($taxonomy ,'photo') !== false) 
				{
					$tmp2=$tmp . DIRECTORY_SEPARATOR .$taxonomy;
				
					if(!file_exists($tmp2)){
						mkdir($tmp2);
					}
					
				$folderxml="<imagefolder>$taxonomy</imagefolder>"; 
				
				}
	  
	  $xml='';
	   $imgxml='';
	  foreach ( $terms as $term ) {
				
				if (strpos($taxonomy ,'photo') !== false) 
				{
					$tmp2=$tmp . DIRECTORY_SEPARATOR .$taxonomy;
				
					if(!file_exists($tmp2)){
						mkdir($tmp2);
					}
				
				$file = get_tax_meta($term->term_id,'image');
				$imid=$file['id'];
				$basename = basename($file['src']);
				copy($file['src'],$tmp2. DIRECTORY_SEPARATOR .$basename) ;
				 
				 
				if($basename!=''){
					$basename="<![CDATA[$basename]]>";
					
				}
				
					$imgxml="\r\n\t\t<image>\r\n\t\t\t<id>$imid</id>\r\n\t\t\t<src>$basename</src>\r\n\t\t</image>";
						}
				
				 
				$termname='';
				
				if($term->name!=''){
					$termname="<![CDATA[" . $term->name . "]]>";
					
					}
				
				if($folderxml!=''){
				$xml.=  $xml."\r\n\t$folderxml\r\n\t<item>\r\n\t\t<id>" . $term->term_id . "</id>\r\n\t\t<name>$termname\r\n\t\t</name>$imgxml</item>";	
					
				}else{
						
				$xml= $xml."\r\n\t<item>\r\n\t\t<id>" . $term->term_id . "</id>\r\n\t\t<name>$termname</name>$imgxml</item>";
				}
					
				$folderxml='';
				}
				
				 $mxml=$mxml."<$taxonomy>$xml\r\n</$taxonomy>\r\n";
		
		}// if checkarray	
	  
	 }//foraech
	 
	 //$content=$mxml;
	 
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$post_types=get_post_types('','names'); 
	
	$post_typexml='';
	foreach ($post_types as $post_type ) {
	  
	if(in_array($post_type,$check_post_array)){
		
				$args=array(
					  'post_type' => $post_type,
					  'post_status' => 'publish',
					  'posts_per_page' => -1,
					  'caller_get_posts'=> 1
					);
					
				$posts=get_posts($args);
			
			 foreach ( $posts as $post ) { //get post type items
					
					$post_type_item_xml="\r\n\t<id>" . $post->ID . "</id>";	
					$name='';
					
					if($post->post_title!=''){
						$name="<![CDATA[" . $post->post_title . "]]>";
					
					}
					$post_type_item_xml=$post_type_item_xml."\r\n\t<title>" . $name . "</title>";			
					if($post_type=='user'){
					$userid=get_post_meta($post->ID,'user_id',true);	
					$post_meta=get_user_meta($userid);
					}else{
						$post_meta=get_post_meta($post->ID);
						}
					
					foreach($post_meta  as $k=>$v)
					{						
						if(is_array($v)){
									  $val=$v[0];							
							}else{ 
								$val=$v;
							}
							
							
							if(in_array($k,$checkdata)){
								if(in_array($k,$post_array)){
										$tmp_post=get_post($val);
										$name='';
										if($tmp_post->post_title!=''){
											$name="<![CDATA[" . $tmp_post->post_title . "]]>";
										
										}
										$post_type_item_xml.="\r\n\t<$k>\r\n\t\t\t<item>\r\n\t\t\t\t<id>".$post_term->term_id."</id>\r\n\t\t\t\t<name>".$name."</name>\r\n\t\t\t</item>\r\n\t</$k>";
								 }
								elseif($k=='user_id'){
									
										$tmp_post=get_userdata($val);
										
										if($tmp_post->user_login!=''){
											$name="<![CDATA[" . $tmp_post->user_login . "]]>";
										
										}
										$post_type_item_xml.="\r\n\t<$k>\r\n\t\t\t<item>\r\n\t\t\t\t<id>".$val."</id>\r\n\t\t\t\t<user_name>".$name."</user_name>\r\n\t\t\t</item>\r\n\t</$k>";
								} 
								 
								 else{
									 $name='';
										if($val!=''){
											$name="<![CDATA[" . $val . "]]>";
										
										}
									 
									$post_type_item_xml.="\r\n\t<$k>$name</$k>";
								}
							}
											
					}// for meta
						
					
				 
					 $files = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'order' => 'ASC', 'orderby' => 'menu_order ID') );  
  
				$results = array();  
			  $imgxml='';
				if ($files) { 
				
				 if(!file_exists($tmp .DIRECTORY_SEPARATOR. $post_type)){
					 mkdir($tmp .DIRECTORY_SEPARATOR. $post_type);
					 }
					
					 
					$imgxml="";
					foreach ($files as $file) { 
					//var_dump($file); 
					if($file->post_mime_type != 'application/pdf'){
						 $path = wp_get_attachment_url($file->ID);  
						$path_parts = pathinfo($path);  
						$tmpimgxml='';
						$name='';
										if($file->post_title!=''){
											$name="<![CDATA[" . $file->post_title . "]]>";
										
										}
						if(file_exists($path))				
						copy($path ,$tmp .DIRECTORY_SEPARATOR. $post_type.'/'.$path_parts[basename]);				
						
						$tmpimgxml= "\r\n\t\t\t<item><id>".$file->ID."</id><name>$name</name><file>".$path_parts[basename]."</file>\r\n\t\t\t</item>";  
					} 
					 $folder="tmp/$post_type";
					 $imgxml="\r\n\t<images>\r\n\t\t<item><folder>$folder</folder>$tmpimgxml\r\n\t\t<\item>\r\n\t</images>";
					}
				}  	
					
				
					
				$attachposts=get_posts($attachargs);
					 $files = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment','post_mime_type' => 'application/pdf', 'order' => 'ASC', 'orderby' => 'menu_order ID') );  
  
				
				if ($files) { 
				
				 if(!file_exists($tmp .DIRECTORY_SEPARATOR. $post_type)){
					 mkdir($tmp .DIRECTORY_SEPARATOR. $post_type);
					 }
					
					 
					$pdfxml="";
					foreach ($files as $file) { 
					//var_dump($file); 
						 $path = wp_get_attachment_url($file->ID);  
						$path_parts = pathinfo($path);  
						
						$name='';
										if($file->post_title!=''){
											$name="<![CDATA[" . $file->post_title . "]]>";
										
										}
						if(file_exists($path))				
						copy($path ,$tmp .DIRECTORY_SEPARATOR. $post_type.'/'.$path_parts[basename]);				
						
						$pdfxml.= "\r\n\t\t\t<item><id>".$file->ID."</id><name>$name</name><file>".$path_parts[basename]."</file>\r\n\t\t\t</item>";  
					} 
					 $folder="tmp/$post_type";
					 $pdfxml="\r\n\t<pdf>\r\n\t\t<item><folder>$folder</folder>$pdfxml\r\n\t\t<\item>\r\n\t</pdf>"; 
				}  	
						
					
					$imgfolder='';
					// GET CATEGORIES OF POST TYPE
					 foreach( get_object_taxonomies( $post_type ) as $tax){
						$txml='';
						$Ltxml='';
						$post_type_item__cat_xml='';
						$post_terms=array();
						if ($post_type=='user' || $post_type=='registration' ){
							 $userid. $check_user_array[$tax];
							$terms=get_user_meta($userid,$check_user_array[$tax],true);
						if(is_array($terms)){	
							foreach( $terms as $term){
								$post_terms[]=get_term( $term, $tax);
							}
						}
						else{
							$post_terms[]=get_term( $terms, $tax);
							
							}
						
						
						 
							}else{
						$post_terms=wp_get_post_terms($post->ID,$tax,array("fields" => "all"));
							}
					   
						foreach( $post_terms as $post_term){
						$name='';
										if($post_term->name!=''){
											$name="<![CDATA[" . $post_term->name . "]]>";
										
										}
						$post_type_item__cat__term_xml="\r\n\t\t\t<id>".$post_term->term_id."</id>\r\n\t\t\t<name>".$name."</name>";
								
							if (strpos($tax ,'photo') !== false){
								$file = get_tax_meta($post_term->term_id,'image');
								$imid=$file['id'];
								$basename = basename($file['src']);
								//$file=get_post_meta($imid,'_wp_attached_file',true);
								if($file!=''){
								
								//copy($file['src'],$tmp2. DIRECTORY_SEPARATOR .$basename) ;
								$file="<![CDATA[$file]]>";	
							
								}
								 
								$post_type_item__cat__term_xml= $post_type_item__cat__term_xml."\r\n\t\t\t<image>\r\n\t\t\t\t<id>$imid</id>\r\n\t\t\t\t<src><![CDATA[$basename]]></src>\r\n\t\t\t</image>";
							}//Image check
							
							
							//join term with category	
							$post_type_item__cat_xml= $post_type_item__cat_xml."\r\n\t\t<item>$post_type_item__cat__term_xml\r\n\t\t</item>
							";
							
							$post_type_item__cat__term_xml='';
							
							}// for terms
							
							$txml=$txml.$Ltxml;
							//join category with post type item
							if (strpos($tax ,'photo') !== false){
						 
									$post_type_item_xml=$post_type_item_xml."\r\n\t\t<$tax>\r\n\t\t<imagefolder>$tax<imagefolder>$post_type_item__cat_xml\r\n\t\t</$tax>";
								}else{
									$post_type_item_xml=$post_type_item_xml."\r\n\t\t<$tax>$post_type_item__cat_xml\r\n\t\t</$tax>";	
								}
								
								 
								$post_type_item__cat_xml='';
							
				}// FOR TAX 
					
						$post_type_item_xml= "\r\n\t<item>$post_type_item_xml\r\n\t $imgxml \r\n\t  $pdfxml </item>";
						
						//join post type item	 with post type
				$post_typexml=$post_typexml.$post_type_item_xml;
				$post_type_item_xml='';
				
			}// for post
				
				 $post_typexml="\r\n<$post_type>$post_typexml\r\n</$post_type>";
								//join post type  with content
				$content=$content.$post_typexml;		
			$post_typexml='';			
	  }//check post array
	
	} //for post type
	 $content="<data>$content</data>";//final one data Node
	 
	 $file= $tmp . DIRECTORY_SEPARATOR ."export.xml" ;
		
	 $fp = fopen($file, 'w');
	 fwrite($fp,  $content);
	fclose($fp);
$downloadfile="<a href='" .site_url()."/tmp/export.xml'>download file from here</a>";
 }//if download
 
 ///////////////////////////////////////////////////////////////////////////////////////////////////
 
//$term= get_term_by('name','Las Vegas','region');
if(isset($_POST['upload'])){
	
  $upload_option=	$_POST['upload_option'];

	
	$allowedExts = array("txt", "xml");
	$extension = end(explode(".", $_FILES["upload_file"]["name"]));
	 
	if (  in_array($extension, $allowedExts))
	  {
	 
		  
	  if ($_FILES["upload_file"]["error"] > 0)
		{
		$emsg= "Error: " . $_FILES["upload_file"]["error"] . "<br>";
		}
	  else//inner Else
		{
		  
					
				$xmldata = simplexml_load_file($_FILES['upload_file']['tmp_name']);
				
				/*foreach( $check_cat_array as $cat )
				 { 
					
					$catitem=$xmldata->xpath("/data/$cat/item");
					
							 foreach( $catitem as $item )
							 { 
								$id  = '';
								$v ='';
							  $id = $item->id;
							 $v = $item->name;
							 
							
							if($id>0){
							$term= get_term(  $id,$cat );
							}else{
							 $term= get_term_by('name',$v,$cat );	
								
							}
							
							 $slug=sanitize_title($v);
							
							if ($term->term_id) {
							  wp_update_term( $id,$cat , array(
									  'name' => $v,
									  'slug' => $slug
									));
									 echo "$v updated <br/>";
							}else{
								
								   wp_insert_term( $v,$cat , array(
									  'description' => '',
									  'slug' => $slug
									));  
									 echo "$v added <br/>";
								 } 
							 } 
							 
						 
				 
				 
				 }//Cat For Each*/
				 
				foreach( $check_post_array as $post )
				 { 
					 $postitem=$xmldata->xpath("/data/$post/item");
					  	
							 foreach( $postitem as $item )
							 { 
								$id  = '';
								$v ='';
								
							 $id = $item->id;
							 $v = $item->title;
								 
								 $postargs=array(
								 'id'=>$id,
								 'post_title'=>$v,
								 'post_type'=>$post ,
								 'post_status'=>'published',
								 'post_name' => sanitize_title( $v)								 
								 );
								//$id=wp_insert_post($postargs);
								 foreach($item as $key=>$val  )
                     			{
									 
									 
									 if(in_array( $key,$checkdata)){
										
										//update_post_meta($id,$key,$val) ; 
									 }
									
								}
								
								
								
							
							 
							 } 
							 
						 
				 
				 
				 }//Cat For Each
		}
	  }//inner Else
	else
	  {
	  $emsg= "Invalid file :Size should not be more than 2 mb";
	  }
		
		
}
		
/*$args = array(
   'post_type' => 'seminar' , // This is where you should put your Post Type 
   'posts_per_page' => -1 
	
	);

$posts=get_posts($args);

foreach($posts as $mypost)
{
	$start_date=get_post_meta($mypost->ID,'start_date',true); 
	$mon1=date("M", strtotime($start_date));
	$date1=date("d", strtotime($start_date));
	//$mon1=date("m", strtotime($start_date));
	$year1=date("Y", strtotime($start_date));
	$end_date=get_post_meta($mypost->ID,'end_date',true); 
	$date2=date("d", strtotime($end_date));
	
	$disp_date="$mon1 $date1-$date2, $year1";
	update_post_meta($mypost->ID,'display_date',$disp_date);
	$targs = array(
   'post_type' => 'featured' , // This is where you should put your Post Type 
   'posts_per_page' => 1,
   'meta_query' => array(
									array(
									'key' => 'course',
									'value' => $mypost->ID
									)
   						) 
	
	);

	$tposts=get_posts($targs);
	foreach($tposts as $tmypost)
	{
	$post_terms=wp_get_post_terms($tmypost->ID,'category',array("fields" => "names"));
	  
	  wp_set_object_terms($mypost->ID,$post_terms,'category');
	
	} 
					
}*/

	?>

	


		<?php if ( $msg !== '' ) : ?>

		<div class="updated fade"><p><strong><?php _e( 'Options saved', 'mertheme' ); ?></strong></p></div>

		<?php endif; ?>



		<form enctype="multipart/form-data" method="post"  >

			

     <table cellspacing="0" class="wp-list-table widefat fixed media" >

     <thead>

     <tr>

     <th  valign='top'width="25%"></th>

     <th  valign='top'>&nbsp;</th>

     </tr>

    
<!--<tr><th >Upload Option</th>

      	  <td>

			<select   id="upload_option" name="upload_option" >
	                                <option value="">Later</option>
                                    <option value="category">Category</option>
                                    <option value="ammentie">Ammenties</option>
                                      <option value="activity">activity</option>
                                      <option value="region">region</option>
                                      <option value="seminar">seminar</option>
                                      <option value="agenda_item">Agenda Item</option>
                                      <option value="conferences">conferences</option>
                                      <option value="course_photo">Course Photo</option>
                                      <option value="venue_photo">Venue Photo</option>
                                      <option value="venue">Venue</option>
                                       <option value="hear_about_seminar">Hear About Seminar</option>
                                       <option value="user_title">User Title</option>
                                       <option value="degree">Degree</option>
                                       <option value="like_information">Like Information</option>
                                       <option value="speciality">Speciality</option>
                                      </select>  </td>

        </tr>-->

   <tr><th >Browse File</th>

      	  <td><input type="file" name="upload_file"  /> <input type="submit" class="button-primary" name="upload" value="<?php _e( 'Upload', 'mertheme' ); ?>" /></td></tr></tfoot>


			  </td>

        </tr>

       

       </thead>

     </table>


     <br/>

     

    

    </thead>

    </table>

		</form>
        <br/>
   <form method="post"  >

   <table cellspacing="0" class="wp-list-table widefat fixed media" >

     <thead>

     <tr>

     <th  valign='top'width="25%"></th>

     <th  valign='top'>&nbsp;</th>

     </tr>

    
<tr><th >Download File</th>

      	  <td>
      <!--       <select   id="download_option" name="download_option" >
	                                <option value="">Later</option>
                                    <option value="category">Category</option>
                                    <option value="ammentie">Ammenties</option>
                                      <option value="activity">activity</option>
                                      <option value="region">region</option>
                                      <option value="seminar">seminar</option>
                                      <option value="agenda_item">Agenda Item</option>
                                      <option value="conferences">conferences</option>
                                      <option value="venue_photo">Venue Photo</option>
                                       <option value="venue">Venue</option>
                                      </select> 
          
                                      </select>-->  <input type="submit" class="button-primary" name="download" value="<?php _e( 'Download', 'mertheme' ); ?>" />
                                      <?php echo $downloadfile; ?>
                                      </td></tr></tfoot>
 </td>

        </tr>
	 

    </table>

		</form>

	
	