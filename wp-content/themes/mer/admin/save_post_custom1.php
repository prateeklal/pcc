<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*  When the post is saved, saves our custom data */
function mer_save_postdata( $post_id ) {      
  // verify if this is an auto save routine. 
  // If it is our form has not been submitted, so we dont want to do anything
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;
  // verify this came from the our screen and with proper authorization,
  // because save_post can be triggered at other times
  if ( !wp_verify_nonce( $_POST['mer_noncename'], plugin_basename( __FILE__ ) ) )
      return;
  // Check permissions
  if ( 'page' == $_POST['post_type'] ) 
  {
    if ( !current_user_can( 'edit_page', $post_id ) )
        return;
  }
  else
  {
    if ( !current_user_can( 'edit_post', $post_id ) )
        return;
  } 

  // OK, we're authenticated: we need to find and save the data

  //if saving in a custom table, get post_ID
  $post_ID = $_POST['post_ID'];
  
  
  
  $checkdata=array(
  //--------Conference Columns
'__kf_Topic_ID','__kf_Venue_ID','__kp_CourseID','Airfare_Statement','Course_Display_Date','Course_Educational_Objective','Course_End_Date','Course_Location','Course_Lowest_Room_Rate','Course_Start_Date','Course_Topic','Course_Topic_ID','Course_Travel_Text','Course_Venue','Course_Venue_Address','Course_Venue_Address_2','Course_Venue_Airport_Code_1','Course_Venue_Airport_Name_1','Course_Venue_Amenities_Text_List','Course_Venue_Area_Site','Course_Venue_Booking_Link','Course_Venue_Check_In_Time','Course_Venue_Check_Out_Time','Course_Venue_City','Course_Venue_Country','Course_Venue_Map_Site','Course_Venue_Passport_Flag','Course_Venue_Reservation_Group_Code','Course_Venue_Reservation_Phone','Course_Venue_State','Course_Venue_Web_Site','Course_Venue_Zip','Early_Fee_Date','MeetingCode','Notes_Accommodations','Pricing_Early_Other','Pricing_Early_Physician','Pricing_Early_Promo_Amount','Pricing_Other','Pricing_Physician','Web_Category_1','Web_Category_2','Credits','create_date','course_live_flag','Course_Duration','Course_Venue_instructions','Course_Venue_Special_Messages',

//----------Agenda Columns
 
'__kf_Course_ID','__kp_agenda_items_ID','Agenda_Item_Faculty','Agenda_Item_Title','Agenda_Item_Date','Agenda_Item_Start_Time','Agenda_Item_End_Time','day','description',
//----------Registration Columns
'__kf_Attendee_ID','__kf_CourseID','__kp_Registration_ID','Registration_Fee','Registration_Date','Registration_Status',
//----------Transaction Columns
 
'CC_Num','CC_Type','First_Name','Last_Name','Note','Registration_Fee_Type','__kf_Registration_ID','Conf_Start_Date','Transaction_Amount','Transaction_Category','__kp_Transaction_ID','Transaction_Source','__kf_Attendee_ID','AN_Transaction_ID','AN_CC_Number','AN_Transaction_Amount','AN_CC_Type','create_date','Record_correction','Cancel_Date','Credit_Expire_Date','Is_Credit_Used',
//----------Topic Columns

'__kp_Topic_ID','Educational_Objectives','Nursing_Purpose','Topic_Order','Target_Audience','Topic_Chair_1','Topic_Chair_2','Topic_Full_Name',
  //----------Unique Columns
  'mer_key'
  
);

global $wpdb;
$data=array(); 

  $mer_key=$_POST['mer_key']; 
  
  $updateid=get_post_meta($post_ID,$mer_key,true); 
  $tbl  =$wpdb->prefix.$_POST['mer_tbl'];
   $tbl_detail  = $tbl."_details";
 
  foreach($_POST as $k=>$v){
	  if ($k==$mer_key && $v==''){
 
	   $sql = "SELECT max($mer_key)+1  from $tbl";  
			 $v= $wpdb->get_var($sql );
	 }
	  if(in_array($k,$checkdata)){
		  	$data[$k]=$v;
			update_post_meta(   $post_ID,$k,$v); 
	  }
  
 }
 // $update[]=$postarr;
 
 
 
 unset($data['mer_key']);
 if(isset($_POST['create_date'])){
  if($data['create_date']==''){
	$data['create_date']=date('Y-m-d');
	}
 }	
 //{ Transaction 
 if ($mer_key=='__kp_Transaction_ID'){
 
  
 	 $sendmail=0;
	  $cred_name=$data['First_Name'] ." ". $data['Last_Name'];
 	  $prev_amount=$_POST['Prev_Transaction_Amount'];
	 $prev_Transaction_Category=$_POST['pre_Transaction_Category'];
	 $credits_available=get_user_meta($data['__kf_Attendee_ID'],'credits_available',true);
	  if($credits_available==''){
	 	$credits_available=0;
		 }
	if($data['Transaction_Category']=='Registration CREDIT USED'){
		 
		$amount=$data['Transaction_Amount'];
		
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
			$credits_available=$credits_available+$prev_amount;
		} 
		
		$credits_available=$credits_available-$amount;
		// if(!is_super_admin( $data['__kf_Attendee_ID'] ))	//update only if user is not super admin		
		 $wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) );
	}
	
	/*if($data['Transaction_Category']=='Registration REFUND'){
	
					$islive=false;
					$requesturl="https://test.authorize.net/gateway/transact.dll";
					$login="44hKSq9Up3XX";
					$tran_key="6W8Xm328FmC7nTaQ";
					$login=$options['api_key'];
					$tran_key=$options['api_password'];
					$api_mode=$options['api_mode'];    
					$x_trans_id=$data['AN_Transaction_ID'];
					 $pricing=$_POST['payment_type'];
					$credit=$_POST['credit'];						
					//Make It live
							if($api_mode=='live'){
								
								$requesturl="https://secure.authorize.net/gateway/transact.dll";
								//$login="7Jqq22UW";
								//$tran_key="8YuxC882942dvnFk";
							}
							  // require_once('authorizenet.class.php');
							$a = new authorizenet_class;
							 $a->add_field('x_login', $login);
							$a->add_field('x_tran_key', $tran_key);
							$a->add_field('x_trans_id', $x_trans_id);
							$a->add_field('x_type', 'CREDIT');
							 $a->gateway_url=$requesturl;
							$a->add_field('x_version', '3.1');
							$a->add_field('x_type', 'AUTH_CAPTURE');
			   				// Just a test transaction
							$a->add_field('x_relay_response', 'FALSE');
							//$a->add_field('x_trans_id', 'abc12345');
							// You *MUST* specify '|' as the delim char due to the way I wrote the class.
							// I will change this in future versions should I have time.  But for now, just
							// make sure you include the following 3 lines of code when using this class.
							$a->add_field('x_delim_data', 'TRUE');
							$a->add_field('x_delim_char', '|'); 
							$a->add_field('x_encap_char', '');
							if($api_mode=='live'){
							    $a->add_field('x_test_request', 'FALSE');
							}else{
								$a->add_field('x_test_request', 'TRUE');
							} 
							
							switch ($a->process($islive))
							{  
							 case 1:  // Successs
							   
							   $data['Note']=$a->get_response_reason_text();  
								 $data['AN_Transaction_ID']=$a->response['Transaction ID'];
							 
							  	break;
							 case 2:  // Fail
								echo $a->get_response_reason_text();
								  break;
								//  exit;
							}
							
	}*/
 	if($data['Transaction_Category']=='Cancellation CREDIT'){
		 
		$data['Cancel_Date']=date('Y-m-d');
		$amount=$data['Transaction_Amount'];
		$credits=$amount-50;
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
			 $credits=$amount;
			 $credits_available=$credits_available-$prev_amount;
			
		} else{
			$data2=$data;
			$data2['Transaction_Amount']=50;
			 $data2['Transaction_Category']='Cancellation FEE';
			 $data2['Conf_Start_Date']='';
			 $data2['CC_Num']='';
			 $data2['CC_Type']='';
			 
			 $data2['Transaction_Source']='';
			 $data2['AN_Transaction_ID']='';
			$data2['AN_CC_Number']='';
			$data2['Note']=''; /**/
			$sql = "SELECT max($mer_key)+1  from $tbl";  
			 $data2[$mer_key]= $wpdb->get_var($sql );
			//unset($data2[$mer_key]);
			$wpdb->insert($tbl , $data2 ); 
			//$data[$mer_key] =$maxid;
	
			$sendmail=1;
		}
		
		$credits_available=$credits_available+$credits;
		
		
		//if(!is_super_admin( $data['__kf_Attendee_ID'] ))//update only if user is not super admin	
		$wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) );
	
	$reg_body="Credit of $credits_available has been issued to $cred_name";
		if($sendmail==1)
		mail("noncredit@mer.org", "Credit Issued", $reg_body, "From: \"New credit issued\" <auto-reply@$host>\r\n" . "X-Mailer: PHP/" . phpversion());
   
	
	}
	
	if($data['Transaction_Category']=='Cancellation REFUND' || $data['Transaction_Category']=='Registration REFUND'){
		$data['Cancel_Date']=date('Y-m-d');
		$credits=$data['Transaction_Amount'];
		  $prev_Transaction_Category;
		  $data['Transaction_Category'];
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
			  $credits_available=$credits_available-$prev_amount;
			
		}  else{
			$data2=$data;
			 
			$sql = "SELECT max($mer_key)+1  from $tbl";  
			 $data2[$mer_key]= $wpdb->get_var($sql );
			//unset($data2[$mer_key]);
			$wpdb->insert($tbl , $data2 ); 
			//$data[$mer_key] =$maxid;
	
			$sendmail=1;
		}
		
		$reg_body= ucwords( $data['Transaction_Category']) ." of ". $data['Transaction_Amount']." has been issued to $cred_name";
		 if($sendmail==1){
		mail("noncredit@mer.org",  ucwords( $data['Transaction_Category']) , $reg_body, "From: \"".ucwords( $data['Transaction_Category'])."\" <auto-reply@$host>\r\n" . "X-Mailer: PHP/" . phpversion());}
		
		
		  $credits_available=$credits_available+$credits;
		//  if(!is_super_admin( $data['__kf_Attendee_ID'] ))	//update only if user is not super admin	
		 $wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) );
		 
		  
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	if($data['Transaction_Category']=='Courtesy CREDIT' || $data['Transaction_Category']=='Loyalty Credit'  || $data['Transaction_Category']=='Promo Applied' ){ 
			 
		  $amount=$data['Transaction_Amount'];
		
		if($prev_amount>0 && $prev_Transaction_Category==$data['Transaction_Category']){
		 	 $credits_available=$credits_available-$prev_amount;
			
		}else{
		$sendmail=1;
		}  
	
		
		  $credits_available=$credits_available+$amount;
		$host="primarycareconferences.com";
		//update_usermeta( $data['__kf_Attendee_ID'], 'credits_available',$credits_available);
		 //echo $data['__kf_Attendee_ID'];
	
		
		// if(!is_super_admin( $data['__kf_Attendee_ID'] ))	//update only if user is not super admin		
		 $wpdb->update($wpdb->prefix.'attendees'  , array('credits_available'=>$credits_available),array('__kp_Attendee_ID'=>$data['__kf_Attendee_ID']) ); 
		
		$reg_body="Credit of $credits_available has been issued to $cred_name";
		if($sendmail==1)
		mail("noncredit@mer.org", "Credit Issued", $reg_body, "From: \"New credit issued\" <auto-reply@$host>\r\n" . "X-Mailer: PHP/" . phpversion());

	}
 
 
 
 }
  
  	
 
	 if($updateid=='')
		 { 
			  $wpdb->insert($tbl , $data ); 
			
		 }
		 else{
	 	$where=array($mer_key=>$updateid);
		  $wpdb->update($tbl , $data,$where );  
		}
			
	  /*echo updateData($tbl , $data,$where);	
	exit;	*/
		
	/*	echo " <div style='width:500px' >";
 var_dump($data);
 echo "</div> ";
 */
	  
  // Do something with $mydata 
  // probably using add_post_meta(), update_post_meta(), or 
  // a custom table (see Further Reading section below)
}

/* Do something with the data entered */
add_action( 'save_post', 'mer_save_postdata' );
?>
