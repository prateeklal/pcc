<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>

<meta http-equiv="refresh" content="10;url=http://www.mer.org/; charset=utf-8">
<title>MER Primary Care Conferences</title>

<script type="text/javascript">
	window.onload=function() {
	function countdown() {
	if ( typeof countdown.counter == 'undefined' ) {
    	countdown.counter = 10; // initial count
    	}
	if(countdown.counter > 0) {
		document.getElementById('count').innerHTML = countdown.counter--;
    	setTimeout(countdown, 1000);
    	}
	else {
    	location.href = 'http://www.mer.org/';
    	}
	}
	countdown();
	};
</script>

<style type="text/css">
  
    * {
	margin:0px;
	padding:0px;
	}

	body {
    font-family: Helvetica, Arial, sans-serif;
    color: #5f6062;
    background-color: #bdbdb0;
    border: 0;
    border-collapse: collapse;
    margin: 0px;
    padding: 0px;
    }
    
	#wrapper {
    margin: 0 auto;
    width: 800px;
    padding: 0px;
    }
    
    #header {
    background-color: #5f6062;
    width: 800px;
    height: 350px;
    padding: 0px;
    margin: 0 auto;
    }
    
    #content {
    background-color: #5f6062;
    width: 800px;
    padding: 20px 0px 60px 0px;
    margin: 0 auto;
    }
    
    #footer {
    background-color: #5f6062;
    width: 800px;
    padding: 0px;
    margin-top: 2px;
    margin-right: 0 auto;
    margin-bottom: 0 auto;
    margin-left: 0 auto;
    clear: both;
    border-radius: 0px 0px 20px 0px;
	-moz-border-radius: 0px 0px 20px 0px;
	-webkit-border-radius: 0px 0px 20px 0px;
	border: 0px none;
    }
        
    h1 {
    font-family: Helvetica, Arial, sans-serif;
    font-size: 24px;
    line-height: 28px;
    color: #ffffff;
    margin: 0px;
    padding: 0px;
    text-align: center;
    }
    
    h2 {
    font-family: Helvetica, Arial, sans-serif;
    font-size: 14px;
    line-height: 18px;
    color: #8d8d8d;
    margin: 0px;
    padding: 30px 0px 0px 0px;
    text-align: center;
    font-weight: normal;    
    }
    
    p {
    font-family: Helvetica, Arial, sans-serif;
    font-size: 12px;
    line-height: 16px;
    color: #8d8d8d;
    text-align: center;
    margin: 0px;
    padding: 20px;
    }
    
    a {
    text-decoration: none;
    color: #aaaaaa;
    }

    a:hover {
    text-decoration: none;
    color: #8d8d8d;
    }
    
    </style>
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<a href="http://www.mer.org"><img src="https://db.tt/42NamAEC" alt="banner" width="800px" height="350px"></a>
		</div>
		<div id="content">
			<h1>Our new site will be coming soon.<br/>
			<span style="font-size: 19px">In the mean time, please visit our current site at:<br/>
			<a href="http://www.mer.org" style="font-size: 36px; line-height: 60px">www.MER.org</a>
			</h1>
			<h2>(You will be redirected in <span id="count"></span> seconds)</h2>
		</div>
		<div id="footer">
			<p>MER / 9785 Maroon Circle, Suite 100 / Englewood, CO 80112<br/>
			tel. 303-798-9682 or 1-800-421-3756 / fax 303-798-5731 / email: <a href="mailto:info@mer.org?subject=Primary%20Care%20Conferences" style="color:#8d8d8d">info@mer.org</a><br/>
			&#169;2015 MER / All rights reserved.
			</p>
		</div>
	</div>
</body>

</html>