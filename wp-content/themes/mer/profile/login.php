<?php

session_start();
/**

 * Template Name:  login

 * Description: login Page for user

 *

 * @package WordPress

 * @subpackage mer

 */

$detect = new Mobile_Detect();

if ($detect->isMobile()) {
require_once(get_template_directory() .'/mobile/profile/login_mobile.php');

}

else{
	
	
 // require_once(get_template_directory() .'/web/profile.php');

					

$options = get_option( 'mer_theme_options' );
$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];
 $profile_page=$options['profile_page'];
$register_page=$options['register_page'];
$msg='';
$emsg='';

if(isset($_REQUEST['logout'])){
	wp_logout();
header("Location:".site_url());
exit;

}
if(isset($_REQUEST['changeUser'])){
	$user_name=$_REQUEST['log'];
	$user = get_user_by('email', $user_name);
	//var_dump($user );
	if($user){$msg='<div class="bigmsg">Your user name  is :'.$user->user_login ."</div>";}else{
		$emsg="This email does not exists ,Please try again";
		 $_REQUEST['forget']=1;
		
		}
	
}else if(isset($_REQUEST['changePassword'])){
	$user_name=$_REQUEST['log'];
	
	$user_id = username_exists( $user_name );
	
	if ( $user_id  ) {
  $user_info=get_userdata($user_id);
	$lastname=$user_info->last_name; 
	$title= get_user_meta($user_id, 'title', true);
	$reset_subject=$options['reset_subject'];
	
	$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
	 wp_set_password( $random_password, $user_id );
	 update_user_meta($user_id, 'user_pass', $random_password);
	
	$user= get_userdata($user_id); 
	$msg=$options['reset_body'];
	$msg=html_entity_decode($msg);
	 $msg=nl2br($msg);
  
	  $from=$options['reset_mail_from'];
	if($from==''){
		$from="support@$host";
	}
	if($reset_subject==''){
		$reset_subject="Your password has been reset";
	}
	$headers  = 'MIME-Version: 1.0' . "\r\n";
								$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
									// Additional headers
								$headers .= 'From: '. $from . "\r\n";
							//	mail($email, $sub, $message,$headers);
			$msg=str_replace("[title]",$title,$msg);
			$msg=str_replace("[lastname]",$lastname,$msg);
			$msg=str_replace("[password]",$random_password,$msg);
	

    mail($user->user_email, $reset_subject , $msg, $headers);
	
		
	$msg="Your password has been reset and sent to your email.";	
		
	}else{
		$emsg="This Username does not exist, please try again";
		$_REQUEST['resetp']=1;
		}
	
	
}

else if(isset($_REQUEST['log'])){


$user_name=$_REQUEST['log'];	
 $pwd=$_REQUEST['user_password'];
 $obj=wp_authenticate($user_name,$pwd);

 if ( is_wp_error($obj) ){
	$emsg= "Username and/or Password is Incorrect";
	 }else{
		 
		 $userdata = get_userdata($obj->ID);
         $user = set_current_user($obj->ID,$user_name);
         // this will actually make the user authenticated as soon as the cookie is in the browser
         wp_set_auth_cookie($obj->ID);
         // the wp_login action is used by a lot of plugins, just decide if you need it
        do_action('wp_login',$userdata->ID); 
		
		 if($conferenceid){ 
		 
		  $link= get_permalink($options['register_page'] );
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .='conference='.$conferenceid;
					   
					  $link = custom_permalink($link );
				header("Location:".$link );
				exit;	      
				
			
			} else{
			
			 $link= get_permalink($options['profile_page'] );
					  
				header("Location:".$link );
			
			} 
/*	if(isset($_REQUEST['redirect_to'])){header("Location:".$_REQUEST['redirect_to']);}else{header("Location:".get_permalink( $options['profile_page'] ));}	*/ 
	
 }
   
}




// if user is loggedin redirect it to profile page
if ($user_ID != '') { 
	
//	header("Location:".get_permalink( $options['profile_page'] ));	
}
get_template_part( 'download', 'file' );
get_header(); 
 
 
 //////////////////////////////////////////////////////////////////////////////////////////////////
get_template_part( 'menu', 'tab' ); 
 
 
?>

	
	<div  class='content' >
    	<div  class='left' >
        
        
        	<?php dynamic_sidebar( 'LeftSideBar' ); ?>
         			 
        
        	 <?php 	get_template_part( 'left', 'side' );?>
            
    	</div>
        
        <div  class='right' >
        <div class="right-inner-gray">
        
      
        
        
          <div    >
               
                <form method="post" onsubmit="return validateForm()" action="<?php  echo  get_permalink($options['login_page'] ); ?>" >
                
                  
                 <div id='regblock' >
                    <div class="wrapper">
                    <?php 
				
					$conferenceid=get_query_var('conferenceid' ) ;
				if($conferenceid==''){
					 $conferenceid=$_SESSION['conference'];
					
					}
					
					if($conferenceid){
				   
				    	 $post_id=$conferenceid;
						 $post=get_post($post_id);
						 $conf_title=get_post_meta($post_id, 'Course_Topic',true);
  						
  						$locations=get_post_meta($post_id, 'Course_Location',true);
  						/*$regions=wp_get_post_terms($post_id,'region',array("fields" => "names"));*/
						$seminar=get_post_custom($post_id);
						if(is_array($seminar)){
									 foreach($seminar  as $k=>$v)
										{
											if(is_array($v)){
													  $seminar[$k]=$v[0];
												
											}else{  $seminar[$k]=$v;
												
											}
											
										}
									} 
									
							
						 extract($seminar);
			 /**/
			 
			 
	 
						 $venue= $Course_Venue ;		
						
			 			$post_thumbnail_id = get_post_thumbnail_id( $post_id ); 
					$seminar_image=wp_get_attachment_image( $post_thumbnail_id,'featured-location',$attr );
					$seminar_image_large=wp_get_attachment_image_src( $post_thumbnail_id,'large-size' );
			if(!$seminar_image){
			 $seminar_image="<p class='noimage'><img src='".site_url()."/wp-content/themes/mer/images/noimage.jpg' width='".$options['thumb_loc_w']."' height='". $options['thumb_loc_h']."' /></p>";
			 $seminar_image_large='wp-content/themes/mer/images/noimage.jpg';
			}
   //////////////////////////////////////////////////////////////////////////////////////////////////
 $current_date=get_post_meta($conferenceid,'Course_Start_Date',true);
 	
	
	
	 $sql = "SELECT pm1.meta_value FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						WHERE p.post_type = 'conference'
						AND p.post_status = 'publish'
						AND pm1.meta_key = 'Course_Start_Date'
						AND pm1.meta_value > '$dt'
						order by pm1.meta_value Desc limit 1,1
						
						"; 
	 		 
  		 $maxdate = $wpdb->get_var($sql );
	
	
	$nextlink='#';
  $sql = "SELECT pm1.meta_value,p.ID FROM $wpdb->posts p
JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
WHERE p.post_type = 'conference'
AND p.post_status = 'publish'
AND pm1.meta_key = 'Course_Start_Date'
AND pm1.meta_value > '$current_date'
order by pm1.meta_value ASC limit 1,1

"; 
	 		 
  			 $row = $wpdb->get_row($sql );
			 
			 
			 
			 
				
		  if($row->meta_value>date('Y-m-d')  && $row->meta_value<=$maxdate){
				 $link= get_permalink($page_id);
				 $link .= (strpos($link, '?')) ? "&" : "?";
				$link .='conference='.$row->ID;
				$nextlink = custom_permalink($link ); 
				 
			}		  
				
		 
  
					
				$prelink='#';	
				
		$preQuery = new WP_Query( $args );
		
		$sql = "SELECT pm1.meta_value,p.ID FROM $wpdb->posts p
JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)

WHERE p.post_type = 'conference'
AND p.post_status = 'publish'
AND pm1.meta_key = 'Course_Start_Date'
AND pm1.meta_value < '$current_date'
order by pm1.meta_value DESC limit 1,1

"; 
	 		 
  			 $row = $wpdb->get_row($sql );
		  
		 if($row->meta_value>date('Y-m-d') ){
				 $link= get_permalink($page_id);
					
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .='conference='.$row->ID;
					  $prelink = custom_permalink($link ); 
					 
					;
						
						
			}
		 
		 
		
 
$Course_Topic=$conf_title;	
			 				$Topics=$options['Topics'];
		 					$link= get_permalink( $Topics );
					         $link .= (strpos($link, '?')) ? "&" : "?";
							
						 $tbl=$wpdb->prefix.'posts';
	 		 $q = "SELECT  post_name FROM $tbl where post_title='$Course_Topic' and post_type='topic'";
  			 $Course_Topic = $wpdb->get_var($q );
							 $tp = $Course_Topic;
							  
					    	$link .= $tp;
					     
					  	$topiclink = custom_permalink($link );
						
							if($Course_Display_Date==''){
			/*$sdate=get_post_meta($post_id, 'Course_Start_Date',true);
			$edate=get_post_meta($post_id, 'Course_End_Date',true);
	*/
		   $Course_Display_Date= conference_date($Course_Start_Date,$Course_End_Date);
		
		}
		
						?>
                <div  class='conference' >
                      <div class="arrow" >
                      
                       <a <?php if($prelink=='#' ) echo " id='input-disable' "; ?> href='<?php echo $prelink; ?>' > &lt; </a>
                      	 
                       </div>
                      <div class="info">
                      	<div class="conference-title"><?php echo $conf_title; ?></div>
                        <div class="location"><?php echo preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Course_Location ) ?>, <?php echo $Course_Display_Date; ?></div>
                      </div>
                      <div class="arrow" ><a  <?php if($nextlink=='#' ) echo " id='input-disable' "; ?>  href='<?php echo $nextlink; ?>' >&gt;</a>
                      </div>
                      <div class="clear"></div>
                  </div>
 				<div id='error' ></div>
                  <div class='conference-detail'>                    
                   <div class="course_thumb">
				 <a href='<?php echo $seminar_image_large; ?>'  class='mer-lightbox float-left' ><div></div><?php echo $seminar_image ;?></a>
                 
				</div>
                               
                      <div class='conference-info course-right_single'>
                       <div class="conference-address"><?php echo preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $locations) ?> </div>
                       <div class="conference-address"><?php echo $venue; ?></div>
                       <div class="conference-date"> <?php echo $Course_Start_Date; ?></div>
                       <div class="conference-title-green" ><a  class="conference-title-green"  href='<?php echo $topiclink;  ?>'> <?php echo $conf_title;  ?></a></div>
                       <div class="price">Physician Fee :$<?php if($Early_Fee_Date>=date('Y-m-d')){ echo $Pricing_Early_Physician;}else{ echo $Pricing_Physician;} ?> </div>
                       <div class="price">Others Fee :$<?php if($Early_Fee_Date>=date('Y-m-d')){ echo $Pricing_Early_Other;}else{ echo $Pricing_Other;}  ?></div>
                    
                    <?php 
					  $dt=date("Y-m-d");
					  $start_date =  date("Y-m-d", strtotime($Course_Start_Date));
					   
					   
					  $link= get_permalink($register_page);
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .='conference='.$post_id;
					   
					  $link = custom_permalink($link );
					// echo $start_date ."  ". $dt ;
					   ?>   
                    
                    
                        <div class="conference-video" >
                       <?php 
                                            if($start_date>$dt) {?>
                       &nbsp;&nbsp;<a class="seminar-register" href='<?php echo  $link ; ?>'></a></div>
                       <?php  } ?>
                       
                        </div>
                       <div class="clear" ></div> 
                    
                    
                        </div>
                  </div>
                  <?php } ?>
                
                     <?php  if($emsg!=''){?>
				    <div  id='error' style="display:block" ><h2>Error </h2><?php echo $emsg; ?> </div>
				   
				<?php   }
                  if($msg!=''){?>
				    <div  class='msg'  ><?php echo $msg; ?> </div>
				   
				<?php   }
				 if ( empty( $redirect_to ) ) {
					if ( isset( $_REQUEST['redirect_to'] ) ) 
						$redirect_to = esc_url( $_REQUEST['redirect_to'] );
					else
						$redirect_to =  current_page_url(); 
					}
		
			if ( force_ssl_admin() ) 
			$redirect_to = str_replace( 'http:', 'https:', $redirect_to );
	
				  if (isset($_REQUEST['forget']) ) {?>
				   <div class="registration_leftpadding">
				    <div class='heading'>Enter Your Email<img src="<?php echo site_url(); ?>/wp-content/themes/mer/images/star_03.jpg"></div>
                     	<div><input type="text" size="30"  name="log" id='email' value="" ></div>
                       <div class="block_150" >
                     	<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="<?php echo site_url(); ?>/wp-content/themes/mer/images/submit.jpg" ></div>
                         <input type="hidden" name="changeUser"   value="" />        
                    </div>
                    </div>
				   <?php  }
				  else if (isset($_REQUEST['resetp']) ) {?>
                  <div class="registration_leftpadding">
					  <div class='heading'>Enter Your Username<img src="<?php echo site_url(); ?>/wp-content/themes/mer/images/star_03.jpg"></div>
                     	<div><input type="text" size="30"  name="log" id='user_name' value="" ></div>
                       <div class="block_150" >
                     	<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="<?php echo site_url(); ?>/wp-content/themes/mer/images/submit.jpg" ></div>
                         <input type="hidden" name="changePassword"   value="" />        
                    </div>
                    </div>
				   
				   <?php  } 
				 else if ($user_ID == '') { ?>
                    <div class="registration_leftpadding"> 
                   <div id='register' class="user_register" >
                      <div class="required heading"> <img src="<?php echo site_url(); ?>/wp-content/themes/mer/images/star_03.jpg">Required fields</div>   
                    <div class="block_150" >
                     	<div class='heading' >Username<img src="<?php echo site_url(); ?>/wp-content/themes/mer/images/star_03.jpg"></div>
                     	<div><input type="text" name="log" id='user_name' value="<?php echo $user_name ;?>" ></div>
                                    
                    </div>
                    <div class="block_150" >
                     	<div class='heading'>Password <img src="<?php echo site_url(); ?>/wp-content/themes/mer/images/star_03.jpg"></div>
                     	<div><input type="password"  name="user_password" id='password' ></div>
                         <input type="hidden" name="redirect_to"   value="<?php echo $_REQUEST['redirect_to']; ?>" />
                         <?php if($conference!=''){ ?>
	
					 <input type="hidden" name="conferenceid"   value="<?php echo $conferenceid ; ?>" /> 
                       
					<?php } ?>            
                    </div>
                     <div class="block_150" >
                     	<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="<?php echo site_url(); ?>/wp-content/themes/mer/images/submit.jpg" ></div>
                                
                    </div>
                   </div>
                     </div>
                   <?php } 
				   
				  
				   
				   
				 ?>
                    
                 </div><!--wrapper-->                
                 
                
                  </div>
                  </form>
               </div><!--registration-->
               
         </div>
                  <script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/mer/js/validation.js"></script>
  <script type="text/javascript">
  
  
 
 
 function validateForm()
{
  
  var counter =0
  
  aresult=true;
  	document.getElementById('error').innerHTML="";
	document.getElementById('error').style.display='none';
	
		isemptyemail=0;
		
			if(document.getElementById('user_name')){
				if(!checkEmptyControl('user_name','error','* UserName Is Required')){
				counter+=1;
				isemptyemail=1;
				}
			}
			if(document.getElementById('email')){
				if(!checkEmptyControl('email','error','* Email Is Required')){
				counter+=1;
				isemptyemail=1;
				}
			}
			if(document.getElementById('email')){
				if(!ValidateEmail('email','error','* Email is not valid')  ){
				counter+=1;
				isemptyemail=false;
				}
			}
			/*if(document.getElementById('user_name')){
				if(!ValidateEmail('user_name','error','* UserName should be an email')  ){
				counter+=1;
				isemptyemail=false;
				}
			}*/
			
			if(document.getElementById('password'))
			if(!checkEmptyControl('password','error','* Password Is Required')){
			counter+=1;
			}
			
			 
		
		
		
  if (counter>0)
  {
	
	 
	 document.getElementById('error').scrollIntoView(true);
 	 aresult=false;
  }
  

   return aresult;

}
  
  </script>
          
           
    	</div>
        <div class="clear" ></div>    
    </div>


</div>

<?php get_footer(); 
}  			
            ?>
           
             