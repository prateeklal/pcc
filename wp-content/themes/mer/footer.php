<?php

 $options = get_option( 'mer_theme_options' );
?> <div class="clear" ></div>   
  <div  class='footer' >
    	 <div  class='footer-inner' >
         	
            <div  class='footer-left' >
            
                <div class="footer-menu" >
                   <?php if ( is_active_sidebar( 'footermenu' ) ) : ?>
        			 <?php dynamic_sidebar( 'footermenu' ); ?>
         			  <?php endif; ?>
                    <?php if (  $options['telephone']!='' ) { ?>
                     <div class='text'><strong>tel</strong>:<?php echo $options['telephone'] ; ?></div>
                      <?php }if (  $options['email']!='' ) { ?>
					<div class='text'><strong>email</strong>:<?php echo $options['email'] ; ?></div>
                        <?php }if (  $options['footer_info']!='' ) { ?>
                    <div class='text'><?php echo $options['footer_info'] ; ?></div>
                         <?php }if (  $options['copyright']!='' ) { ?>
                            <div class='text'><?php echo $options['copyright'] ; ?></div>
                             <?php }  ?>
                </div>
            
            </div>
            
  
     <div  id='plusone-div' >
     </div>
            <div  class='footer-right' >
             <ul> 

<li class="pinterest"  ><a target="_new" href='<?php echo  $options['pinterest'] ; ?>'>pinterest</a> 
</li>
                    <li class="google"  ><a  target="_new" href='<?php echo  $options['google'] ; ?>'>google</a> 
</li>
                    <li><a  target="_new"  href='<?php echo  $options['facebook'] ; ?>'>facebook</a></li>
                    <li><a  target="_new"  href='<?php echo   $options['twitter'] ; ?>'>twitter</a></li>
                     <li><a  target="_new"  href='<?php echo   $options['linkedin'] ; ?>'>linkedin</a></li>
                    <li class="youtube"  ><a target="_new" href='<?php echo  $options['youtube'] ; ?>'>youtube</a> 
</li>
                    </ul>
          

            </div>
            <div class="clear" ></div>
    	 </div>
    </div>

</div>
   <?php wp_footer();?>
</body>