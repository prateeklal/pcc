<?php


/**

 * Template Name:  User Purchase

 * Description: Purchase Page for user

 *

 * @package WordPress

 * @subpackage mer

 */

get_template_part( 'download', 'file' );
get_header(); 
$options = get_option( 'mer_theme_options' );

$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];
 $profile_page=$options['profile_page'];
 $login_page=get_permalink($options['Login_Page']);
if(!$user_ID  ){
 header("Location:$login_page");
}	
 //////////////////////////////////////////////////////////////////////////////////////////////////
get_template_part( 'menu', 'tab' ); 
 
 
?>

	
	<div  class='content' >
     
        
        <div  class='right' style="width:100%" >
        
		  
         <div class="right-inner-gray" style="width:100%;margin-left:0;" >
		 <h2>Transactions History</h2>
 <?php 
 //echo $user_ID;
	/*$post_type='transaction';
    $posts_per_page=$options['post_per_page'];
	$loop=0;
	 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'orderby'        => 'ID',
        'order'            => 'DESC',
        'post_type' => $post_type , // This is where you should put your Post Type 
		
        'post_status'        => 'publish',
        'posts_per_page'    => $posts_per_page,
        'paged'            => $paged,
		
		
    );*/
 $post_type='registration';
   $args = array(
    'post_type' => $post_type , // This is where you should put your Post Type 
     'meta_key' => '__kf_Attendee_ID',
	 'meta_value' => $user_ID,	  
	'post_status' => 'publish',
	'posts_per_page' => $posts_per_page,
	'orderby'=>'ID',
	'order'=> 'DESC', 
	'paged'            => $paged,
	'meta_query' => array(

							array(

									'key' => 'Registration_Status',
									'value' => 'Completed',
									'compare' => '='
									) 

   						)
   );

    
/*	$postslist=get_posts($args);*/
	$the_query = new WP_Query( $args );
	 $total=$the_query->max_num_pages;

	 
		 $pages = $the_query->max_num_pages;
		
		?><table class="gridtable" width="100%" ><tr>
        <th align="left" valign='top'>Registration Date</th>
        <th align="left" valign='top'>Fee Type</th>        
        <th align="left" valign='top'>Conference</th>
        <th align="left" valign='top'>Conference Location</th>
        <th align="left" valign='top'>Conference Date</th>
        <th align="left" valign='top'>Total Fee</th>
        <th align="left" valign='top'>Cancellation Credit Applied</th>
         <th align="left" valign='top'>Paid</th>
  <th valign='top'>CC Number/Check</th>
  
  </tr>
        <?php
	  while ( $the_query->have_posts()   ):
	$the_query->next_post();
	$registration_id= $the_query->post->ID;
	
	$__kp_Registration_ID=get_post_meta($registration_id,'__kp_Registration_ID',true);
	$Registration_Fee=get_post_meta($registration_id,'Registration_Fee',true);
	$Registration_Date=get_post_meta($registration_id,'Registration_Date',true);
	$Registration_Status=get_post_meta($registration_id,'Registration_Status',true);
	 $course=get_post_meta($registration_id,'__kf_CourseID',true);
	
	
	//$lc_prt1=" JOIN $wpdb->postmeta pm2 ON (p.ID = pm2.post_id) ";
  	 


     $sql = "SELECT p.ID FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						$lc_prt1
						WHERE p.post_type = 'conference'
						AND p.post_status = 'publish'
						AND pm1.meta_key = '__kp_CourseID'
						AND pm1.meta_value = '$course'
						$lc_prt2 						
						";
					$fposts= $wpdb->get_row($sql );
					
	 $id=$fposts->ID;
	 $venue=get_post_meta($id,'Course_Venue',true);
	 	$sdate=get_post_meta($id, 'Course_Start_Date',true);
			$edate=get_post_meta($id, 'Course_End_Date',true);
	
		   $display_date= conference_date($sdate,$edate);
		$location=get_post_meta($id, 'Course_Location',true);
	
	
	 $link1= get_permalink($id);
	 $title=get_post_meta($id,'Course_Topic',true);
	 
		$post_type='transaction';
  //  $posts_per_page=$options['post_per_page'];
	/*$loop=0;
	 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;*/
    $args2 = array(
        'orderby'        => 'ID',
        'order'            => 'DESC',
        'post_type' => $post_type , // This is where you should put your Post Type 
		
        'post_status'        => 'publish',
        'posts_per_page'    => -1,
       
		'meta_query' => array(

							array(

									'key' => '__kf_Registration_ID',
									'value' => $__kp_Registration_ID,
									'compare' => '='
									)
									
								

   						)
		
    );
 $postslist=get_posts($args2);
  
	$CC_Num='';
	$transaction_amount='0';
		 $credit='0';
$feetype='';
 foreach ( $postslist as $p   ){
	
	 $transaction_post_id= $p->ID;
	 $category=get_post_meta($transaction_post_id,'Transaction_Category',true)   ;
	
	if($category=='Registration PAID CC'){
	 $CC_Num=get_post_meta($transaction_post_id,'CC_Num',true);
	$transaction_amount=get_post_meta($transaction_post_id,'Transaction_Amount',true);
	 $feetype=get_post_meta($transaction_post_id,'Registration_Fee_Type',true)   ;
	 }
	if($category=='Registration CREDIT USED'){
	$credit=get_post_meta($transaction_post_id,'Transaction_Amount',true);
	 }
	
	}
	$transaction_amount= $Registration_Fee-$credit;
	

	 if($CC_Num!=''){
	 $CC_Num="XXXX....$CC_Num";
	 }
	/* if($category=='Cancellation CREDIT'){
	 $credit=$transaction_amount-50; 
	 $transaction_amount=50;
	 
	 }*/
/*	 if($category=='Registration CREDIT USED'){
	 
	 
	$credit= $transaction_amount;
	$transaction_amount=$registration_fee-$transaction_amount;
	
	 }else{
		}
*/	  echo  "<tr> <td align='left'>".date("Y-m-d", strtotime($Registration_Date))."</td>
<td align='left' > $feetype</td>
	 
	  <td align='left'><a class='conference-title-green' href='$link1'>$title </a></td>
	  <td align='left'>".$venue."</td>
	   <td align='left'>".$display_date."</td>
	     <td align='right' >$".number_format($Registration_Fee, 2)."</td>
		  <td align='right'>$". number_format($credit, 2) ."</td>
	  <td align='right'>$".number_format($transaction_amount, 2)."</td>
	  <td>$CC_Num</td>
	  </tr>" ;
endwhile;
	?>
    
    <tr><td colspan="10" >
	<?php 
      post_pagination($total);
	
	 
	   ?>
		</td></tr>  
        <?php
	
   
	
	  if($pages==0 ){
		 echo  "<tr><td colspan='6' >No Transaction Found</td></tr>" ;

		}
		
		wp_reset_query();
wp_reset_postdata(); 
		
		
		
		
		
		
	   ?>
          </table>
          
          
        <h2>Cancellation </h2>
 <?php 
 //echo $user_ID;
	$post_type='transaction';
  //  $posts_per_page=$options['post_per_page'];
	$loop=0;
	 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'orderby'        => 'ID',
        'order'            => 'DESC',
        'post_type' => $post_type , // This is where you should put your Post Type 
		
        'post_status'        => 'publish',
        'posts_per_page'    => -1,
       
		'meta_query' => array(

							array(

									'key' => 'Transaction_Category',
									'value' => array('Cancellation CREDIT','Registration REFUND','Cancellation REFUND'),
									'compare' => 'in'
									),

									array(
									'key' => '__kf_Attendee_ID',
									'value' => $user_ID

									)

   						)
		
    );
 
    
/*	$postslist=get_posts($args);*/
	$the_query = new WP_Query( $args );
	// $total=$the_query->max_num_pages;

	 
		// $pages = $the_query->max_num_pages;
		
		?><table class="gridtable" width="100%" ><tr>
        <th align="left" valign='top'>Cancellation Date</th>
         
      
        <th align="left" valign='top'>Conference</th>
        <th align="left" valign='top'>Conference Location</th>
        <th align="left" valign='top'>Conference Date</th>
        <th align="left" valign='top'>Total Fee</th>
        
         <th align="left" valign='top'>Refund</th>
        <th align="left" valign='top'>Cancellation Credit Applied</th>
  <th valign='top'>CC Number/Check</th>
  
  </tr>
        <?php
	  while ( $the_query->have_posts()   ):
	$the_query->next_post();
	$id= $the_query->post->ID;
	$cancel_date= $the_query->post->post_date;
	$registration_id=get_post_meta($id,'__kf_Registration_ID',true);
	$transaction_amount=get_post_meta($id,'Transaction_Amount',true);
	 $feetype=get_post_meta($id,'Registration_Fee_Type',true)   ;
	  $category=get_post_meta($id,'Transaction_Category',true)   ;
	
	 $CC_Num=get_post_meta($id,'CC_Num',true);
	 $__kp_Transaction_ID=get_post_meta($id,'__kp_Transaction_ID',true);
	//GET REGISTRATION POST ID
	  $sql = "SELECT p.ID FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						$lc_prt1
						WHERE p.post_type = 'registration'
						AND p.post_status = 'publish'
						AND pm1.meta_key = '__kp_Registration_ID'
						AND pm1.meta_value = '$registration_id'
						$lc_prt2 						
						";
					$fregistrationpostidposts= $wpdb->get_row($sql );
	 $registrationpostid=$fregistrationpostidposts->ID;
	 
	  $course=get_post_meta($registrationpostid,'__kf_CourseID',true);
	$Registration_Date=get_post_meta($registrationpostid,'Registration_Date',true);
	 $registration_fee=get_post_meta($registrationpostid,'Registration_Fee',true);
	//GET REGISTRATION CONFERENCE POST ID
	  $sql = "SELECT p.ID FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						$lc_prt1
						WHERE p.post_type = 'conference'
						AND p.post_status = 'publish'
						AND pm1.meta_key = '__kp_CourseID'
						AND pm1.meta_value = '$course'
						$lc_prt2 						
						";
					$fposts= $wpdb->get_row($sql );
					
	 $postid=$fposts->ID;
	 $venue=get_post_meta($postid,'Course_Venue',true);
	 	$sdate=get_post_meta($postid, 'Course_Start_Date',true);
			$edate=get_post_meta($postid, 'Course_End_Date',true);
	
		   $display_date= conference_date($sdate,$edate);
		$location=get_post_meta($postid, 'Course_Location',true);
	
	
	 $link1= get_permalink($postid);
	 $title=get_post_meta($postid,'Course_Topic',true);
	 
	 if($CC_Num!=''){
	 $CC_Num="XXXX....$CC_Num";
	 }
	 $refund='0';
	 $credit=0;
	 if($category=='Cancellation CREDIT'){
	 $refund=$registration_fee-50; 
	 $credit=50;
	 
	 }else{
	 $refund=$registration_fee;
	  
	 }
	 
	  echo  "<tr>
	  <td align='left' >".date("Y-m-d", strtotime( $cancel_date))."</td>
	 
	  <td align='left'><a class='conference-title-green' href='$link1'>$title </a></td>
	  <td align='left'>".$venue."</td>
	   <td align='left'>".$display_date."</td>
	     <td align='right' >$".number_format($registration_fee, 2)."</td>
		  <td align='right'>$". number_format($refund, 2) ."</td>
		   <td align='right'>$". number_format($credit, 2)."</td>
	  <td>$CC_Num</td>
	  </tr>" ;
endwhile;
	?>
    
    <!--<tr><td colspan="10" >
	<?php 
   //   post_pagination($total);
	
	 
	   ?>
		</td></tr>  -->
        <?php
	
   
	
	  if($pages==0 ){
		 echo  "<tr><td colspan='10' >No Transaction Found</td></tr>" ;

		}
		
		wp_reset_query();
wp_reset_postdata(); 
		
		
		
		
		
		
	   ?>
          </table>   
         
        <br/>
        <br/>
    	</div>
        <div class="clear" ></div>    
    </div>


</div>
<div class="clear" ></div> 
<?php get_footer(); ?>
  