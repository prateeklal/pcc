<?php 
/**

 * Template Name: Side Bar-Gray Content

 * Description: Page With Side Bar With Gray Content

 *
 * @package WordPress

 * @subpackage mer

 */

get_template_part( 'download', 'file' );

get_header(); 
  $options=$_SESSION['mer_site_option'];;

$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];
 $profile_page=$options['profile_page'];


	get_template_part( 'menu', 'tab' ); 
?>
	<div  class='content' >
    	<div  class='left' >
        
        			 <?php dynamic_sidebar( 'LeftSideBar' ); ?>
        <?php 
		
		get_template_part( 'left', 'side' );
		?>
        	
            
    	</div>
        
        <div  class='right' >
        	<div class="right-inner-gray" >
         	<h2><?php the_title() ?></h2>
             <div  class='content' >
            	<h2><?php the_content(); ?></h2>
            </div>
            </div>
    	</div>
        <div class="clear" ></div>    
    </div>


</div>

<?php get_footer(); ?>
  