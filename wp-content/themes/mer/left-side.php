<?php
/* 
Converted this leftbar code into widgets
(
sidebar_banner_widget,
sidebar_conferences_by_month_widget,
sidebar_conferences_by_topic_widget
)
*/
if(0) {
    $currenr = '';
    $options = $_SESSION['mer_site_option'];
    $Conference_Schedule_Page = $options['Conference_Schedule_Page'];
    $Location_Page = $options['Location_Page'];
    if(get_query_var('month' ) != ''){
        $currenr = get_query_var('month' );
    }else{
        $currenr = date('m-Y');
    }
    $loc = 0;
    $tag = get_query_var('loc' );
    $dt = date('Y-m-d');
    $p_mon = '';
    $b = 0;
    $options = $_SESSION['mer_theme_options'];

    ?> 
    <div class="banner-left-side" >
        <?php if($options['home_banner_left_side']!=''){
            echo $options['home_banner_left_side'];
        }?>
    </div>

    <div class="left-scroll">
        <div class="left-block date-block">
        <?php
        $loc =array();
        $block_str='';
        $left_str='';
        $mindate=date('Y-m-d');
        $maxdate=date('Y-m-d');

        $sql = "SELECT pm1.meta_value FROM $wpdb->posts p
        JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
        WHERE p.post_type = 'conference'
        AND p.post_status = 'publish'
        AND pm1.meta_key = 'Course_Start_Date'
        AND pm1.meta_value >= '$dt'
        order by pm1.meta_value ASC limit 1,1";
        $mindate = $wpdb->get_var($sql );

        $sql = "SELECT pm1.meta_value FROM $wpdb->posts p
        JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
        WHERE p.post_type = 'conference'
        AND p.post_status = 'publish'
        AND pm1.meta_key = 'Course_Start_Date'
        AND pm1.meta_value > '$dt'
        order by pm1.meta_value Desc limit 1,1";

        $last_date= date('Y-m-d',strtotime( date("Y-m-01",strtotime($mindate)). " -1 day") );
        $maxdate = $wpdb->get_var($sql );
        $end=monthsBetween($mindate,$maxdate);

        for($l=0;$l<=$end+1;$l++){
            $date=date("Y-m-d",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $num_month=date("m-Y",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $num_monthY=date("Y",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $num_monthM=date("m",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $month=date("F Y",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $link='#';
            $link= get_permalink(  $Conference_Schedule_Page );
            $link .= (strpos($link, '?')) ? "&" : "?";
            $link .= "$num_monthY&$num_monthM";
            $link=  custom_permalink($link) ;
            echo  "<h2 class='title l_cat'  ><a    href='". $link ."'>" .$month ."</a></h2>";
            $firstdate=  date('Y-m-01',  strtotime($date));
            if($firstdate<date('Y-m-d')){
                $firstdate=date('Y-m-d');
            }
            $last_date= date('Y-m-t',strtotime($date));
            
            $sql = "SELECT p.ID FROM $wpdb->posts p
            JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
            WHERE p.post_type = 'conference'
            AND p.post_status = 'publish'
            AND pm1.meta_key = 'Course_Start_Date'
            AND pm1.meta_value >= '$firstdate' and pm1.meta_value <='$last_date'
            order by pm1.meta_value ASC";
            $lposts= $wpdb->get_results($sql);
            // $lposts=get_posts($args);

            $loc=array();
            $count=1;

            //var_dump($lposts);
            foreach($lposts as $lpost){
                $lc=get_post_meta($lpost->ID, 'Course_Location',true);
                if($lc!=''){
                    if (array_key_exists($lc, $loc)) {
                        $count=$loc[$lc]+1;
                    }
                    $loc[$lc]=$count;
                }
            }					 

            foreach($loc as $k=>$v){
                $cls=" class='conference-title' ";
                $tbl=$wpdb->prefix.'location';
                $q = "SELECT  slug FROM $tbl where location='$k'";
                $location = $wpdb->get_var($q );

                if( $tag==$location && $currenr==$num_month){
                    $cls=" class='conference-title active' ";
                }
                $tlink= get_permalink(  $Location_Page );
                $tlink .= (strpos($tlink, '?')) ? "&" : "?";
                $tlink .= "$location&$num_monthY&$num_monthM";
                $tlink=  custom_permalink($tlink); 
                $cnt='';
                if($v>1){
                  $cnt =" ($v)";
                }
                echo"<p><a  $cls   href='" .  $tlink  ."'>" .    preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $k)  . $cnt ."</a></p>";
            }
        }
    ?>
        </div>
    </div>

    <div class="left-block conference-topic">
        <h2 class="title">Conferences by Topic</h2>
        <?php
            $checkarray_topic_l=array();
            $tag= get_query_var('topics' );
            $checkarray_topic_l=array();
            $post_type='topic';
            $args=array(
                          'post_type' => $post_type,
                          'post_status' => 'publish',
                          'posts_per_page' => -1,
                          'orderby'          => 'title',
                          'order'            => 'ASC'
                        );

            $topics = get_posts($args);
            $post_id = get_the_ID();
            $dt = date('Y-m-d');
            $loc = array();				

            //echo count($topics)   ;

            foreach ($topics  as $topic)
            {

                $kf_Topic_ID =  get_post_meta($topic->ID, '__kp_Topic_ID',true);	
                $lnk =     $topic->post_title ;    
       
                  /* $lc_prt1=" JOIN $wpdb->postmeta pm2 ON (p.ID = pm2.post_id) ";
                    $lc_prt2=" AND pm2.meta_key = '__kf_Topic_ID' AND pm2.meta_value = '".$kf_Topic_ID."' ";
                   
                     $sql = "SELECT pm1.meta_value,p.ID FROM $wpdb->posts p
                                        JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
                                        $lc_prt1
                                        WHERE p.post_type = 'conference'
                                        AND p.post_status = 'publish'
                                        AND pm1.meta_key = 'Course_Start_Date'
                                        AND pm1.meta_value >= '$dt'
                                        $lc_prt2 	
                                        order by pm1.meta_value ASC limit 1,1";
                 
                $topicpost= $wpdb->get_row($sql );*/
                $meta_query = array();	
                $args2 = array(
                            'orderby'        => 'post_title',
                            'order'            => 'ASC',
                            'post_type' => 'conference', // This is where you should put your Post Type 
                            'post_status'        => 'publish',
                            'posts_per_page'    => 1,
                            'meta_query' => array(
                                                array(
                                                        'key' => 'Course_Start_Date',
                                                        'value' => $dt,
                                                        'compare' => '>=',
                                                        'type' => 'date'
                                                        ),
                                                        array(
                                                        'key' => '__kf_Topic_ID',
                                                        'value' => $kf_Topic_ID,
                                                        'compare' => '='
                                                        ) 
                                            )
                            
                        );		   
                $topicpost = get_posts($args2);	

                if(count($topicpost)>0)
                { 
                    $Topics=$options['Topics'];
                    $link= get_permalink( $Topics );
                    $link .= (strpos($link, '?')) ? "&" : "?";
                    $tp = $topic->post_name;
                    $link .= $tp;
                    $link = custom_permalink($link );
                    $active='';

                    if($tag==$topic->post_name){ $active= " active";}  
                    $date=get_post_meta($topicpost->ID, 'Course_Start_Date',true);  
                    $lnk= "<p><a class='conference-title". $active ."'  href='". $link ."'>". $topic->post_title ."</a></p>";
                    $checkarray_topic_l[]=array('link'=>$lnk,	'date'=>$date);
                }//if count
            }	

        // array_sort_by_column($checkarray_topic_l, 'date');
        foreach ($checkarray_topic_l  as $topic)
        {
            echo $topic['link'] ;
        }
        //var_dump($checkarray_topic_l);
        ?>
    </div>
<?php } ?>