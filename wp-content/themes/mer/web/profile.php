<?php

session_start();
global $current_user;

////////////////////////////// ATTACHMENT ///////////////////////////////////////////////
$options=$_SESSION['mer_site_option'];
get_template_part( 'download', 'file' );

 /////////////////////////// UPDATE USER //////////////////////////////////////////////////

  $login_page=get_permalink($options['Login_Page']);
 
if(!$user_ID  ){
 header("Location:$login_page");
}	
$msg='';
if(isset($_REQUEST['new_profile']))
{
	include_once(get_template_directory() . '/saveprofile.php');
}

if(isset($_REQUEST['update_profile']))
{
	require_once(get_template_directory() . '/updateprofile.php');
}


if(isset($_REQUEST['update_pass']))
{
	require_once(get_template_directory() . '/updatepassword.php');
}
if(isset($_POST['update_email']))
{
	require_once(get_template_directory() . '/updatepassword.php');
}


if(isset($_REQUEST['logout'])){
	wp_logout();
	session_destroy();
	$pg=$options['Login_Page'];
			$lg= get_permalink( $options['Login_Page']);
header("Location:".$lg);

}
/////////////////////////////////////////////////////////////////////////////
		
  
  
///////////////////////////// RETRIEV USER INFO ////////////////////////////////////////////////
		  
 $user_data=get_user_meta($user_ID);

 if(is_array($user_data)){
 foreach($user_data  as $k=>$v)
	{ 
	
	 
	$user_data[$k]=$v[0];
	 
		
	}
	
 if($current_user->user_login){
	 
	$user_data['user_name']= $current_user->user_login;
	 }
	 
	 // var_dump($user_data); 

unset($user_data['conferenceid']);
 extract($user_data);
 }
get_header(); 

//////////////////////////// GET PROFILE DATA ////////////////////////////////////////////////////

$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];
$profile_page=$options['profile_page'];
//////////////////////////////////////////////////////////////////////////////////////////////////
 get_template_part( 'menu', 'tab' ); 
$show_detail=$_REQUEST['r'];

?>

	
	<div  class='content' >
    	<div  class='left' >
       
        			 <?php dynamic_sidebar( 'leftsidebar' ); ?>
         	
        	 <?php 
		
		get_template_part( 'left', 'side' );
		?>
            
    	</div>
        
        <div  class='right' >
              <div  class='right-inner-gray' >
             
			  	
			  <?php
			  
			
			   if($show_detail=='update_password' || $show_detail=='updateemail') { 
                require_once(get_template_directory() .'/profile/password.php');
                 }elseif ($show_detail=='view_credits') { 
				 require_once(get_template_directory() .'/profile/user_credit.php');
				 }				 
				 elseif ($show_detail=='reg') { 
				require_once(get_template_directory() .'/profile/editprofile.php');
				  }elseif ($show_detail=='user_registration') { 
				  require_once(get_template_directory() .'/profile/user_registration.php');
				 
				  }elseif ($show_detail=='update_personal') { 
				require_once(get_template_directory() .'/profile/editprofile.php');
				 
				  }elseif ($show_detail=='p') { 
				 require_once(get_template_directory() .'/profile/user_purchase.php');
				 }
				 elseif ($user_ID != '') { 
				 
				 
				 $conferenceid=$_SESSION['conference'];
				 if($conferenceid){
				
				  $post_id=$conferenceid;
						 $post=get_post($post_id);
						 $conf_title=get_post_meta($post_id, 'Course_Topic',true);
  						
  						$locations=get_post_meta($post_id, 'Course_Location',true);
  						/*$regions=wp_get_post_terms($post_id,'region',array("fields" => "names"));*/
						$seminar=get_post_custom($post_id);
						if(is_array($seminar)){
									 foreach($seminar  as $k=>$v)
										{
											if(is_array($v)){
													  $seminar[$k]=$v[0];
												
											}else{  $seminar[$k]=$v;
												
											}
											
										}
									} 
									
							
						 extract($seminar);
			 /**/
			 
			 
	 
						 $venue= $Course_Venue ;		
						
			 			$post_thumbnail_id = get_post_thumbnail_id( $post_id ); 
					$seminar_image=wp_get_attachment_image( $post_thumbnail_id,'featured-location',$attr );
					$seminar_image_large=wp_get_attachment_image_src( $post_thumbnail_id,'large-size' );
			if(!$seminar_image){
			 $seminar_image="<p class='noimage'><img src='".site_url()."/wp-content/themes/mer/images/noimage.jpg' width='".$options['thumb_loc_w']."' height='". $options['thumb_loc_h']."' /></p>";
			 $seminar_image_large='wp-content/themes/mer/images/noimage.jpg';
			}
   //////////////////////////////////////////////////////////////////////////////////////////////////
 $current_date=get_post_meta($conferenceid,'Course_Start_Date',true);
 	
		 $sql = "SELECT pm1.meta_value FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						WHERE p.post_type = 'conference'
						AND p.post_status = 'publish'
						AND pm1.meta_key = 'Course_Start_Date'
						AND pm1.meta_value > '$dt'
						order by pm1.meta_value Desc limit 1,1
						
						"; 
	 		 
  		 $maxdate = $wpdb->get_var($sql );
	
	$nextlink='#';
  $sql = "SELECT pm1.meta_value,p.ID FROM $wpdb->posts p
JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
WHERE p.post_type = 'conference'
AND p.post_status = 'publish'
AND pm1.meta_key = 'Course_Start_Date'
AND pm1.meta_value > '$current_date'
order by pm1.meta_value ASC limit 1,1

"; 
	 		 
  			 $row = $wpdb->get_row($sql );
				$nextlink='#';	
		  if($row->meta_value>date('Y-m-d') && $row->meta_value<=$maxdate ){
				 $link= get_permalink($page_id);
				 $link .= (strpos($link, '?')) ? "&" : "?";
				$link .='conference='.$row->ID;
				$nextlink = custom_permalink($link ); 
				 
			}		  
				
		 
  
					
				$prelink='#';	
				
		$preQuery = new WP_Query( $args );
		
		$sql = "SELECT pm1.meta_value,p.ID FROM $wpdb->posts p
JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)

WHERE p.post_type = 'conference'
AND p.post_status = 'publish'
AND pm1.meta_key = 'Course_Start_Date'
AND pm1.meta_value < '$current_date'
order by pm1.meta_value DESC limit 1,1

"; 
	 		 
  			 $row = $wpdb->get_row($sql );
		  
		 if($row->meta_value>date('Y-m-d') ){
				 $link= get_permalink($page_id);
					
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .='conference='.$row->ID;
					  $prelink = custom_permalink($link ); 
					
						
			}
		 
		 
		
 
$Course_Topic=$conf_title;	
			 				$Topics=$options['Topics'];
		 					$link= get_permalink( $Topics );
					         $link .= (strpos($link, '?')) ? "&" : "?";
							
						 $tbl=$wpdb->prefix.'posts';
	 		 $q = "SELECT  post_name FROM $tbl where post_title='$Course_Topic' and post_type='topic'";
  			 $Course_Topic = $wpdb->get_var($q );
							 $tp = $Course_Topic;
							  
					    	$link .= $tp;
					     
					  	$topiclink = custom_permalink($link );
						
						 //$display_date= get_post_meta($lpost->ID, 'Course_Display_Date',true);
		 
		//if($Course_Display_Date==''){
			/*$sdate=get_post_meta($post_id, 'Course_Start_Date',true);
			$edate=get_post_meta($post_id, 'Course_End_Date',true);
	*/
		   $Course_Display_Date= conference_date($Course_Start_Date,$Course_End_Date);
		
		//}
		
							
						?>
                <div  class='conference' >
                      <div class="arrow" >
                      
                       <a <?php if($prelink=='#' ) echo " id='input-disable' "; ?> href='<?php echo $prelink; ?>' > &lt; </a>
                      	 
                       </div>
                      <div class="info">
                      	<div class="conference-title"><?php echo $conf_title; ?></div>
                        <div class="location"><?php echo preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Course_Location ) ?>, <?php echo $Course_Display_Date; ?></div>
                      </div>
                      <div class="arrow" ><a  <?php if($nextlink=='#'){ echo ' id="input-disable" ' ; } ?> href='<?php echo $nextlink; ?>' >&gt;</a>
                      </div>
                      <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                   <h2 class="page_heading">My Account</h2>
                   
                  
                  <div class='conference-detail'>                    
                   <div class="course_thumb">
				 <a href='<?php echo $seminar_image_large; ?>'  class='mer-lightbox float-left' ><div></div><?php echo $seminar_image ;?></a>
                 
				</div>
                               
                      <div class='conference-info course-right_single'>
                       <div class="conference-address"><?php echo preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $locations) ?> </div>
                       <div class="conference-address"><?php echo $venue; ?></div>
                       <div class="conference-date"> <?php echo $Course_Display_Date; ?></div>
                       <div class="conference-title-green" ><a  class="conference-title-green"  href='<?php echo $topiclink;  ?>'> <?php echo $conf_title;  ?></a></div>
                       <div class="price">Physician Fee :$<?php $price=0; if($Early_Fee_Date>=date('Y-m-d')){ echo $price=$Pricing_Early_Physician;}else{ echo $price=$Pricing_Physician;} ?> </div>
                       <div class="price">Others Fee :$<?php if($Early_Fee_Date>=date('Y-m-d')){ echo $price=$Pricing_Early_Other;}else{ echo $price=$Pricing_Other;}  
					   
					   $dt=date("Y-m-d");
					    $start_date =  date("Y-m-d", strtotime($Course_Start_Date));
						$register_page=$options['register_page'];
					   $link= get_permalink($register_page);
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .='conference='.$post_id;
					   
					  $link = custom_permalink($link );
					   
					    if($start_date>$dt ) {
                     
								if($price<1){ echo "<div id='error2'  >This conference is not available yet</div>";}else{?>
							   &nbsp;&nbsp;<a class="seminar-register" href='<?php echo  $link ; ?>'></a>
							   <?php 
							   }
					   }
					  
					   ?></div>
                    
                        </div>
                  </div>
                  <?php } ?>
				 
				<div class='profile_wrap'  >
               
                 <?php 

 global $post;
				$page_id =$post->ID;
				 if(get_query_var('msg' )==1){
				 $msg="Profile has been updated";
				 }
				 if ($msg!=''){ echo "<div class='clear'></div><div class='msg' >$msg</div>";} ?>
               <div class='profile_part'  ><form method="post" action="<?php echo  get_permalink( $profile_page); ?>"><input type="hidden" value="update_password" name="r" /><a  href='#'><input type='submit' value="Change or Update Email/Password"  /></a></form></div> 
                  <div  class='profile_part'   ><form method="post" action="<?php echo  get_permalink( $profile_page); ?>"><input type="hidden" value="update_personal" name="r"  /><a  href='#'><input type='submit' value="Change Contact and Personal Information"  /></a></form></div> 
               <div  class='profile_part'   ><form method="post" action="<?php echo  get_permalink( $profile_page); ?>"><input type="hidden" value="view_credits" name="r"  /><a  href='#'><input type='submit' value="Available Credit Balance"  /></a></form></div> 
                <div  class='profile_part'   ><a  href='<?php echo get_permalink( $options['user_purchase']);  ?>'><input type='submit' value="Transaction History""  /></a></div>   
                 <div  class='profile_part'   ><form  method="post" action="<?php echo get_permalink($options['user_reg_page']);  ?>"><a  href='#'><input type='submit' value="Conference History"  /></a></form></div> 
               
              
               <div  class='profile_part'   ><a  href='<?php echo home_url();  ?>'><input type='submit' value="Explore Conferences"  /></a></div> 
              		 
				<?php	 }
				else{
					 require_once(get_template_directory() .'/profile/personal.php');
				} ?>
                <!--registration-->
                  
             </div><!--right-inner-gray-->
            
    	</div>
        <div class="clear" ></div>    
    </div>


</div>

  <div class="clear" ></div>  
<?php get_footer(); ?>
  