<?php /**

 * Template Name:  Conference Schedule

 * Description: Conference Schedule Page

 *

 * @package WordPress

 * @subpackage mer

 */


get_template_part( 'download', 'file' );

get_header(); 
  $options=$_SESSION['mer_site_option'];

$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];
 $profile_page=$options['profile_page'];

	get_template_part( 'menu', 'tab' ); 
?>
	<div  class='content' >
    	<div  class='left' >
        
        
        			 <?php dynamic_sidebar( 'leftsidebar' ); ?>
         			 
        <?php 
		
		get_template_part( 'left', 'side' );
		?>
        	
            
    	</div>
        
        <div  class='right' >
        
        <div class="right-inner-gray" >
          <div  class='conference' >
          <?php 
		  $start_date=date('Y-m-d');
		/*  if(isset($_POST['mer_schd'])){
			   $start_date=$_POST['mer_schd'];
			  
			  }*/
			  
			global $wpdb;  
			 $sql = "SELECT pm1.meta_value FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						WHERE p.post_type = 'conference'
						AND p.post_status = 'publish'
						AND pm1.meta_key = 'Course_Start_Date'
						AND pm1.meta_value > '$dt'
						order by pm1.meta_value Desc limit 1,1
						
						"; 
	 		 
  		 $maxdate = $wpdb->get_var($sql ); 
		  if(get_query_var('month' )!=''){
			 $start_date="01-".get_query_var('month' );
			  $d=explode('-',$start_date);
		 $start_date=$d[2]."-".$d[1]."-".$d[0];
			}
		   $d=explode('-',$start_date);
		
		
		 $date = strtotime(date("Y-m-d", strtotime($start_date)) . " -1 month");
		 $prevmonthtitle=date("M",$date);
		 $prevmonth=date("Y-m-d",$date);
		
		$date = strtotime(date("Y-m-d", strtotime($start_date)) . " +1 month");
		 $nextmonthtitle=date("M",$date);
		
		$nextmonth=date("Y-m-d",$date);
		
		$title=date("F Y",mktime(0,0,0,$d[1],1,$d[0]));
		/* $dt=date('Y-m-d');
 */
		 $link= get_permalink( $Conference_Schedule_Page );
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .= date("Y",strtotime($prevmonth)). "&" .date("m",strtotime($prevmonth));
					   
					  $plink = custom_permalink($link );
					  
					  $link= get_permalink( $Conference_Schedule_Page );
					  $link .= (strpos($link, '?')) ? "&" : "?";
		 			$link .= date("Y",strtotime($nextmonth)) . "&" .date("m",strtotime($nextmonth));
					   
					  $nlink = custom_permalink($link );
		
		  
		  ?>
         
                      <div class="arrow" ><a class="move"   <?php if($prevmonth<date("Y-m-d")){ echo 'href="#" id="input-disable" disabled="disabled" ';   }else{  echo "href='$plink'"; } ?> >&lt;<?php echo $prevmonthtitle; ?> </a></div>
                      <div class="info">
                      	<div class="conference-title"><?php echo $title ; ?> Conferences</div>
                        
                      </div>
                      <div class="arrow" ><a  class="move"    <?php if($nextmonth>$maxdate){ echo 'href="#" id="input-disable" disabled="disabled" ';   }else{  echo "href='$nlink'";  } ?>     ><?php echo $nextmonthtitle; ?>&gt;  </a></div>
                      <div class="clear"></div>
             </div>
        
        	<?php get_template_part( 'course', 'listSchedule' );
           ?>
        </div>
        	<div class="right-inner" >
         	<?php 
			
           get_template_part( 'featured', 'list' );?>
    	</div>
    </div>
           
   </div>
        <div class="clear" ></div>    
    </div>


</div>

<?php get_footer(); ?>
  