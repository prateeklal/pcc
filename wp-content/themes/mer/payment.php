<?php 
/**

 * Template Name:  payment

 * Description: Payment Page for user

 *

 * @package WordPress

 * @subpackage mer

 */


$detect = new Mobile_Detect();
if ($detect->isMobile()) {
require_once(get_template_directory() .'/mobile/payment_mobile.php');

}else{


   require_once(get_template_directory() .'/web/payment.php');
}

