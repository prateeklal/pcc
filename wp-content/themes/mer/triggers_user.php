<?php

$hostname_db = "localhost";
$database_db = "merhost_mer";
$username_db = "merhost_mer";
$password_db = "hxbUqb]koc?m";
$link = mysqli_connect($hostname_db, $username_db, $password_db,$database_db) or trigger_error(mysql_error(),E_USER_ERROR);
if(mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$sql="
CREATE TRIGGER trigger_attendee_user After INSERT ON `wp_attendees`
FOR EACH ROW  BEGIN

  Declare `p`,`CheckExists`,`aid` int;
 Declare `pass` varchar(100);
 Declare `uname` varchar(100);
 IF(New.`no_wp_user`=1) THEN 
   SELECT `ID` INTO `CheckExists` FROM wp_users  WHERE  `ID`= New.`__kp_Attendee_ID`  ; 
   
  IF(New.`Degree`<>'') THEN 
  	REPLACE INTO `wp_degree` SET  degree=New.`Degree`  ;
  END IF;
  
  IF(New.`Title`<>'') THEN 
  	REPLACE INTO `wp_titles` SET  title=New.`Title`  ;	
   END IF;
	
	IF(New.`Specialty`<>'') THEN 
 		REPLACE INTO `wp_speciality` SET  speciality=New.`Specialty`  ;
 	END IF;
 
 SET `pass`=New.`password`;
 SET `uname`=New.`username`;
 
 
  IF (`CheckExists` > 1) THEN 
  
  		SET `p`=`CheckExists` ;
		
		IF(`uname`<>'') THEN 
		update `wp_users` set `user_login`=`uname`, `user_nicename`=`uname`   where `ID`=`p` ;		
		END IF;
		
		IF(`pass`<>'') THEN 
		update `wp_users` set `user_pass`=`pass`   where `ID`=`p` ;		
		END IF;
		
		IF(New.`Email_1`<>'') THEN 
		update `wp_users` set `user_email`=New.`Email_1` where `ID` =`p` ;
		END IF;
		
		 
		IF(New.`Full_Name`<>'') THEN 
		update `wp_users` set `display_name`=New.`Full_Name`   where `ID`   =`p`;
		END IF;
			
			IF (New.`role` ='administrator') THEN		
				update `wp_usermeta` set `wp_capabilities`='a:1:{s:13:\"administrator\";b:1;}'   where `ID`   =`p`;
			ELSE	
				update `wp_usermeta` set `wp_capabilities`='a:1:{s:10:\"subscriber\";s:1:\"1\";}'   where `ID`   =`p`;
			END IF;
	
  		update `wp_usermeta` set `meta_value`=New.`credits_available`  where `meta_key`='credits_available' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`__kp_Attendee_ID`  where `meta_key`='__kp_Attendee_ID' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`first_name`  where `meta_key`='first_name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`last_name`  where `meta_key`='last_name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Full_Name`  where `meta_key`='Full_Name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`City`  where `meta_key`='City' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`State_Province`  where `meta_key`='State_Province' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Country`  where `meta_key`='Country' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Zip`  where `meta_key`='Zip' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Phone_Office`  where `meta_key`='Phone_Office' and `user_id`=`p`;
        update `wp_usermeta` set `meta_value`=New.`Phone_Home`  where `meta_key`='Phone_Home' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Name`  where `meta_key`='Billing_Name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`MI`  where `meta_key`='MI' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Address`  where `meta_key`='Billing_Address' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Billing_Address_2`  where `meta_key`='Billing_Address_2' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`billing_city`  where `meta_key`='billing_city' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_State_Province`  where `meta_key`='Billing_State_Province' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Zip`  where `meta_key`='Billing_Zip' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Billing_Country`  where `meta_key`='Billing_Country' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Address`  where `meta_key`='Address' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Address_2`  where `meta_key`='Address_2' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Same`  where `meta_key`='Billing_Same' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Degree`  where `meta_key`='Degree' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Specialty`  where `meta_key`='Specialty' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Title`  where `meta_key`='Title' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Pharm_BirthDate`  where `meta_key`='Pharm_BirthDate' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Pharm_NAPB`  where `meta_key`='Pharm_NAPB' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Phone_Contact`  where `meta_key`='Phone_Contact' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Phone_Mobile`  where `meta_key`='Phone_Mobile' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Phone_Primary`  where `meta_key`='Phone_Primary' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Email_2`  where `meta_key`='Email_2' and `user_id`=`p`;
		IF(New.`Email_1`<>'') THEN 
		update `wp_usermeta` set `meta_value`=New.`Email_1`  where `meta_key`='Email_1' and `user_id`=`p`;
		END IF;
	ELSE
	
	IF (`pass` = '') THEN
  		SET `pass`=MD5('password');
 	END IF;
 	IF (`uname` = '') THEN
 		 SET `uname`=CONCAT(New.`First_Name`,New.`__kp_Attendee_ID`);  
  
	 END IF;
	
	
	delete from `wp_users` where `ID`=New.`__kp_Attendee_ID`;
	 
	delete from `wp_usermeta` where `user_id`=New.`__kp_Attendee_ID`;
	INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES (New.`__kp_Attendee_ID`, `uname`,   `pass` ,`uname`, New.`Email_1`, '', NOW(), '', '0', New.`Full_Name`);
	
	
	
	IF (New.`role` ='administrator') THEN
		INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}');
	
	ELSE
	
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";s:1:\"1\";}');
	
	END IF;
	
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'wp_user_level', '0');
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'credits_available',New.`credits_available`  );
	
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'first_name', New.`first_name`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'last_name', New.`last_name`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Full_Name', New.`Full_Name`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'City', New.`City`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'State_Province', New.`State_Province`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Country', New.`Country`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Zip', New.`Zip`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Phone_Office', New.`Phone_Office`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Phone_Home', New.`Phone_Home`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_Name', New.`Billing_Name`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'MI', New.`MI`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_Address', New.`Billing_Address`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_Address_2', New.`Billing_Address_2`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_City', New.`Billing_City`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_State_Province', New.`Billing_State_Province`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_Zip', New.`Billing_Zip`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_Country', New.`Billing_Country`);

	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Address', New.`Address`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Address_2', New.`Address_2`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Billing_Same', New.`Billing_Same`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Degree', New.`Degree`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Specialty', New.`Specialty`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Title', New.`Title`);
	
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Pharm_BirthDate', New.`Pharm_BirthDate`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Pharm_NAPB', New.`Pharm_NAPB`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Phone_Contact', New.`Phone_Contact`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Phone_Mobile', New.`Phone_Mobile`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Phone_Primary', New.`Phone_Primary`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Email_2', New.`Email_2`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'Email_1', New.`Email_1`);
	INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES (NULL, New.`__kp_Attendee_ID`, 'mer_key', '__kp_Attendee_ID'); 
		
	
	END IF;
 END IF;
END  
";		
	$trs = mysqli_multi_query($link,$sql) ;

if( mysqli_errno($link) > 0)
    echo "query err : ".mysqli_error($link);
	else
	echo "Done  trigger_attendee_user<br/>";
	
	
/*{*/

		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	  3)
/*{*/
$sql="
CREATE TRIGGER trigger_attendee_user_upd After update ON `wp_attendees`
FOR EACH ROW  BEGIN

  Declare `p`,`CheckExists` int;
 Declare `pass` varchar(100);
 Declare `uname` varchar(100);
IF(New.`no_wp_user`=1) THEN  
  SELECT `ID` INTO `CheckExists` FROM wp_users  WHERE  `ID`= New.`__kp_Attendee_ID`  ; 
  
  REPLACE INTO `wp_degree` SET  degree=New.`Degree`  ;
  REPLACE INTO `wp_titles` SET  title=New.`Title`  ;
 REPLACE INTO `wp_speciality` SET  speciality=New.`Specialty`  ;
 
 SET `pass`=New.`password`;
 SET `uname`=New.`username`;
  
  
  IF (`CheckExists` > 1) THEN 
  	SET `p`=`CheckExists` ;
  		
		IF(`uname`<>'') THEN 
		update `wp_users` set `user_login`=`uname`, `user_nicename`=`uname`   where `ID`=`p` ;		
		END IF;
		
		IF(`pass`<>'') THEN 
		update `wp_users` set `user_pass`=`pass`   where `ID`=`p` ;		
		END IF;
		
		IF(New.`Email_1`<>'') THEN 
		update `wp_users` set `user_email`=New.`Email_1` where `ID`  =`p` ;
		
		END IF;
		 
		
		IF(New.`Full_Name`<>'') THEN 
		update `wp_users` set `display_name`=New.`Full_Name`   where `ID`   =`p`;
		END IF;
		
		IF (New.`role` ='administrator') THEN		
				update `wp_usermeta` set `wp_capabilities`='a:1:{s:13:\"administrator\";b:1;}'   where `ID`   =`p`;
			ELSE	
				update `wp_usermeta` set `wp_capabilities`='a:1:{s:10:\"subscriber\";s:1:\"1\";}'   where `ID`   =`p`;
			END IF;
	
		
		update `wp_usermeta` set `meta_value`=New.`credits_available`  where `meta_key`='credits_available' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`__kp_Attendee_ID`  where `meta_key`='__kp_Attendee_ID' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`first_name`  where `meta_key`='first_name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`last_name`  where `meta_key`='last_name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Full_Name`  where `meta_key`='Full_Name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`City`  where `meta_key`='City' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`State_Province`  where `meta_key`='State_Province' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Country`  where `meta_key`='Country' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Zip`  where `meta_key`='Zip' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Phone_Office`  where `meta_key`='Phone_Office' and `user_id`=`p`;
        update `wp_usermeta` set `meta_value`=New.`Phone_Home`  where `meta_key`='Phone_Home' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Name`  where `meta_key`='Billing_Name' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`MI`  where `meta_key`='MI' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Address`  where `meta_key`='Billing_Address' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Billing_Address_2`  where `meta_key`='Billing_Address_2' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`billing_city`  where `meta_key`='billing_city' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_State_Province`  where `meta_key`='Billing_State_Province' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Zip`  where `meta_key`='Billing_Zip' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Billing_Country`  where `meta_key`='Billing_Country' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Address`  where `meta_key`='Address' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Address_2`  where `meta_key`='Address_2' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Billing_Same`  where `meta_key`='Billing_Same' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Degree`  where `meta_key`='Degree' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Specialty`  where `meta_key`='Specialty' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Title`  where `meta_key`='Title' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Pharm_BirthDate`  where `meta_key`='Pharm_BirthDate' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Pharm_NAPB`  where `meta_key`='Pharm_NAPB' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Phone_Contact`  where `meta_key`='Phone_Contact' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Phone_Mobile`  where `meta_key`='Phone_Mobile' and `user_id`=`p`;
	    update `wp_usermeta` set `meta_value`=New.`Phone_Primary`  where `meta_key`='Phone_Primary' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Email_2`  where `meta_key`='Email_2' and `user_id`=`p`;
		update `wp_usermeta` set `meta_value`=New.`Email_1`  where `meta_key`='Email_1' and `user_id`=`p`;
	
	
	END IF;
	
 END IF;
END  ;
";	
	
	$trs = mysqli_multi_query($link,$sql) ;

if( mysqli_errno($link) > 0)
    echo "query err : ".mysqli_error($link);
	else
	echo "Done  trigger_attendee_user_upd<br/>";
/*}*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	 4)
/*{*/

/*$sql="DROP  TRIGGER trigger_registration_post_upd";
$sql="DROP  TRIGGER trigger_registration_post";
$sql="DROP  TRIGGER trigger_transaction_post";
$sql="DROP  TRIGGER trigger_transaction_post_upd";
$sql="DROP  TRIGGER trigger_topics_post";
$sql="DROP  TRIGGER trigger_topics_post_upd";
$sql="DROP  TRIGGER trigger_agenda_post";
$sql="DROP  TRIGGER trigger_agenda_post_upd";*/

?>