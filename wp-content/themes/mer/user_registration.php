<?php

/**

 * Template Name:  User Registration

 * Description: User's Past Registration Page 

 *

 * @package WordPress

 * @subpackage mer

 */


get_template_part( 'download', 'file' );
get_header(); 
$options = get_option( 'mer_theme_options' );
//var_dump($options);

$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];
 $profile_page=$options['profile_page'];
 $login_page=get_permalink($options['Login_Page']);
 $register_page=$options['register_page'];
$payment_page=$options['payment_page'];
if(!$user_ID  ){
 header("Location:$login_page");
}			 
 
 //////////////////////////////////////////////////////////////////////////////////////////////////
get_template_part( 'menu', 'tab' ); 
 
 
?>

	
	<div  class='content' >
     
        
        <div  class='right' style="width:100%" >
        
		  
         <div class="right-inner-gray" style="width:100%;margin-left:0;" >
         
         <?php
//echo $user_ID;
$options = get_option( 'mer_theme_options' );?>
<h2>Conference History</h2>
          
      <?php 
 $posts_per_page=$options['post_per_page'];
 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;	
	
	$post_type='registration';
   $args = array(
    'post_type' => $post_type , // This is where you should put your Post Type 
     'meta_key' => '__kf_Attendee_ID',
	 'meta_value' => $user_ID,	  
	'post_status' => 'publish',
	'posts_per_page' => $posts_per_page,
	'orderby'=>'ID',
	'order'=> 'DESC', 
	'paged'            => $paged,
   );
   
   
     
	$loop=0;
	
   $total=0;
     
     $the_query = new WP_Query( $args );
   $total=$the_query->max_num_pages;
		$pages = $the_query->max_num_pages;
		
		?>
<table class="gridtable" width="100%" ><tr>
<th align='left' valign='top' >Registration ID</th
><th align='left' valign='top'>Date Registered</th>
<th align='left' valign='top'>Conference</th>
<th align='left' valign='top'>Conference Location</th>
<th align='left' valign='top'>Conference Date </th>
<th align='left' valign='top'>Conference Hotel</th>
<th align='left'  valign='top'>Registration Status</th></tr>

        <?php
		  
		
	  while ( $the_query->have_posts()   ):
	$the_query->next_post();
	$registration_id= $the_query->post->ID;
	
	$__kp_Registration_ID=get_post_meta($registration_id,'__kp_Registration_ID',true);
	$Registration_Fee=get_post_meta($registration_id,'Registration_Fee',true);
	$Registration_Date=get_post_meta($registration_id,'Registration_Date',true);
	$Registration_Status=get_post_meta($registration_id,'Registration_Status',true);
	 $course=get_post_meta($registration_id,'__kf_CourseID',true);
	
	
	//$lc_prt1=" JOIN $wpdb->postmeta pm2 ON (p.ID = pm2.post_id) ";
  	 


     $sql = "SELECT p.ID FROM $wpdb->posts p
						JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
						$lc_prt1
						WHERE p.post_type = 'conference'
						AND p.post_status = 'publish'
						AND pm1.meta_key = '__kp_CourseID'
						AND pm1.meta_value = '$course'
						$lc_prt2 						
						";
					$fposts= $wpdb->get_row($sql );
					
	 $id=$fposts->ID;
	 $venue=get_post_meta($id,'Course_Venue',true);
	 	$sdate=get_post_meta($id, 'Course_Start_Date',true);
			$edate=get_post_meta($id, 'Course_End_Date',true);
	
		   $display_date= conference_date($sdate,$edate);
		$location=get_post_meta($id, 'Course_Location',true);
	
	
	 $link1= get_permalink($id);
	 $title=get_post_meta($id,'Course_Topic',true);
	if($Registration_Status=='Pending'){
	
	$link= get_permalink($payment_page);
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .='conference='.$id ."&pending=".$__kp_Registration_ID;
					   
					  $link = custom_permalink($link );
	$Registration_Status="<a class='pending' title='Registration is not complete .Please click on the link to enter your credit card information.' href='$link'>$Registration_Status</a>";
	}
	//$registration_id=get_post_meta($id,'registration_id',true);
	 
	 //$locations=wp_get_post_terms($coursepost->ID,'location',array("fields" => "names"));
	 echo  "<tr><td align='left'>$__kp_Registration_ID</td>
	 <td align='left'>".date("Y-m-d", strtotime($Registration_Date))."</td>
	 <td align='left'><a class='conference-title-green' href='$link1'>$title </a></td>
	 <td align='left' >$location</td>	
	  <td align='left' >$display_date</td>
	 <td align='left' >$venue</td>
	 <td align='left' >$Registration_Status</td></tr>" ;

	 
endwhile;
	
	
	?>
    
    
    
    
        <?php

 
 // post_pagination($total);
	
	  
	   ?>
		<tr><td colspan="7" >
	<?php 
      post_pagination($total);
	
	  	wp_reset_query();
wp_reset_postdata(); 
	   ?>
		</td></tr>  
 </table>
           </div>
          
           
    	</div>
        <div class="clear" ></div>    
    </div>


</div>

<?php get_footer(); ?>
  


           
    	