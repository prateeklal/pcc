<?php


get_header('mobile'); 
$options=$_SESSION['mer_site_option'];

$register_page=$options['register_page'];
   $post_id=get_the_ID(); //seminar ID
  
 $course_id=post_id; // Course ID
  $seminar=get_post_custom($course_id);
 
	
if(is_array($seminar)){
			 foreach($seminar  as $k=>$v)
				{
					if(is_array($v)){
							  $seminar[$k]=$v[0];
						
					}else{  $seminar[$k]=$v;
						
					}
				}
				
			} 

			
			
 extract($seminar);
$venue=get_post_meta($post_id, 'Course_Venue',true);		

 ?>
 
	
	<div  class='content' >
                 <div class="gray-block">
    <div class="arrow" ><a  class="back" href='<?php echo site_url(); ?>' id="cat_menu"      >&nbsp;</a></div>
    <div class="info">
    	<div  class="conference-sched-title">Back to Conferences</div>
    	<div   class="conference-category">
            
           
        </div>
    </div>  
    </div>
                  <div class='featured-category-block'> 
                  
                  <?php 
				  
                  
		 $link= get_permalink($register_page);
					        $link .= (strpos($link, '?')) ? "&" : "?";
					    $link .='conference='.$post_id;
					  $link = custom_permalink($link );
		 
	$post_thumbnail_id = get_post_thumbnail_id( $post_id); 
	
	$seminar_image=wp_get_attachment_image( $post_thumbnail_id,'featured-location',$attr );	
	
	if(!$seminar_image){
			 $seminar_image="<p class='noimage'><img src='".site_url()."/wp-content/themes/mer/images/noimage.jpg' width='".$options['thumb_loc_w']."' height='". $options['thumb_loc_h']."' /></p>";
			 $seminar_image_large='wp-content/themes/mer/images/noimage.jpg';
			}			
	if($seminar_image!=''){
				echo "<div class='course_thumb'>";
				echo "<a class='float-left' href='".  get_permalink($post_id)."'><div></div>$seminar_image</a>";
				echo "</div>";
				}
				
				echo "<div class='course-right' >";
			//if($locations){
			 echo "<div class='s_address' >".preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Course_Location )."</div>";
			//}
			
			if($Course_Display_Date==''){
			
		   $Course_Display_Date= conference_date($Course_Start_Date,$Course_End_Date);
		
		}
		
			
			if($Course_Display_Date!='')
			{ 
				echo "<div class='s_date' >$Course_Display_Date</div>";
			} 
			 
           ?>	
			<div class='s_title' ><?php  echo $Course_Topic; ?></div>  
			
                 <?php 
           echo "</div>";  ?>             
                      
            <div class="clear" ></div>
            <div  class="course_thumb">
            <div class="price">Physician Fee:$<?php $price=0;if($Early_Fee_Date>=date('Y-m-d')){ echo $price=$Pricing_Early_Physician;}else{ echo $price=$Pricing_Physician;} ?>  </div>
                       <div class="price">Other Fee:$<?php if($Early_Fee_Date>=date('Y-m-d')){ echo $price=$Pricing_Early_Other;}else{ echo $price=$Pricing_Other;}  ?></div>
                       
		     </div>
             <div class="register-block">
           
                       <div class="conference-video" >
                    <?php
                    $dt=date('Y-m-d');
                     if($Course_Start_Date>$dt ) {
                     	if($price<1){ echo "<span id='error2'  >This conference is not available yet</span>";}else{?>
                       &nbsp;&nbsp;<a href='<?php echo $link; ?>' ><img src='<?php echo site_url() ;?>/wp-content/themes/mer/images/register_03.jpg'   /></a>
                       <?php 
					   }
					   
					    }
						?> 
            		</div>
             </div>
              <div class="clear" ></div>
            </div>
                  
                  
                 
                
                    <div class="active" id='accomdation_container' >
                   			
                         <?php 
						 					
						// GET VEnue Detail from Venue id
						 $phone =get_post_meta($venue, 'Course_Venue_Web_Site',true);
						 
					
					 $seminar_map_link =get_post_meta($venue, 'map_link',true);
				
                    $seminar_booking_information =get_post_meta($$venue, 'booking_information',true);
					$seminar_booking_information=str_replace('[venue]',$venuepost->post_title,$seminar_booking_information);
					
					 $seminar_booking_website =get_post_meta($venue, 'Course_Venue_Booking_Link',true);
					$seminar_booking_phone =get_post_meta($venue, 'Course_Venue_Reservation_Phone',true);
					$seminar_group_code =get_post_meta($venue, 'Course_Venue_Reservation_Group_Code',true);
					$seminar_booking_room_price  =get_post_meta($venue, 'Course_Lowest_Room_Rate',true) ;
					
					
					// retreive venue images 
					
				 ?>
                 <div>Please use one of the below available option to book with in our specially negotiated rate.</div>
                 <div class="accom-web">Web: <a href='<?php echo $Course_Venue_Web_Site; ?>'>Reservation Link</a></div>
                 <div>Phone: <?php echo $Course_Venue_Reservation_Phone; ?></div>
                 <div>Group Code : <?php echo $Course_Venue_Reservation_Group_Code; ?></div>
                 <div>Room Starting at $<?php echo $seminar_booking_room_price; ?> per night*</div>
                 <p><?php echo $seminar_booking_extra_note; ?></p>
                
                    
                    <div class="clear" ></div>
                    </div>
         </div>
          
           
    	</div>
           
  

<?php //get_footer(); ?>
        
 
 