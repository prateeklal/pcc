<?php

/**

 * Template Name:  User Registration

 * Description: User's Past Registration Page 

 *

 * @package WordPress

 * @subpackage mer

 */


get_template_part( 'download', 'file' );
get_header(); 
$options = get_option( 'mer_theme_options' );

$Conference_Schedule_Page=$options['Conference_Schedule_Page'];
$Topics=$options['Topics'];
$Accredetation=$options['Accredetation'];
$Half_Day_Format=$options['Half_Day_Format'];
 $profile_page=$options['profile_page'];
 
 
 //////////////////////////////////////////////////////////////////////////////////////////////////
get_template_part( 'menu', 'tab' ); 
 
 
?>

	
	<div  class='content' >
    	<div  class='left' >
        
        
        	<?php dynamic_sidebar( 'LeftSideBar' ); ?>
         			 
        
        	 <?php 	get_template_part( 'left', 'side' );?>
            
    	</div>
        
        <div  class='right' >
         
		  
         <div class="right-inner-gray" >
         
         <?php
//echo $user_ID;
$options = get_option( 'mer_theme_options' );?>
<h2>Your Registration</h2>
          
      <?php 
 
	$post_type='registration';
   $args = array(
    'post_type' => $post_type , // This is where you should put your Post Type 
     'meta_key' => '__kf_AttendeeID',
	 'meta_value' => $user_ID,	  
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'orderby'=>'ID',
	'order'=> 'DESC' 
	
   );
   
   
     $posts_per_page=$options['post_per_page'];
	$loop=0;
	 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'orderby'        => 'post_date',
        'order'            => 'DESC',
        'category'        => '13',
        'post_type'        => 'post',
        'post_status'        => 'publish',
        'posts_per_page'    => $posts_per_page,
        'paged'            => $paged,
    );
 
   $postslist = get_posts( $args );
    foreach ($postslist as $post) {
		echo $query->post_title;
		
		} 
	 
	
	
      post_pagination(10);
	
	  
	   ?>
		 
 
           </div>
          
           
    	</div>
        <div class="clear" ></div>    
    </div>


</div>

<?php get_footer(); ?>
  


           
    	