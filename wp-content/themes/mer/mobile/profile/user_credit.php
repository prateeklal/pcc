<?php


//echo $user_ID;
$options = get_option( 'mer_theme_options' );?>
<h2>Your Credits</h2>
          
      <?php 
 
	$post_type='transaction';
    $posts_per_page=$options['post_per_page'];
	$loop=0;
	 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'orderby'        => 'ID',
        'order'            => 'DESC',
        'post_type' => $post_type , // This is where you should put your Post Type 
		 'meta_query' => array(
							array(
							'key' => 'user_id',
							'value' => $user_ID 
							),
							array(
							'key' => 'refunded',
							'value' => 1
							),
							array(
							'key' => 'credit_used',
							'value' => '0'
							)),
        'post_status'        => 'publish',
        'posts_per_page'    => -1
        
    );
 
    
	/*$postslist=get_posts($args);
	var_dump($postslist);*/
	$the_query = new WP_Query( $args );
	 

	 ?><table class="gridtable" width="100%" ><tr><th>Invoice ID</th><th>Registration ID</th><th>Date</th><th>Course</th><th align='right'>Credits</th></tr>

        <?php
		  
		$total=0;
	  while ( $the_query->have_posts()   ):
	$the_query->next_post();
	$id= $the_query->post->ID;
	$course=get_post_meta($id,'course',true);
	$registration_id=get_post_meta($id,'registration_id',true);
	 $coursepost=get_post($course);
	 $locations=wp_get_post_terms($coursepost->ID,'location',array("fields" => "names"));
	 echo  "<tr><td>".$the_query->post->ID."</td><td>".$registration_id."</td><td>".date("Y-m-d", strtotime($the_query->post->post_date))."</td><td><a class='conference-title-green' href='".get_permalink($course)."'>".$coursepost->post_title .', '.$coursepost->start_date.", ". $locations[0]."</td><td align='right' >".$coursepost->credits."</td></tr>" ;
$total=$total+$coursepost->credits;
	 // echo  "<p><a class='conference-title-green' href='".get_permalink($course)."'>".$coursepost->post_title .', '.$coursepost->start_date.", ". $locations[0]."</a></p>" ;
endwhile;
	if($total =='0'){
		 echo  "<tr><td colspan='5'>No credits found</td></tr>" ;

		}else{
			
			echo  "<tr><td align='right' colspan='4'>Total :</td><td  align='right'>$total</td></tr>" ;

			}
	
	?>
    
    
    
    </table>
        <?php
	wp_reset_query();
wp_reset_postdata(); 
 
 // post_pagination($total);
	
	  
	   ?>
		 <br/>
 <br/>
           </div>
          
      

           
    	