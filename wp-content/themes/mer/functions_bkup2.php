<?php
//
session_start();

if (isset($_GET['review'])){

 $_SESSION['review']=1;
header("Location:index.php");
}

if (!$_SESSION['review']){

 require_once(get_template_directory() .'/offline.php');
 exit;
}


 

/**

 * @package WordPress

 * @subpackage mer

 */



/**

 * Make theme available for translation

 * Translations can be filed in the /languages/ directory

 * If you're building a theme based on Bluewave, use a find and replace

 * to change 'Bluewave' to the name of your theme in all the template files

 */
 
 
function is_edit_page($new_edit = null){
    global $pagenow;
    //make sure we are on the backend
    if (!is_admin()) return false;


    if($new_edit == "edit")
        return in_array( $pagenow, array( 'post.php',  ) );
    elseif($new_edit == "new") //check for new post page
        return in_array( $pagenow, array( 'post-new.php' ) );
    else //check for either new or edit
        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
}


function mer_AjaxWrapper_callback(){
$prefix='mer_';
$function=$prefix.$_POST['FUNCTION'];
 $objs = $_POST;
  unset( $objs['FUNCTION']);
  unset( $objs['action']);
  $pramas=array();
  
                   foreach ( $objs as $k=>$v)
                {
				
                     $pramas[] = $v ;
                }

	$str=call_user_func_array($function,$pramas );
	
	header('Content-Type: application/json');
	echo $str;
	die();
}
add_action( 'wp_ajax_mer_data_callback', 'mer_data_callback' );

add_action( 'wp_ajax_nopriv_mer_data_callback', 'mer_data_callback' );
function mer_date_compare($a, $b)
						{
							$t1 = strtotime($a['sort_date']); //$a['datetime'] sorting column name
							$t2 = strtotime($b['sort_date']);
							return $t1 - $t2;
						}    
function mer_data_callback() {

	mer_AjaxWrapper_callback();
}

 function mer_check_email($email,$id) {

	  global  $wpdb;   
	 
 $tbl= $wpdb->prefix."users";
   $sql="SELECT ID FROM  $tbl where user_email='$email' " ;
 $result=0;
 $rowid = $wpdb->get_var($sql);
 
 if($rowid!=""){
 		if($rowid!=$id){
		 $result=$rowid;
		 }
 
 }
 
 $str = "{\"result\" : \"" . 0 . "\",\"content\" : \"" . $result . "\"}";
return $str;
}
 
 
 function mer_check_username($uname,$id) {

	  global  $wpdb;   
	 
 $tbl= $wpdb->prefix."users";
 
  $sql="SELECT ID FROM  $tbl where user_login='$uname' " ;
 $result=0;
 $rowid = $wpdb->get_var($sql);
 
 if($rowid!=""){
 		if($rowid!=$id){
		 $result=$rowid;
		 }
 
 }
 
 $str = "{\"result\" : \"" . 0 . "\",\"content\" : \"" . $result . "\"}";
return $str;
}
 
 
if ( get_magic_quotes_gpc() ) {

    $_POST      = array_map( 'stripslashes_deep', $_POST );
    $_GET       = array_map( 'stripslashes_deep', $_GET );
    $_COOKIE    = array_map( 'stripslashes_deep', $_COOKIE );
    $_REQUEST   = array_map( 'stripslashes_deep', $_REQUEST );
}
load_theme_textdomain( 'mer', TEMPLATEPATH . '/languages' );
require_once ( get_template_directory() . '/Mobile_Detect.php' );

require_once ( get_template_directory() . '/functions-options.php' );
require_once(get_template_directory() . "/Tax-meta-class.php");
require_once(ABSPATH . 'wp-includes/class-wp-image-editor.php');
require_once(get_template_directory() .'/admin/authorizenet.class.php');
$locale = get_locale();

$locale_file = TEMPLATEPATH . "/languages/$locale.php";

if ( is_readable( $locale_file ) )

	require_once( $locale_file );



/**

 * Set the content width based on the theme's design and stylesheet.

 */

if ( ! isset( $content_width ) )

	$content_width = 640; /* pixels */



/**

 * This theme uses wp_nav_menu() in one location.

 */

 register_nav_menus( array(

	'mainmenu' => __( 'Main Menu', 'mer' ),

	'topmenu' => __( 'Top Menu', 'mer' ),

	'footermenu' => __( 'Footer Menu', 'mer' ),

) );

 
add_filter( 'show_admin_bar', '__return_false' );
 
/**

 * Add default posts and comments RSS feed links to head

 */

add_theme_support( 'automatic-feed-links' );

/**

 * Add support for the Aside and Gallery Post Formats

 */

add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );


/**

 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.

 */
function updateData($tablename, $input_array,$where) {
        $query = "update $tablename set ";
		$fields="";
		foreach ($input_array as $k => $v) {
		$v=GetSQLValueString($v,"text"); 
		
			$fields .= "`$k` =$v,";
		
		}
		
		
		$fields = substr($fields,0,strlen($fields)-1);
		
		$query .= $fields ;
		$condition ='';
		
		//////////////////////  where /////////////////////////
		if(is_array($where)){
			foreach($where as  $key => $value ){
			
			
					if  ($condition==''){
					$condition="$key=". GetSQLValueString($value,"text"); 
					}else{
					 $condition.=" and $key=". GetSQLValueString($value,"text"); 
					 }
			}
			$where=$condition;
			 $query .=" where $where";
		}

	 return  $query;
            
    }



function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "''";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "1";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "1";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "1";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}


function mer_page_menu_args($args) {

	$args['show_home'] = true;

	return $args;

}

function conference_date($sdate,$edate) {
$sdateD= date("d",  strtotime($sdate));
$sdateM= date("M",  strtotime($sdate));	
$sdateY= date("Y",  strtotime($sdate));
	
$edateD= date("d",  strtotime($edate));
$edateM= date("M",  strtotime($edate));	
$edateY= date("Y",  strtotime($edate));

$date='';
	if($sdateY==$edateY){
		if($sdateM==$edateM)
			{$date="$sdateM $sdateD-$edateD, $sdateY";}
		else
			{$date="$sdateM $sdateD-$edateM $edateD,$sdateY";}
	}else 
	{
		$date="$sdateM $sdateD-$edateM $edateD, $edateY";
		
	}

return $date;
}

 

add_filter( 'wp_page_menu_args', 'mer_page_menu_args' );

function custom_permalink($permalink) {
		 $permalink_structure = get_option('permalink_structure');
		 if( $permalink_structure){
		 $permalink= str_replace('/?','/',$permalink);	
		 $permalink= str_replace('?','/',$permalink);
		 $permalink= str_replace('=','/',$permalink);
    	 $permalink= str_replace('&','/',$permalink);
		 }
     return $permalink;

};

function add_rewrite_rules($rules) {
$aNewRules = array('register/conference/([^/]+)/?$' => 'index.php?pagename=register&conferenceid=$matches[1]');
 $rules = $aNewRules + $rules;
$aNewRules = array('payment/conference/([^/]+)/pending/([^/]+)/?$' => 'index.php?pagename=payment&conferenceid=$matches[1]&pending=$matches[2]');
 $rules = $aNewRules + $rules;


$aNewRules = array('location/([^/]+)/([^/]+)/([^/]+)/?$' => 'index.php?pagename=location&loc=$matches[1]&month=$matches[3]-$matches[2]');
$rules = $aNewRules + $rules; 

$aNewRules = array('location/([^/]+)/([^/]+)/?$' => 'index.php?&pagename=location&month=$matches[2]-$matches[1]');
$rules = $aNewRules + $rules;

/* $aNewRules = array('location/month/([^/]+)/?$' => 'pagename=location&index.php?month=$matches[1]');
$rules = $aNewRules + $rules; 

 $aNewRules = array('location/([^/]+)/month/([^/]+)/?$' => 'index.php?pagename=location&loc=$matches[1]&month=$matches[2]');
$rules = $aNewRules + $rules; */



 $aNewRules = array('topics/([^/]+)/?$' => 'index.php?pagename=topics&topics=$matches[1]');
$rules = $aNewRules + $rules; 


$aNewRules = array('schedule/([^/]+)/([^/]+)/?$' => 'index.php?pagename=schedule&month=$matches[2]-$matches[1]');

$aRules = $aNewRules + $rules;



/* echo "<pre>";
 print_r($aRules);
echo "</pre>";*/ 
 return $aRules; 

}
 
// hook add_rewrite_rules function into rewrite_rules_array
add_filter('rewrite_rules_array', 'add_rewrite_rules');

 //wp_page_menu('show_home=1&include=9999');  

function array_sort_by_column(&$array, $column, $direction = SORT_ASC) {
    $reference_array = array();

    foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
    }

    array_multisort($reference_array, $direction, $array);
}
/**

 * Register  area and update sidebar with default widgets

 */
    add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );  
    function custom_image_sizes_choose( $sizes ) {  
        $custom_sizes = array(  
            'slider-image' => 'Home Slider Image' ,
			 'featured-location' => 'Featured Location Image' ,
			  'schedule-page-image' => 'Schedule Page Image' ,
			   'venue-image' => 'Venue Image' , 
			    'course-image' => 'Course Image' , 
        );
		
		
		  
        return array_merge( $sizes, $custom_sizes );  
    }  
add_action( 'show_user_profile', 'mer_user_profile_fields' );
add_action( 'edit_user_profile', 'mer_user_profile_fields' );	
add_action( 'personal_options_update', 'mer_save_profile_fields' );
add_action( 'edit_user_profile_update', 'mer_save_profile_fields' );
function mer_post_types_in_search($query) {
	if($query->is_search && is_admin() ) {

	// put all the meta fields you want to search for here	 
		$custom_fields = array(
			"__kp_Transaction_ID",
			"__kf_Attendee_ID",
			"Transaction_Category",
			"First_Name",
			"Last_Name",
			"CC_Num",
			"Registration_Fee_Type"
			);
	$meta_query = array('relation' => 'OR');
foreach($custom_fields as $cf) {
		array_push($meta_query, array(
		'key' => $cf,
		'value' => $query->query_vars['s'],
		
		'compare' => 'LIKE'));
	
	}
	$query->set('meta_query', $meta_query );

  
/*
	  echo "<pre>";
	  var_dump($query->query_vars['post_type']);
	echo "</pre>"; */
	}
	return $query;
}
 add_filter('pre_get_posts', 'mer_post_types_in_search');	
function mer_init() {
	add_rewrite_tag('%loc%','([^&]+)');
	add_rewrite_tag('%month%','([^&]+)');
	add_rewrite_tag('%topics%','([^&]+)');
	add_rewrite_tag('%conferenceid%','([^&]+)');
	add_rewrite_tag('%cat%','([^&]+)');
	add_rewrite_tag('%msg%','([^&]+)');
	add_rewrite_tag('%switch%','([^&]+)');
	add_rewrite_tag('%pending%','([^&]+)');
	add_rewrite_tag('%review%','([^&]+)');

	if ( is_admin()) {
	$options = get_option( 'mer_theme_options' );
	//var_dump($options);
	add_theme_support( 'post-thumbnails' );
	//set_post_thumbnail_size( 150, 150, true ); 
	
	add_image_size( 'featured-location', $options['thumb_loc_w'],  $options['thumb_loc_h'], true );
	
	 add_image_size( 'slider-image', $options['thumb_semi_w'],  $options['thumb_semi_h'], true );
	 set_post_thumbnail_size( $options['thumb_semi_w'],  $options['thumb_semi_h'] );
	  /*add_image_size( 'schedule-page-image', $options['thumb_sch_h'],  $options['thumb_sch_h'], true );
	   add_image_size( 'course-image', $options['thumb_course_w'],  $options['thumb_course_h'], true );*/
	    add_image_size( 'venue-image', $options['thumb_ven_w'],  $options['thumb_ven_h'], true );
	}
	
	if (! is_admin()) {
		
		wp_enqueue_style('lightbox.css','/wp-content/themes/mer/css/jquery.lightbox-0.5.css', null, '1.0');
		wp_enqueue_script('jquery');
		wp_register_script( 'mer-lightbox', '/wp-content/themes/mer/js/jquery.lightbox-0.5.min.js');
	wp_enqueue_script('mer-lightbox'   );
	
	}
	
	/////////////////////////// Topic  /////////////////////////////////////////////////////////
	register_post_type( 'topic',
		array(
			'labels' => array(
				'name' => __( 'Topic' ),
				'singular_name' => _x( 'Topic', 'Topic singular name' ),
				'search_items' =>  __( 'Search Topic' ),
				'all_items' => __( 'All Topics' ),
				'view_item' => __( 'View Topic' ),
				'parent_item_colon' => __( 'Parent Topic:' ),
				'edit_item' => __( 'Edit Topic' ), 
				'update_item' => __( 'Update Topic' ),
				'add_new_item' => __( 'Add New Topic' ),
				'new_item_name' => __( 'New Topic' ),
				'not_found' => __( 'No Topic found' ),
				'not_found_in_trash' => __( 'No Topic found in Trash' ),
				'menu_name' => __( 'Topic' ),
				'rewrite' => array('slug' => 'topic') 
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'supports' => array( '' )
		)
	);	


	
	 /////////////////////////// Conference  /////////////////////////////////////////////////////////
	register_post_type( 'conference',
		array(
			'labels' => array(
				'name' => __( 'Conference' ),
				'singular_name' => _x( 'Conference', 'Conference singular name' ),
				'search_items' =>  __( 'Search Conference' ),
				'all_items' => __( 'All Conferences' ),
				'view_item' => __( 'View Conference' ),
				'parent_item_colon' => __( 'Parent Conference:' ),
				'edit_item' => __( 'Edit Seminar' ), 
				'update_item' => __( 'Update Conference' ),
				'add_new_item' => __( 'Add New Conference' ),
				'new_item_name' => __( 'New Conference' ),
				'not_found' => __( 'No Conference found' ),
				'not_found_in_trash' => __( 'No Conference found in Trash' ),
				'menu_name' => __( 'Conference' ),
				'rewrite' => array('slug' => 'conference') 
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		capabilities=>array('create_posts' => false,
				'edit_post' => 'manage_options',
                'read_post' => 'manage_options',
                'delete_post' => 'manage_options',
                'edit_posts' => 'manage_options',
                'edit_others_posts' => 'manage_options',
                'publish_posts' => 'manage_options',
                'read_private_posts' => 'manage_options'),
		'supports' => array( '',  'thumbnail' )
		)
	);
	
add_filter( 'manage_edit-conference_columns', 'edit_conference_columns' ) ;

function edit_conference_columns( $columns ) {

	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
		'Course_Topic' => __( 'Course Topic' ),
		'__kp_CourseID' => __( 'Course ID' ),
		'Course_Start_Date' => __( 'Start Date' ),
		'Course_End_Date' => __( 'End Date' ),
		'Course_Location' => __( 'Location' ),
		'Date' => __( 'Date' )
	);

	return $columns;
}	


add_action( 'manage_conference_posts_custom_column', 'manage_conference_columns', 10, 2 );

function manage_conference_columns( $column, $post_id ) {
	global $post;
	if($column=='Course_Topic'){$title= get_post_meta( $post_id, $column, true );

echo "<a href='post.php?post=$post_id&action=edit'  class='row-title' title='$title' >$title</a>";


	}
	else if($column=='Date'){
	echo date('Y-m-d', strtotime($post->post_date));
	}
	else{

	echo get_post_meta( $post_id, $column, true );
	}
}
	

add_filter( 'manage_edit-conference_sortable_columns', 'conference_sortable_columns' );

function conference_sortable_columns( $columns ) {

	 
	$columns['__kp_CourseID'] = '__kp_CourseID';
$columns['Course_Start_Date'] = 'Course_Start_Date'; 
$columns['Course_Location'] = 'Course_Location';
 

	return $columns;
}


/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'edit_conference_load' );

function edit_conference_load() {
	add_filter( 'request', 'sort_conference' );
}

/* Sorts the movies. */
function sort_conference( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'conference' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			/* Merge the query vars with our custom variables. */
			$arr['meta_key']=$vars['orderby'];
			if($vars['orderby']=='__kp_CourseID' || $vars['orderby']=='Course_Start_Date' )
			$arr['orderby']='meta_value_num';
			 
			$vars = array_merge(
				$vars,$arr
				
			);
		}
	}

	return $vars;
}

	//////////////////////////// Registration  ///////////////////////////////////////////////

	register_post_type( 'registration',
		array(
			'labels' => array(
				'name' => __( 'Registration' ),
				'singular_name' => __( 'Registration' ),
				'singular_name' => _x( 'Registration', 'Registration singular name' ),
				'search_items' =>  __( 'Search Registration' ),
				'all_items' => __( 'All Registration' ),
				'view_item' => __( 'View Registration' ),
				'parent_item_colon' => __( 'Parent Registration:' ),
				'edit_item' => __( 'Edit Registration' ), 
				'update_item' => __( 'Update Registration' ),
				'add_new_item' => __( 'Add New Registration' ),
				'new_item_name' => __( 'New Registration' ),
				'not_found' => __( 'No Registration found' ),
				'not_found_in_trash' => __( 'No Registration found in Trash' ),
				'menu_name' => __( 'Registration' )
			),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('hear_about_seminar'),
		'rewrite' => true,
		'capability_type' => 'post',
		'rewrite' => array('slug' => 'registration') ,
		'supports' => array( ''),
		 
		)
	);
	
	
add_filter( 'manage_edit-registration_columns', 'edit_registration_columns' ) ;

function edit_registration_columns( $columns ) {

	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
		'Attendee' => __( 'Attendee' ),
		'__kf_Attendee_ID' => __( 'Attendee ID' ),
		'__kp_Registration_ID' => __( 'Reg ID' ),
		'Registration_Fee' => __( 'Reg Fee' ),
		'Registration_Date' => __( 'Reg Date' ),
		'Registration_Status' => __( 'Status' ),
		'Date' => __( 'Date' )
	);

	return $columns;
}	


add_action( 'manage_registration_posts_custom_column', 'manage_registration_columns', 10, 2 );

function manage_registration_columns( $column, $post_id ) {
	global $post;
	if($column=='Attendee'){
	
	 $user_id=get_post_meta( $post_id, '__kf_Attendee_ID', true );
	$title= get_user_meta( $user_id, 'first_name', true ) . ' ' . get_user_meta( $user_id, 'last_name', true );

echo "<a href='post.php?post=$post_id&action=edit'  class='row-title' title='$title' >$title</a>";


	}else if($column=='Registration_Date'){
	echo date('Y-m-d', strtotime(get_post_meta( $post_id, $column, true )));
	}
	else if($column=='Date'){
	echo date('Y-m-d', strtotime($post->post_date));
	}
	else{

	echo get_post_meta( $post_id, $column, true );
	}
}
		
   
 add_filter( 'manage_edit-registration_sortable_columns', 'registration_sortable_columns' );

function registration_sortable_columns( $columns ) {

	 
	$columns['__kf_Attendee_ID'] = '__kf_Attendee_ID';
$columns['__kp_Registration_ID'] = '__kp_Registration_ID'; 
$columns['Registration_Date'] = 'Registration_Date';
$columns['Registration_Status'] = 'Registration_Status';
 


	return $columns;
}



/* Only run our customization on the 'edit.php' page in the admin. */
add_action( 'load-edit.php', 'edit_registration_load' );

function edit_registration_load() {
	add_filter( 'request', 'sort_registration' );
}

/* Sorts the movies. */
function sort_registration( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'registration' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			/* Merge the query vars with our custom variables. */
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => $vars['orderby'],
					'orderby' => 'meta_value_num'
				)
			);
		}
	}

	return $vars;
} 
  
  $prefix = '';
  $config = array(
    'id' => 'mer_meta_box',          // meta box id, unique per meta box
    'title' => 'Image Detail',          // meta box title
    'pages' => array('course_photo','venue_photo'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => true,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  $my_meta =  new Tax_Meta_Class($config);
   $my_meta->addImage($prefix.'image',array('name'=> __('Image','mer')));
$my_meta->Finish();
$config = array(
    'id' => 'mer_meta_box',          // meta box id, unique per meta box
    'title' => 'Document Detail',          // meta box title
    'pages' => array('document'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => true,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
   $my_meta =  new Tax_Meta_Class($config);
  
  $my_meta->addFile($prefix.'document',array('name'=> __('Course Document ','mer')));
 $my_meta->Finish();
  
//////////////////////////////////////////////////////////////////////////////////
	


  
  
  /////////////////////////// Agenda  /////////////////////////////////////////////////////////
	register_post_type( 'agenda',
		array(
			'labels' => array(
				'name' => __( 'Agendas' ),
				'singular_name' => __( 'Agenda' ),
				'singular_name' => _x( 'Agendas', 'Agenda singular name' ),
				'search_items' =>  __( 'Search Agenda' ),
				'all_items' => __( 'All Agendas' ),
				'view_item' => __( 'View Agenda' ),
				'parent_item_colon' => __( 'Parent Agenda:' ),
				'edit_item' => __( 'Edit Agenda' ), 
				'update_item' => __( 'Update Agenda' ),
				'add_new_item' => __( 'Add New Agenda' ),
				'new_item_name' => __( 'New Agenda' ),
				'not_found' => __( 'No course found' ),
				'not_found_in_trash' => __( 'No course found in Trash' ),
				'menu_name' => __( 'Agenda' ),
				'rewrite' => array('slug' => 'agenda') 
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'supports' => array( '', )
		)
	);
	
	
  
	
	//////////////////////////// Transaction  ///////////////////////////////////////////////

	register_post_type( 'transaction',
		array(
			'labels' => array(
				'name' => __( 'Transactions' ),
				'singular_name' => __( 'Transaction' ),
				'singular_name' => _x( 'Transaction', 'Transaction singular name' ),
				'search_items' =>  __( 'Search Transaction' ),
				'all_items' => __( 'All Transaction' ),
				'view_item' => __( 'View Transaction' ),
				'parent_item_colon' => __( 'Parent Transaction:' ),
				'edit_item' => __( 'Edit Transaction' ), 
				'update_item' => __( 'Update Transaction' ),
				'add_new_item' => __( 'Add New Transaction' ),
				'new_item_name' => __( 'New Transaction' ),
				'not_found' => __( 'No Transaction found' ),
				'not_found_in_trash' => __( 'No Transaction found in Trash' ),
				'menu_name' => __( 'Transactions' )
				
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'rewrite' => array('slug' => 'transaction') ,
		'supports' => array( ''),
		 
		)
	);
	
	
	
	add_filter( 'manage_edit-transaction_columns', 'edit_transaction_columns' ) ;

function edit_transaction_columns( $columns ) {

	$columns = array(
		'cb' => '<input id="cb-select-all-1" type="checkbox" />',
		'Name' => __( 'Name' ),		
		'__kf_Attendee_ID' => __( 'Attendee ID' ),
		'__kp_Transaction_ID' => __( 'Transaction ID' ),
			'__kf_Registration_ID' => __( 'Registration ID' ),
		'Registration_Fee_Type' => __( 'Fee Type' ),
		'Transaction_Amount' => __( 'Amount' ),
		'Transaction_Category' => __( 'Category' ),
		
			
		'Date' => __( 'Date' )
	);

	return $columns;
}	



add_action( 'manage_transaction_posts_custom_column', 'manage_transaction_columns', 10, 2 );

function manage_transaction_columns( $column, $post_id ) {
	global $post;
	if($column=='Name'){$title= get_post_meta( $post_id, 'First_Name', true ) .' ' . get_post_meta( $post_id, 'Last_Name', true ) ;

echo "<a href='post.php?post=$post_id&action=edit'  class='row-title' title='$title' >$title</a>";


	}
	else if($column=='Date'){
	echo date('Y-m-d', strtotime($post->post_date));
	}
	else{

	echo get_post_meta( $post_id, $column, true );
	}
}
	

 add_filter( 'manage_edit-transaction_sortable_columns', 'transaction_sortable_columns' );

function transaction_sortable_columns( $columns ) {

	 
	$columns['__kp_Transaction_ID'] = '__kp_Transaction_ID';
$columns['__kf_Attendee_ID'] = '__kf_Attendee_ID'; 
$columns['Transaction_Category'] = 'Transaction_Category';
 

	return $columns;
}


/* Only run our customization on the 'edit.php' page in the admin. */
//add_action( 'load-edit.php', 'edit_transaction_load' );

function edit_transaction_load() {
	add_filter( 'request', 'sort_transaction' );
}

/* Sorts the movies. */
function sort_transaction( $vars ) {

	/* Check if we're viewing the 'movie' post type. */
	if ( isset( $vars['post_type'] ) && 'transaction' == $vars['post_type'] ) {

		/* Check if 'orderby' is set to 'duration'. */
		if ( isset( $vars['orderby'] )  ) {

			 
			/* Merge the query vars with our custom variables. */
			$arr['meta_key']=$vars['orderby'];
			if($vars['orderby']=='__kp_Transaction_ID' || $vars['orderby']=='__kf_Attendee_ID' )
			$arr['orderby']='meta_value_num';
			
			$vars = array_merge(
				$vars,$arr
				
			);
		}
	}

	return $vars;
}

 
  
 /////////////////////////  SIDE BAR          ////////////////////////////////////////////////////////
 register_sidebar( array (

		'name' => __( 'Left Side Bar', 'mer' ),

		'id' => 'leftsidebar',

		'description' => __( 'A widget area for left side', 'mer' ),

		'before_widget' => '',

		'after_widget' => '',

		'before_title' => '<h2 class="widget-title">',

		'after_title' => '</h2>',

	) );



/////////////////////////  SIDE BAR          ////////////////////////////////////////////////////////
 register_sidebar( array (

		'name' => __( 'Home Page Slider', 'mer' ),

		'id' => 'homeslider',

		'description' => __( 'A widget area for Home Slider', 'mer' ),

		'before_widget' => '',

		'after_widget' => '',

		'before_title' => '<h2 class="widget-title">',

		'after_title' => '</h2>',

	) );
register_sidebar( array (

		'name' => __( 'Footer Menu', 'mer' ),

		'id' => 'footermenu',

		'description' => __( 'A widget area for footer menu', 'mer' ),

		'before_widget' => '',

		'after_widget' => '',

		'before_title' => '<h2 class="widget-title">',

		'after_title' => '</h2>',

	) );

}

 function monthsBetween($startDate, $endDate) {
    $retval = "";

    // Assume YYYY-mm-dd - as is common MYSQL format
    $splitStart = explode('-', $startDate);
    $splitEnd = explode('-', $endDate);

    if (is_array($splitStart) && is_array($splitEnd)) {
        $difYears = $splitEnd[0] - $splitStart[0];
        $difMonths = $splitEnd[1] - $splitStart[1];
        $difDays = $splitEnd[2] - $splitStart[2];

        $retval = ($difDays > 0) ? $difMonths : $difMonths - 1;
        $retval += $difYears * 12;
    }
    return $retval;
}
  function the_breadcrumb() {



	if (!is_home()) {

	

		echo '<a href="';

		echo get_option('home');

		echo '">';

		bloginfo('name');

		echo "</a> / ";

		if (is_category() || is_single()) {

			the_category('title_li=');

			if (is_single()) {

				echo " / ";

				the_title();

			}

		} elseif (is_page()) {

			echo the_title();

		}

	}

} 

   function current_page_url() {
	$pageURL = 'http';
	if( isset($_SERVER["HTTPS"]) ) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

 function credits_function() {

	require_once ( get_template_directory() . '/admin/credits_list.php' );
}


function my_remove_meta_boxes() {

 add_menu_page( 'Credits', 'User Credits', 'edit_others_posts', 'credits', 'credits_function', '' , 25 ); 

  //add_menu_page( 'Credits', 'User Credits', 'manage_options', 'themes.php?type=credit&amp;page=theme_options', '', '', .6 );


//if(!current_user_can('administrator')) {
	
	
	/* remove_meta_box('course_photodiv', 'course', 'side');
 	remove_meta_box('venue_photodiv', 'venue', 'side');
 remove_meta_box('venue_photodiv', 'featured', 'side');*/
 
	
remove_meta_box('conferencediv', 'seminar', 'side');	
  remove_meta_box('user_titlediv', 'user', 'side');
  remove_meta_box('user_titlediv', 'registration', 'side');
  
  
 
  remove_meta_box('hear_about_seminardiv', 'registration', 'side');
  
  remove_meta_box('degreediv', 'user', 'side');
  remove_meta_box('degreediv', 'registration', 'side');
 
   remove_meta_box('agenda_itemdiv', 'agenda', 'side');
  
  
  
  remove_meta_box('specialitydiv', 'user', 'side');
  remove_meta_box('specialitydiv', 'registration', 'side');
  
  
  
  remove_meta_box('activitydiv', 'user', 'side');
  
  remove_meta_box('like_informationdiv', 'user', 'side');
 
 
//}
 
}
	function post_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)){
			if(!isset($_GET['paged']))
			{
				$paged =$_GET['paged']  ; 
			}else{
			 $paged = 1;
			}
	   }

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}
add_action( 'admin_menu', 'my_remove_meta_boxes' );
add_action( 'init', 'mer_init' );

add_action("admin_notices", "show_names");
 function show_names(){
	// $_SESSION['my_admin_notices']='oops';
	 $msg= get_transient( 'my_admin_notices' );
	 if($msg !=''){
	 echo '<div class="error">
       <p>'.$msg.'</p>
    </div>';
	 delete_transient( 'my_admin_notices' );
	 }
	// echo "User Name already";
}
require_once(get_template_directory() .'/admin/meta-boxes.php');
require_once(get_template_directory() .'/admin/meta-boxes-function.php');

add_action( 'add_meta_boxes', 'mer_add_custom_box' );

/* Do something with the data entered */
add_action( 'save_post', 'mer_save_postdata' );


 
 